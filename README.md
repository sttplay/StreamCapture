# StreamCapture

#### 介绍
**StreamCapture是基于开源音视频处理库FFMpeg**，StreamCapture为从事音视频处理领域的开发者简单高效的获取音视频帧数据。StreamCapture具有极强的通用性，使用StreamCapture能够快速高效的搭建开发（测试）环境，用户可以根据自己的需求灵活自定义各种功能。StreamCapture分为三大模块，分别为**音视频采集模块**，**音频重采样模块**和**音频播放模块**，音频重采样模块和音频播放模块是辅助模块，能够让用户快速见到效果。StreamCapture也提供了丰富的输出像素格式，支持YUV类，NV类，RGB类等等。
项目请看 [https://gitee.com/sttplay/StreamCapture]


#### 项目介绍
打开/StreamCapture/src/StreamCapture.sln解决方案  
![EasyPR 输出窗口](resources/image/preview.png)   
StreamCapture目前分为四个工程，作用如下
1.  StreamCapture是核心工程(Cmake)
2.  SCPlayer基于StreamCapture开发的音视频播放器（基于Qt5.9.8）
3.	SCPlayer/SCPlayer.pro为Qt工程
4.  TestStreamCapture是StreamCapture的测试工程（C++版）
5.	StreamCaptureUnity是适用于Unity的SCPlayer播放器

#### SCPlayer使用说明
SCPlayer是基于StreamCapture开发的音视频播放器（控制与界面分离），有着良好的兼容性，目前支持在windows7，windows10以及linux上运行，SCPlayer不仅可以打开媒体文件也支持USB网络摄像机。具体操作见下图。  
###### 基础设置
![EasyPR 输出窗口](resources/image/base.png)  
###### 相机设置
![EasyPR 输出窗口](resources/image/camera.png)  
###### 其他设置
![EasyPR 输出窗口](resources/image/other.png)  