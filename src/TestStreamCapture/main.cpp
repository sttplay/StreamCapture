#include <iostream>
#include "../StreamCapture/Core/IStreamCapture.h"
#include <windows.h>
#include <assert.h>
#include <thread>
#include <atomic>

#define ENABLE_SDL		0

#if ENABLE_SDL
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#pragma comment(lib, "SDL2.lib")
#endif // ENABLE_SDL
#pragma comment(lib, "StreamCapture.lib")

void sclogcb(int level, const char* log)
{
	printf("[%d]:%s\n", level, log);
}

void scopencb(void* owner, int oi, StreamsParameters* sp)
{

}
int openindex = 0;

void test(void* capture)
{
	SetOption(capture, OptionType::OT_EnableAudio, 0);
	SetOption(capture, OptionType::OT_TryHardwareAccel, HW_ACCEL_CUDA);
	//StreamsParameters* p = Open(capture, u8"F:/HTTPServer/8K天使瀑布.mp4");
	OpenAsync(capture, openindex++, u8"F:/HTTPServer/8K天使瀑布.mp4", scopencb);
	//if (p->retcode != RETCODE_SUCCESS)
	//{
	//	printf("%s\n", p->errLog);
	//	return;
	//}
	////OpenAsync(capture, openindex++, "F:/HTTPServer/8K.mp4")
	//while (true)
	//{
	//	Frame* frame = TryGrabFrame(capture, FRAME_TYPE_VIDEO);
	//	if (!frame)
	//	{
	//		Sleep(10);
	//		continue;
	//	}
	//	if(frame->pts > 100)
	//		break;
	//	if (frame->flags == FRAME_FLAG_EOF)
	//		break;
	//	RemoveFrame(capture, FRAME_TYPE_VIDEO);
	//	printf("%lld\n", frame->pts);
	//}
	Sleep(100);
}
int main(int argc, char *argv[])
{
	InitializeStreamCapture(sclogcb);
	void* capture = CreateStreamCapture(NULL);
	for (int i = 0; i < 100; i++)
	{
		test(capture);
	}
	DeleteStreamCapture(capture);
	TerminateStreamCapture();
	UninstallStreamCapture();
	return 0;
}