#pragma once
#include "StreamCaptureDef.h"
#ifdef __cplusplus
#define EXTERNC extern "C"
#else
#define EXTERNC 
#endif

#ifdef _WIN32 //包含win32 和win64
#define HEAD EXTERNC __declspec(dllexport)
#define CallingConvention _cdecl
#else
#define HEAD EXTERNC
#define CallingConvention
#endif

#pragma region StreamCapture

/*
 * 获取版本号
 */
HEAD unsigned int CallingConvention GetStreamCaptureVersion();

/*
 * 初始化StreamCapture
 * @param cb:日志回调函数
 */
HEAD void CallingConvention InitializeStreamCapture(log_callback cb);

/*
 * 终止所有StreamCapture
 */
HEAD void CallingConvention TerminateStreamCapture();

/*
 * 等待所有对象全部删除，利于StreamCapture的卸载
 */
HEAD void CallingConvention UninstallStreamCapture();

/*
 * 创建StreamCapture
 * @param owner:StreamCapture的唯一标识，拥有者
 * @return StreamCapture
 */
HEAD void* CallingConvention CreateStreamCapture(void* owner);

/*
 * 删除StreamCapture
 * @param capture:要删除的StreamCapture
 */
HEAD void CallingConvention DeleteStreamCapture(void* capture);

/*
 * 非阻塞打开文件，网络流 ...
 * @param capture:目标StreamCapture
 * @param openIndex:打开的索引，用于判断回调是否有效
 * @param url:url(必须为utf8编码)
 * @param cb:打开后的回调函数
 */
HEAD void CallingConvention OpenAsync(void* capture, int openIndex, const char* url, open_callback cb);

/*
 * 阻塞打开文件，网络流 ...
 * @param capture:目标StreamCapture
 * @param url:url(必须为utf8编码)
 * @return 打开的结果参数
 */
HEAD StreamsParameters* CallingConvention Open(void* capture, const char* url);

/*
 * 注册中断回调函数
 * @param capture:目标StreamCapture
 * @param interrupt_callback:回调函数
 */
HEAD void CallingConvention RegisterInterruptCallback(void* capture, interrupt_callback cb);

/*
 * 关闭已经打开的文件，网络流 ...
 * @param capture:目标StreamCapture
 */
HEAD void CallingConvention Close(void* capture);

/*
 * 获取一帧数据
 * @param capture:目标StreamCapture
 * @param frameType:帧类型参考 FRAME_TYPE_
 * @return 失败返回NULL，成功返回一帧数据
 */
HEAD Frame* CallingConvention TryGrabFrame(void* capture, int frameType);

/*
 * 获取上一帧数据
 * @param capture:目标StreamCapture
 * @param frameType:帧类型参考 FRAME_TYPE_
 * @return 失败返回NULL，成功返回一帧数据
 */
HEAD Frame* CallingConvention TryGrabLastFrame(void* capture, int frameType);

//HEAD Frame* CallingConvention GrabFrame(void* capture, int frameType);

/*
 * 移除一帧数据
 * @param capture:目标StreamCapture
 * @param frameType:帧类型参考 FRAME_TYPE_
 */
HEAD void CallingConvention RemoveFrame(void* capture, int frameType);

///*
// * 锁定Frame
// * @param capture:目标StreamCapture
// * @param frameType:帧类型参考 FRAME_TYPE_
// */
//HEAD void CallingConvention LockFrame(void* capture, int frameType);
//
///*
// * 解锁Frame
// * @param capture:目标StreamCapture
// * @param frameType:帧类型参考 FRAME_TYPE_
// */
//HEAD void CallingConvention UnlockFrame(void* capture, int frameType);

/*
 * 获取当前解码器中packet和frame的数量
 * @param capture:目标StreamCapture
 * @return 失败返回NULL，成功返回PFCounts
 */
HEAD PFCounts* CallingConvention GetPFCounts(void* capture);

/*
 * 获取当前播放进度的估计值
 * @param capture:目标StreamCapture
 * @return 返回播放进度 取值范围[0, 1]
 */
HEAD double CallingConvention GetCurrentProgress(void* capture);

/*
 * 获取总时长
 * @param capture:目标StreamCapture
 * @return 获取总时长 单位：毫秒（对于实时流无意义）
 */
HEAD long long CallingConvention GetDuration(void* capture);

/*
 * 跳转到目标位置（非高精度）
 * @param capture:目标StreamCapture
 * @param percent:模糊范围,取值[0, 1)
 */
HEAD void CallingConvention Seek(void* capture, double percent);

/*
 * 设置参数
 * @param capture:目标StreamCapture
 * @param optionType:要设置的类型 参考枚举值 OptionType
 * @param value:要设置的值， 参考OptionType对应枚举值的说明
 * @return 设置成功返回0， 否则为设置失败，设置失败则恢复默认值
 */
HEAD int CallingConvention SetOption(void* capture, int optionType, int value);

/*
 * 清理packet缓冲队列
 * @param capture:目标StreamCapture
 * @param cacheType:缓冲类型参考CACHE_TYPE_
 */
HEAD void CallingConvention ClearCache(void* capture, int cacheType);

/*
 * 设置音轨
 * @param capture:目标StreamCapture
 * @param index:音轨索引
 */
HEAD void CallingConvention SetAudioTrack(void* capture, int index);

/*
 * 获取摄像机参数信息
 * @param camera: window上输入相机名称，linux上输入/dev/video0 ...
 * @return :参数信息  utf8 编码
 */
HEAD CameraDescription* CallingConvention GetCameraInfo(const char* camera);

#pragma endregion StreamCapture

#pragma region Resampler

/*
 * 创建Resampler
 * @return Resampler
 */
HEAD void* CallingConvention CreateResampler();

/*
 * 删除Resampler 并释放所有资源
 * @param sampler:要删除的Resampler
 */
HEAD void CallingConvention DeleteResampler(void* sampler);

/*
 * 删除Resampler
 * @param sampler:Resampler
 * @param srcas:输入流信息
 * @param destas:AudioPlay支持流信息
 * @return 0 成功 <0 失败
 */
HEAD int CallingConvention OpenResampler(void* sampler, AudioStream* srcas, AudioStream* destas);

/*
 * 关闭Resampler
 * @param sampler:Resampler
 */
HEAD void CallingConvention CloseResampler(void* sampler);

/*
 * 重采样音频数据
 * @param sampler:Resampler
 * @param frame:一帧音频数据
 * @return 返回空为失败
 */
HEAD ResampleData* CallingConvention Resample(void* sampler, Frame* frame);

#pragma endregion Resampler

#pragma region AudioPlay

/*
 * 创建AudioPlay
 * @param owner:AudioPlay的唯一标识，拥有者
 * @return 返回AudioPlay指针
 */
HEAD void* CallingConvention CreateAudioPlay(void* owner);

/*
 * 删除AudioPlay
 * @param ap:AudioPlay
 */
HEAD void CallingConvention DeleteAudioPlay(void* ap);

/*
 * 非阻塞打开音频设备
 * @param ap:AudioPlay
 * @param openIndex:打开的索引，用于判断回调是否有效
 * @param srcas:输入流信息
 * @param opencb:打开成功回调
 * @param playcb:播放回调
 */
HEAD void CallingConvention OpenAudioPlayAsync(void* ap, int openIndex, AudioStream* srcas, openaudioplay_callback opencb, audio_play_callback playcb);

/*
 * 阻塞打开音频设备
 * @param ap:AudioPlay
 * @param srcas:输入流信息
 * @param playcb:播放回调
 * @return 失败返回NULL
 */
HEAD AudioPlayParams* CallingConvention OpenAudioPlay(void* ap, AudioStream* srcas, audio_play_callback playcb);

/*
 * 关闭AudioPlay
 * @param ap:AudioPlay
 */
HEAD void CallingConvention CloseAudioPlay(void* ap);

/*
 * 开始播放
 * @param ap:AudioPlay
 */
HEAD void CallingConvention StartAudioPlay(void* ap);

/*
 * 混音
 * @param stream:目标地址
 * @param src:数据缓冲
 * @param len:数据缓冲大小（字节）
 * @param volume:音量大小
 */
HEAD void CallingConvention MixAudioFormat(unsigned char* stream, const unsigned char* src, int len, float volume);

#pragma endregion AudioPlay

#pragma region Utility
/*
 * 获取时间戳
 * @return 时间戳 单位 ns
 */
HEAD long long CallingConvention GetTimestamp();

/*
 * 获取UTC时间戳
 * @return UTC时间戳 单位 ns
 */
HEAD long long CallingConvention GetTimestampUTC();

/*
 * C# 数组转 pointer
 */
HEAD void* CallingConvention ArrayToPointer(void* ptr);

/*
 * C 语言的 memset
 */
HEAD void CallingConvention MemorySet(void* ptr, char value, int len);

HEAD void* CallingConvention MemoryAlignment(unsigned char* srcdata, int linepixelsize, int height, int linesize, unsigned char* destdata);

HEAD void* CallingConvention CreateAVFrame();
HEAD void CallingConvention ReleaseAVFrame(void* frame);
HEAD void* CallingConvention AVFrameClone(void* dst, void* src, Frame* &frame);
HEAD void* CallingConvention AVFrameMoveRef(void* dst, void* src, Frame* &frame);
#ifdef ENABLE_OPENCV
HEAD void CallingConvention CVDisplayAVFrame_Red(struct AVFrame* frame);
HEAD void CallingConvention CVDisplayFrame_Red(Frame* frame);
#endif // ENABLE_OPENCV
#pragma endregion Utility