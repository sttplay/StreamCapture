#include "AudioPlay.h"
#include <SDL2/SDL.h>
#include "AudioPlaySession.h"
#include "../StreamCapture/Tools.h"
#include "../SCModuleManager.h"


AudioPlay::AudioPlay(void* owner)
{
	++SCModuleManager::GlobalSessionRef.audioPlay_c;
	this->owner = owner;
}

AudioPlay::~AudioPlay()
{
	Close();
	++SCModuleManager::GlobalSessionRef.audioPlay_d;
}

AudioPlayParams* AudioPlay::Open(AudioStream* as, audio_play_callback playcb)
{
	Close();
	session = new AudioPlaySession(owner, NULL, playcb);
	return session ? session->Open(as) : NULL;
}

void AudioPlay::OpenAsync(AudioStream* as, int openIndex, openaudioplay_callback opencb, audio_play_callback playcb)
{
	Close();
	session = new AudioPlaySession(owner, opencb, playcb);
	session->OpenAsync(as, openIndex);
}

void AudioPlay::Close()
{
	if (!session) return;

	session->Release();
	AudioPlaySession* s = session;
	std::thread([s] {
		delete s;
	}).detach();
	session = NULL;
}

void AudioPlay::Start()
{
	if (!session) return;
	session->Start();
}
