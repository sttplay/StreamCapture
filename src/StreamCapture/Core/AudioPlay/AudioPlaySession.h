#pragma once
#include "../StreamCapture/StreamCapture.h"
#include "../StreamCapture/ThreadManager.h"
#include <thread>
#include <atomic>
class AudioPlaySession
{
public:
	AudioPlaySession(void* owner, openaudioplay_callback opencb, audio_play_callback playcb);
	
	~AudioPlaySession();
	
	void OpenAsync(AudioStream* _ias, int openIndex);

	AudioPlayParams* Open(AudioStream* _ias);

	void Start();
	
	void Release();

private:
	void ReleaseSyncLock();
	void Process();
	static void sdl_audio_callback(void *userdata, uint8_t * stream, int len);
private:
	audio_play_callback pAudioPlayCallback = NULL;
	openaudioplay_callback pOpenAudioPlayCallback = NULL;
	void* owner = NULL;
	int oindex = 0;
	std::thread process_t;
	std::atomic<int> isClosed;
	AudioStream ias;
	AudioStream oas;
	AudioPlayParams app;
	int audio_dev = -1;
	int audio_hw_buf_size = 0;
	AudioPlaySession* session = NULL;

	MUTEX startMux;
	CONDITION startCond;
	std::atomic<bool> isWakeup;

	MUTEX finishMux;
	CONDITION finishCond;

	//ͬ����
	MUTEX syncOpenMutex;
	CONDITION syncOpenCond;
};

