#include "AudioPlaySession.h"
#include <atomic>
#include "../StreamCapture/Tools.h"
#include "../SCModuleManager.h"
#include <SDL2/SDL.h>
extern "C"
{
#include <libavutil/time.h>
#include "libavutil/channel_layout.h"
#include "libavutil/common.h"

}

#define  SDL_AUDIO_MIN_BUFFER_SIZE 512
//计算实际缓冲区大小记住不要造成过于频繁的音频回调
#define SDL_AUDIO_MAX_CALLBACKS_PER_SEC 30

MUTEX _global_open_mutex;
AudioPlaySession::AudioPlaySession(void* owner, openaudioplay_callback opencb, audio_play_callback playcb)
{
	++SCModuleManager::GlobalSessionRef.audioPlay_session_c;
	this->pOpenAudioPlayCallback = opencb;
	this->pAudioPlayCallback = playcb;
	this->owner = owner;
	memset(&ias, 0, sizeof(ias));
	memset(&oas, 0, sizeof(oas));
	memset(&app, 0, sizeof(app));

	Init_Mutex(startMux);
	Init_Condition(startCond);

	Init_Mutex(finishMux);
	Init_Condition(finishCond);

	Init_Mutex(syncOpenMutex);
	Init_Condition(syncOpenCond);

	static bool isFirst = true;
	if (isFirst)
	{
		isFirst = false;
		Init_Mutex(_global_open_mutex);
	}
	session = this;

	isClosed = false;
	isWakeup = false;

}
AudioPlaySession::~AudioPlaySession()
{

	Start();
	Release();
	ReleaseSyncLock();

	if (process_t.joinable())
		process_t.join();

	Destroy_Mutex(startMux);
	Destroy_Condition(startCond);

	Destroy_Mutex(finishMux);
	Destroy_Condition(finishCond);

	Destroy_Mutex(syncOpenMutex);
	Destroy_Condition(syncOpenCond);
	++SCModuleManager::GlobalSessionRef.audioPlay_session_d;
}

void AudioPlaySession::OpenAsync(AudioStream* _ias, int openIndex)
{
	AudioStream* ias = &this->ias;
	CopyAudioStream(_ias, ias);
	oindex = openIndex;
	AudioStream* ap = &this->ias;
	//SCModuleManager::DebugLog(LEVEL_INFO, "channel_layout:%d, channels:%d, samplefmt:%d, samplerate:%d, streamCount:%d", ap->channel_layout, ap->channels, ap->samplefmt, ap->samplerate, ap->streamCount);
	process_t = std::thread(&AudioPlaySession::Process, this);
}

AudioPlayParams* AudioPlaySession::Open(AudioStream* _ias)
{
	OpenAsync(_ias, 0);
	Mutex_Lock(syncOpenMutex);
	Cond_Wait(syncOpenCond, syncOpenMutex);
	Mutex_Unlock(syncOpenMutex);
	return &app;
}

void AudioPlaySession::Start()
{
	isWakeup = true;
	Mutex_Lock(startMux);
	Cond_Signal_One(startCond);
	Mutex_Unlock(startMux);
}

void AudioPlaySession::ReleaseSyncLock()
{
	Mutex_Lock(syncOpenMutex);
	Cond_Signal_One(syncOpenCond);
	Mutex_Unlock(syncOpenMutex);
}

void AudioPlaySession::Process()
{
	if (isClosed) return;

	AudioStream* ias = &this->ias;
	app.desas = &this->oas;
	AudioPlayParams* params = &app;

	AudioStream* destas = params->desas;
	int wanted_channel_layout = ias->channel_layout;



	SDL_AudioSpec des_spec, *wanted_spec;
	wanted_spec = &des_spec;
	//wanted_spec = new SDL_AudioSpec();
	//通道数
	wanted_spec->channels = av_get_channel_layout_nb_channels(wanted_channel_layout);
	//采样率
	wanted_spec->freq = ias->samplerate;
	//音频格式 AV_SAMPLE_FMT_S16
	wanted_spec->format = AUDIO_S16SYS;
	//音频缓冲区静音值(计算)
	wanted_spec->silence = 0;
	//采样帧中的音频缓冲区大小(总采样数除以通道数)
	wanted_spec->samples = SCMAX(SDL_AUDIO_MIN_BUFFER_SIZE, 2 << av_log2(wanted_spec->freq / SDL_AUDIO_MAX_CALLBACKS_PER_SEC));
	wanted_spec->callback = sdl_audio_callback;
	wanted_spec->userdata = session;

	SDL_AudioSpec spec;

	Mutex_Lock(_global_open_mutex);
	audio_dev = SDL_OpenAudioDevice(NULL, 0, wanted_spec, &spec, SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE);
	if (audio_dev <= 0)
	{
		Mutex_Unlock(_global_open_mutex);
		SCModuleManager::DebugLog(LEVEL_ERROR, "%s", SDL_GetError());
		goto finished;
	}
	Mutex_Unlock(_global_open_mutex);

	if (spec.format != AUDIO_S16SYS)
	{
		SCModuleManager::DebugLog(LogLevel::LEVEL_ERROR, "SDL advised audio format %d is not supported!", spec.format);
		goto finished;
	}
	if (spec.channels != wanted_spec->channels)
	{
		wanted_channel_layout = av_get_default_channel_layout(spec.channels);
		if (!wanted_channel_layout)
		{
			SCModuleManager::DebugLog(LogLevel::LEVEL_ERROR, "SDL advised channel count %d is not supported!", spec.channels);
			goto finished;
		}
	}
	destas->streamCount = ias->streamCount;
	destas->channels = spec.channels;
	destas->samplerate = spec.freq;
	destas->channel_layout = wanted_channel_layout;
	destas->samplefmt = ias->samplefmt;
	audio_hw_buf_size = spec.size;
	params->hwSize = audio_hw_buf_size;

	ReleaseSyncLock();

	if (isClosed)
		goto finished;

	if (pOpenAudioPlayCallback && !isClosed)
		pOpenAudioPlayCallback(owner, oindex, params);

	if (!isWakeup)
	{
		Mutex_Lock(startMux);
		Cond_Wait(startCond, startMux);
		Mutex_Unlock(startMux);
	}
	
	SDL_PauseAudioDevice(audio_dev, 0);

	if (!isClosed)
	{
		Mutex_Lock(finishMux);
		Cond_Wait(finishCond, finishMux);
		Mutex_Unlock(finishMux);
	}

finished:
	if (audio_dev > 0)
		SDL_CloseAudioDevice(audio_dev);
	audio_dev = -1;
}

void AudioPlaySession::Release()
{
	isClosed = true;
	Mutex_Lock(finishMux);
	Cond_Signal_One(finishCond);
	Mutex_Unlock(finishMux);
}

void AudioPlaySession::sdl_audio_callback(void *userdata, uint8_t * stream, int len)
{
	AudioPlaySession* session = (AudioPlaySession*)userdata;
	session->pAudioPlayCallback(session->owner, stream, len);
}
