#pragma once
#include "../StreamCaptureDef.h"
#include <thread>
#include "../StreamCapture/ThreadManager.h"

class AudioPlaySession;
class AudioPlay
{
public:
	AudioPlay(void* owner);
	~AudioPlay();

	AudioPlayParams* Open(AudioStream* as, audio_play_callback playcb);

	void OpenAsync(AudioStream* as, int openIndex, openaudioplay_callback opencb, audio_play_callback playcb);

	void Close();

	void Start();
private:
	AudioPlaySession* session = NULL;
	void* owner = NULL;
};