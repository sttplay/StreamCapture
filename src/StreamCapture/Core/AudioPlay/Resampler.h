#pragma once
#include "../SCModuleManager.h"

struct SwrContext;
class Resampler
{
public:
	Resampler();

	~Resampler();

	int Open(AudioStream* srcas, AudioStream* destas);

	ResampleData* Resample(Frame* frame);

	void Close();

private:

	int InitSwrContext(int64_t ch_layout, int sample_fmt, int sample_rate);

private:
	SwrContext *actx = NULL;

	int sample_rate = 0;
	int channels = 0;
	unsigned long long channel_layout = 0;
	unsigned long long des_channel_layout = 0;
	int format = -1;

	//AV_SAMPLE_FMT_S16
	int sampleOutputFmt = 1;

	ResampleData rd;
};