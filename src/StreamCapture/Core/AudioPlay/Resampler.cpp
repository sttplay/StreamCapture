#include "Resampler.h"
extern "C"
{
#include <libswresample/swresample.h>
#include <libavcodec/avcodec.h>
}

Resampler::Resampler()
{
	++SCModuleManager::GlobalSessionRef.resampler_c;
	memset(&rd, 0, sizeof(ResampleData));
}

Resampler::~Resampler()
{
	Close();
	++SCModuleManager::GlobalSessionRef.resampler_d;
}



int Resampler::Open(AudioStream* srcas, AudioStream* destas)
{
	Close();

	//返回给定数量通道的默认通道布局。
	int nb_channels = av_get_channel_layout_nb_channels(srcas->channel_layout);
	this->channel_layout = (srcas->channel_layout && srcas->channels == nb_channels) ?
		srcas->channel_layout : av_get_default_channel_layout(srcas->channels);
	this->des_channel_layout = destas->channel_layout;
	this->sample_rate = destas->samplerate;
	this->channels = destas->channels;
	this->format = srcas->samplefmt;

	actx = swr_alloc();
	
	return InitSwrContext(channel_layout, format, sample_rate);
}


int Resampler::InitSwrContext(int64_t ch_layout, int sample_fmt, int sample_rate)
{
	actx = swr_alloc_set_opts(
		actx,
		des_channel_layout,								//输出格式
		(AVSampleFormat)sampleOutputFmt,					//输出样本格式  AV_SAMPLE_FMT_S16
		sample_rate,										//输出采样率
		ch_layout,									//输入格式
		(AVSampleFormat)sample_fmt,						//输入样本格式
		sample_rate,									//输入采样率
		0, NULL);

	//设置为用户参数后初始化上下文
	int ret = swr_init(actx);

	if (ret != 0)
	{
		if (actx) swr_free(&actx);
		SCModuleManager::DebugLog(LEVEL_ERROR, "swr_init failed");
		return -1;
	}

	return 0;
}

/*
 * return >0 : 执行成功，表示重采样大小
 *		  -1 : frame 为NULL
 *		  -2 : av_samples_get_buffer_size 错误
 *		  -3 ：swr_convert 失败
 *		  -4 : InitSwrContext 失败
 *		  -12: av_fast_malloc 错误
 *
 */
ResampleData* Resampler::Resample(Frame* frame)
{
	if (!frame) return NULL;

	//检验重采样器
	int dec_channel_layout =
		(frame->channel_layout && frame->channels == av_get_channel_layout_nb_channels(frame->channel_layout)) ?
		frame->channel_layout : av_get_default_channel_layout(frame->channels);
	int wanted_nb_samples = frame->nb_samples;
	//int wanted_nb_samples = synchronize_audio(is, af->frame->nb_samples);
	if (frame->format != format ||
		dec_channel_layout != channel_layout ||
		frame->sample_rate != sample_rate ||
		(frame->nb_samples != wanted_nb_samples && !actx))
	{
		swr_free(&actx);
		if (InitSwrContext(dec_channel_layout, frame->format, frame->sample_rate) != 0)
			return NULL;

		sample_rate = frame->sample_rate;
		channels = frame->channels;
		channel_layout = frame->channel_layout;
		format = frame->format;
	}
	//如果初始化了重采样，则对这一帧数据重采样输出
	const uint8_t **in = (const uint8_t **)frame->extended_data;
	uint8_t **out = &rd.data;
	int out_count = (int64_t)frame->nb_samples * sample_rate / frame->sample_rate + 256;
	int out_size = av_samples_get_buffer_size(NULL, channels, out_count, AV_SAMPLE_FMT_S16, 0);
	if (out_size < 0) {
		printf("av_samples_get_buffer_size() failed\n");
		return NULL;
	}

	unsigned int audio_buf_size = 0;
	//av_fast_malloc(&outdata, &audio_buf_size, out_size);
	av_fast_malloc(out, &audio_buf_size, out_size);
	if (!out)
		return NULL;


	int ret = swr_convert(actx, &rd.data, out_count, in, frame->nb_samples);
	if (ret < 0) {
		printf("swr_convert() failed\n");
		return NULL;
	}
	if (ret == out_count) {
		av_log(NULL, AV_LOG_WARNING, "audio buffer is probably too small\n");
		if (swr_init(actx) < 0)
			swr_free(&actx);
	}

	rd.len = ret * channels * av_get_bytes_per_sample((AVSampleFormat)sampleOutputFmt);

	return &rd;
}

void Resampler::Close()
{
	if (actx) swr_free(&actx);
	actx = NULL;

	if (rd.data)
		av_free((void*)rd.data);
	rd.data = NULL;
}