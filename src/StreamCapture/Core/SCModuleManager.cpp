#include "SCModuleManager.h"
#include "StreamCapture/StreamCapture.h"
#include <thread>
#include <SDL2/SDL.h>
extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavdevice/avdevice.h>
#include <libavutil/time.h>
}



#ifdef _WIN32

#define ENABLE_UDP_LOG_
#ifdef ENABLE_UDP_LOG
#include <windows.h>
#pragma comment(lib, "ws2_32.lib")

SOCKET _sock;
sockaddr_in _addr;

void UDPLog(const char* buf)
{
	sendto(_sock, buf, strlen(buf), 0, (SOCKADDR*)&_addr, sizeof(SOCKADDR));
}

#endif //ENABLE_UDP_LOG


#pragma comment(lib, "avformat.lib")
#pragma comment(lib, "avcodec.lib")
#pragma comment(lib, "avutil.lib")
#pragma comment(lib, "swresample.lib")
#pragma comment(lib, "swscale")
#pragma comment(lib, "avdevice.lib")
#pragma comment(lib, "SDL2.lib")

#else
#include <algorithm>
#endif //_WIN32

log_callback SCModuleManager::Logfunc = NULL;
SessionOfStatistical SCModuleManager::GlobalSessionRef;

SCModuleManager::SCModuleManager()
{
	//初始化FFmpeg
	av_register_all();
	avcodec_register_all();
	avdevice_register_all();
	avformat_network_init();

	if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_TIMER) != 0) {
		DebugLog(LEVEL_WARNING, "Could not initialize SDL - %s", SDL_GetError());
	}

#ifdef ENABLE_UDP_LOG
	WSAData wd;
	WSAStartup(MAKEWORD(2, 2), &wd) == 0;
	_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	bool b = _sock != INVALID_SOCKET;
	//服务器地址
	_addr.sin_port = htons(5555);
	_addr.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	_addr.sin_family = AF_INET;
	UDPLog("VDModule UDP LogSys Init Success!");
#endif //ENABLE_UDP_LOG
}


SCModuleManager::~SCModuleManager()
{
#ifdef ENABLE_UDP_LOG
	closesocket(_sock);
	WSACleanup();
#endif //ENABLE_UDP_LOG
}

void SCModuleManager::DebugLog(LogLevel level, const char* _Format, ...)
{
	va_list ap;
	va_start(ap, _Format);
	char buffer[LOG_BUFFER_MAX_SIZE] = { 0 };
	//vsprintf(buffer, _Format, ap);
	vsnprintf(buffer, LOG_BUFFER_MAX_SIZE - 1, _Format, ap);
#ifdef ENABLE_UDP_LOG
	UDPLog(buffer);
#endif // ENABLE_UDP_LOG
	if (Logfunc)
		Logfunc((int)level, buffer);
	else
		printf("%s\n", buffer);
	va_end(ap);
}

void SCModuleManager::InitializeStreamCapture(log_callback cb)
{
	Logfunc = cb;
	DebugLog(LEVEL_INFO, "InitializeStreamCapture OK!");
}

void SCModuleManager::TerminateStreamCapture()
{
	Logfunc = NULL;
}

void SCModuleManager::UninstallStreamCapture()
{
	while (GlobalSessionRef.audioPlay_c != GlobalSessionRef.audioPlay_d ||
		GlobalSessionRef.audioPlay_session_c != GlobalSessionRef.audioPlay_session_d ||
		GlobalSessionRef.streamCapture_c != GlobalSessionRef.streamCapture_d || 
		GlobalSessionRef.streamCapture_session_c != GlobalSessionRef.streamCapture_session_d ||
		GlobalSessionRef.resampler_c != GlobalSessionRef.resampler_d
		)
	{
		/*DebugLog(LEVEL_INFO, "sc_c:%d, sc_d:%d", GlobalSessionRef.streamCapture_c, GlobalSessionRef.streamCapture_d);
		DebugLog(LEVEL_INFO, "sc_s_c:%d, sc_s_d:%d", GlobalSessionRef.streamCapture_session_c, GlobalSessionRef.streamCapture_session_d);
		DebugLog(LEVEL_INFO, "ap_c:%d, ap_d:%d", GlobalSessionRef.audioPlay_c, GlobalSessionRef.audioPlay_d);
		DebugLog(LEVEL_INFO, "ap_s_c:%d, ap_s_d:%d", GlobalSessionRef.audioPlay_session_c, GlobalSessionRef.audioPlay_session_d);
		DebugLog(LEVEL_INFO, "re_c:%d, re_d:%d", GlobalSessionRef.resampler_c, GlobalSessionRef.resampler_d);*/
		av_usleep(10000);
	}
	SDL_Quit();
}

void* SCModuleManager::CreateStreamCapture(void* owner)
{
	return new StreamCapture(owner);
}

void SCModuleManager::DeleteStreamCapture(void* capture)
{
	StreamCapture* sc = (StreamCapture*)capture;
	std::thread([sc] {
		delete sc;
	}).detach();
}

void SCModuleManager::OpenAsync(void* capture, int openIndex, const char* url, open_callback cb)
{
	StreamCapture* sc = (StreamCapture*)capture;
	sc->OpenAsync(url, openIndex, cb);
}

StreamsParameters* SCModuleManager::Open(void* capture, const char* url)
{
	StreamCapture* sc = (StreamCapture*)capture;
	return sc->Open(url);
}

void SCModuleManager::RegisterInterruptCallback(void* capture, interrupt_callback cb)
{
	StreamCapture* sc = (StreamCapture*)capture;
	sc->RegisterInterruptCallback(cb);
}

void SCModuleManager::Close(void* capture)
{
	StreamCapture* sc = (StreamCapture*)capture;
	sc->Close();
}

Frame* SCModuleManager::TryGrabFrame(void* capture, int frameType)
{
	StreamCapture* sc = (StreamCapture*)capture;
	return sc->TryGrabFrame(frameType);
}

Frame* SCModuleManager::TryGrabLastFrame(void* capture, int frameType)
{
	StreamCapture* sc = (StreamCapture*)capture;
	return sc->TryGrabLastFrame(frameType);
}

//Frame* SCModuleManager::GrabFrame(void* capture, int frameType)
//{
//	StreamCapture* sc = (StreamCapture*)capture;
//	return sc->GrabFrame(frameType);
//}

void SCModuleManager::RemoveFrame(void* capture, int frameType)
{
	StreamCapture* sc = (StreamCapture*)capture;
	sc->RemoveFrame(frameType);
}

void SCModuleManager::LockFrame(void* capture, int frameType)
{
	StreamCapture* sc = (StreamCapture*)capture;
	sc->LockFrame(frameType);
}

void SCModuleManager::UnlockFrame(void* capture, int frameType)
{
	StreamCapture* sc = (StreamCapture*)capture;
	sc->UnlockFrame(frameType);
}

PFCounts* SCModuleManager::GetPFCounts(void* capture)
{
	StreamCapture* sc = (StreamCapture*)capture;
	return sc->GetPFCounts();
}

double SCModuleManager::GetCurrentProgress(void* capture)
{
	StreamCapture* sc = (StreamCapture*)capture;
	return sc->GetCurrentProgress();
}

long long SCModuleManager::GetDuration(void* capture)
{
	StreamCapture* sc = (StreamCapture*)capture;
	return sc->GetDuration();
}

void SCModuleManager::Seek(void* capture, double percent)
{
	StreamCapture* sc = (StreamCapture*)capture;
	sc->Seek(percent);
}

int SCModuleManager::SetOption(void* capture, int optionType, int value)
{
	StreamCapture* sc = (StreamCapture*)capture;
	return sc->SetOption(optionType, value);
}

void SCModuleManager::ClearCache(void* capture, int cacheType)
{
	StreamCapture* sc = (StreamCapture*)capture;
	return sc->ClearCache(cacheType);
}

void SCModuleManager::SetAudioTrack(void* capture, int index)
{
	StreamCapture* sc = (StreamCapture*)capture;
	sc->SetAudioTrack(index);
}

void SCModuleManager::AV_LOG_CALLBACK(void *avcl, int level, const char *fmt, va_list vl)
{
	if (avcl == SCModuleManager::GetInstance()->assist)
	{
		char buff[512] = { 0 };
		vsprintf(buff, fmt, vl);
		SCModuleManager::GetInstance()->OnReceiveCameraInfo(buff);
	}
	else
	{
		av_log_default_callback(avcl, level, fmt, vl);
	}
}

void SCModuleManager::OnReceiveCameraInfo(char* buff)
{
	std::string str(buff);
#ifdef _WIN32
	if (str.find("vcodec=mjpeg") != std::string::npos)
		cameraDesc.mjpeg = 1;
	if (str.find("pixel_format=yuyv422") != std::string::npos)
		cameraDesc.yuyv422 = 1;
	if (str.find("pixel_format=nv12") != std::string::npos)
		cameraDesc.nv12 = 1;
	if (str.find("pixel_format=bgr0") != std::string::npos)
		cameraDesc.bgr0 = 1;
#else
	if (str.find("mjpeg") != std::string::npos)
		cameraDesc.mjpeg = 1;
	if (str.find("yuyv422") != std::string::npos)
		cameraDesc.yuyv422 = 1;
#endif // _WIN32
	cameraInfo += buff;
}

CameraDescription* SCModuleManager::GetCameraInfo(const char* camera)
{
	//查看所有设备
	//ffmpeg -f dshow -list_devices true -i dumpy
	//windows上查看摄像头参数
	//ffmpeg -f dshow -list_options true -i video="aoni Webcam";
	//linux上查看摄像头参数
	//ffmpeg -f video4linux2 -list_formats all -i /dev/video0

	//windows上播放摄像头
	//ffplay -f dshow -i video="aoni Webcam"
	//linux上播放摄像头
	//ffplay -f v4l2  -i /dev/video0

	memset(&cameraDesc, 0, sizeof(CameraDescription));

	cameraInfo = "";
	assist = avformat_alloc_context();
	AVDictionary* options = NULL;
	AVInputFormat* iformat = NULL;
	/*av_dict_set(&options, "list_devices", "true", 0);
	iformat = av_find_input_format("dshow");
	int ret = avformat_open_input(&ic, "video=dumpy", iformat, &options);*/

#ifdef _WIN32
	iformat = av_find_input_format("dshow");
	av_dict_set(&options, "list_options", "true", 0);
	std::string finalUrl = std::string("video=") + std::string(camera);
#else
	iformat = av_find_input_format("video4linux2");
	av_dict_set(&options, "list_formats", "all", 0);
	std::string finalUrl = std::string(camera);
#endif // _WIN32

	av_log_set_callback(AV_LOG_CALLBACK);
	int ret = avformat_open_input(&assist, finalUrl.c_str(), iformat, &options);
	av_log_set_callback(av_log_default_callback);

	av_dict_free(&options);
	avformat_close_input(&assist);

	cameraDesc.desc = (char*)cameraInfo.c_str();
	return &cameraDesc;
}
