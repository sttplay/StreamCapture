#include "IStreamCapture.h"
#include "SCModuleManager.h"
#include "AudioPlay/AudioPlay.h"
#include "AudioPlay/Resampler.h"
#include <SDL2/SDL.h>
#include <thread>

#ifdef ENABLE_OPENCV
#include <opencv2/core.hpp>			//Mat
#include <opencv2/imgcodecs.hpp>	//imread
#include <opencv2/highgui.hpp>		//namedWindow imshow waitKey
#ifdef _DEBUG
#pragma comment(lib, "opencv_world430d.lib")
#else
#pragma comment(lib, "opencv_world430.lib")
#endif // _DEBUG
#endif
extern "C"
{
#include <libavutil/time.h>
#include <libavutil/frame.h>
}

#pragma region StreamCapture

HEAD unsigned int CallingConvention GetStreamCaptureVersion()
{
	SCModuleManager::GetInstance();
	//3.0.0	(2020/7/16) vdmodule2.0版本无法很好的适用Unity
	//3.0.1	(2020/7/24) 测试StreamCapture
	//3.1.1	(2020/8/20) VDPlayer Release版本1
	//3.2.0	(2020/9/12) SCPlayer Release测试版本（Gitee 发布版本)
	//3.2.1 (2020/10/9) SCPlayer 修复RTP，HTTP流音频播放错误
	//3.2.2 (2020/12/27) SCPlayer 修复关闭错误提示
	//3.3.0 (2021/1/16) SCPlayer 支持硬件加速
	return MAKEUINT(0, 3, 3, 0);
}

HEAD void CallingConvention InitializeStreamCapture(log_callback cb)
{
	SCModuleManager::GetInstance()->InitializeStreamCapture(cb);
}

HEAD void CallingConvention TerminateStreamCapture()
{
	SCModuleManager::GetInstance()->TerminateStreamCapture();
}

HEAD void CallingConvention UninstallStreamCapture()
{
	SCModuleManager::GetInstance()->UninstallStreamCapture();
}

HEAD void* CallingConvention CreateStreamCapture(void* owner)
{
	return SCModuleManager::GetInstance()->CreateStreamCapture(owner);
}

HEAD void CallingConvention DeleteStreamCapture(void* capture)
{
	SCModuleManager::GetInstance()->DeleteStreamCapture(capture);
}

HEAD void CallingConvention OpenAsync(void* capture, int openIndex, const char* url, open_callback cb)
{
	SCModuleManager::GetInstance()->OpenAsync(capture, openIndex, url, cb);
}

HEAD StreamsParameters* CallingConvention Open(void* capture, const char* url)
{
	return SCModuleManager::GetInstance()->Open(capture, url);
}

HEAD void CallingConvention RegisterInterruptCallback(void* capture, interrupt_callback cb)
{
	SCModuleManager::GetInstance()->RegisterInterruptCallback(capture, cb);
}

HEAD void CallingConvention Close(void* capture)
{
	SCModuleManager::GetInstance()->Close(capture);
}

HEAD Frame* CallingConvention TryGrabFrame(void* capture, int frameType)
{
	return SCModuleManager::GetInstance()->TryGrabFrame(capture, frameType);
}

HEAD Frame* CallingConvention TryGrabLastFrame(void* capture, int frameType)
{
	return SCModuleManager::GetInstance()->TryGrabLastFrame(capture, frameType);
}

//HEAD Frame* CallingConvention GrabFrame(void* capture, int frameType)
//{
//	return SCModuleManager::GetInstance()->GrabFrame(capture, frameType);
//}

HEAD void CallingConvention RemoveFrame(void* capture, int frameType)
{
	SCModuleManager::GetInstance()->RemoveFrame(capture, frameType);
}

//HEAD void CallingConvention LockFrame(void* capture, int frameType)
//{
//	SCModuleManager::GetInstance()->LockFrame(capture, frameType);
//}
//
//HEAD void CallingConvention UnlockFrame(void* capture, int frameType)
//{
//	SCModuleManager::GetInstance()->UnlockFrame(capture, frameType);
//}

HEAD PFCounts* CallingConvention GetPFCounts(void* capture)
{
	return SCModuleManager::GetInstance()->GetPFCounts(capture);
}

HEAD double CallingConvention GetCurrentProgress(void* capture)
{
	return SCModuleManager::GetInstance()->GetCurrentProgress(capture);
}

HEAD long long CallingConvention GetDuration(void* capture)
{
	return SCModuleManager::GetInstance()->GetDuration(capture);
}

HEAD void CallingConvention Seek(void* capture, double percent)
{
	SCModuleManager::GetInstance()->Seek(capture, percent);
}

HEAD int CallingConvention SetOption(void* capture, int optionType, int value)
{
	return SCModuleManager::GetInstance()->SetOption(capture, optionType, value);
}

HEAD void CallingConvention ClearCache(void* capture, int cacheType)
{
	SCModuleManager::GetInstance()->ClearCache(capture, cacheType);
}

HEAD void CallingConvention SetAudioTrack(void* capture, int index)
{
	SCModuleManager::GetInstance()->SetAudioTrack(capture, index);
}

HEAD CameraDescription* CallingConvention GetCameraInfo(const char* camera)
{
	return SCModuleManager::GetInstance()->GetCameraInfo(camera);
}
#pragma endregion StreamCapture

#pragma region Resampler

HEAD void* CallingConvention CreateResampler()
{
	return new Resampler();
}

HEAD void CallingConvention DeleteResampler(void* sampler)
{
	Resampler* s = (Resampler*)sampler;
	delete s;
}

HEAD int CallingConvention OpenResampler(void* sampler, AudioStream* srcas, AudioStream* destas)
{
	Resampler* s = (Resampler*)sampler;
	return s->Open(srcas, destas);
}

HEAD void CallingConvention CloseResampler(void* sampler)
{
	Resampler* s = (Resampler*)sampler;
	s->Close();
}

HEAD ResampleData* CallingConvention Resample(void* sampler, Frame* frame)
{
	Resampler* s = (Resampler*)sampler;
	return s->Resample(frame);
}
#pragma endregion Resampler

#pragma region AudioPlay


HEAD void* CallingConvention CreateAudioPlay(void* owner)
{
	SCModuleManager::GetInstance();
	return new AudioPlay(owner);
}

HEAD void CallingConvention DeleteAudioPlay(void* ap)
{
	SCModuleManager::GetInstance();
	AudioPlay* player = (AudioPlay*)ap;
	delete player;
}

HEAD void CallingConvention OpenAudioPlayAsync(void* ap, int openIndex, AudioStream* srcas, openaudioplay_callback opencb, audio_play_callback playcb)
{
	SCModuleManager::GetInstance();
	AudioPlay* player = (AudioPlay*)ap;
	player->OpenAsync(srcas, openIndex, opencb, playcb);
}

HEAD AudioPlayParams* CallingConvention OpenAudioPlay(void* ap, AudioStream* srcas, audio_play_callback playcb)
{
	SCModuleManager::GetInstance();
	AudioPlay* player = (AudioPlay*)ap;
	return player->Open(srcas, playcb);
}

HEAD void CallingConvention CloseAudioPlay(void* ap)
{
	SCModuleManager::GetInstance();
	AudioPlay* player = (AudioPlay*)ap;
	player->Close();
}

HEAD void CallingConvention StartAudioPlay(void* ap)
{
	SCModuleManager::GetInstance();
	AudioPlay* player = (AudioPlay*)ap;
	player->Start();
}

HEAD void CallingConvention MixAudioFormat(unsigned char* stream, const unsigned char* src, int len, float volume)
{
	int volume_i = SCMIN(SDL_MIX_MAXVOLUME, SCMAX(0, volume * SDL_MIX_MAXVOLUME));
	if (volume_i == SDL_MIX_MAXVOLUME)
	{
		memcpy(stream, src, len);
	}
	else
	{
		memset(stream, 0, len);
		SDL_MixAudioFormat(stream, src, AUDIO_S16SYS, len, volume_i);
	}
}

#pragma endregion AudioPlay

#pragma region Utility

HEAD long long CallingConvention GetTimestamp()
{
	return av_gettime_relative();
}

HEAD long long CallingConvention GetTimestampUTC()
{
	return av_gettime();
}


HEAD void* CallingConvention ArrayToPointer(void* ptr)
{
	return ptr;
}

HEAD void CallingConvention MemorySet(void* ptr, char value, int len)
{
	memset(ptr, value, len);
}

HEAD void* CallingConvention MemoryAlignment(unsigned char* srcdata, int linepixelsize, int height, int linesize, unsigned char* destdata)
{
	for (int i = 0; i < height; i++)
		memcpy(destdata + linepixelsize * i, srcdata + linesize * i, linepixelsize);
	
	return destdata;
}

HEAD void* CallingConvention CreateAVFrame()
{
	return av_frame_alloc();
}

HEAD void CallingConvention ReleaseAVFrame(void* frame)
{
	AVFrame* f = (AVFrame*)frame;
	av_frame_free(&f);
}

static void ConvertFrame(Frame* &outframe, AVFrame* inframe)
{
	memcpy(outframe->data, inframe->data, sizeof(outframe->data));
	memcpy(outframe->linesize, inframe->linesize, sizeof(outframe->linesize));
	outframe->width = inframe->width;
	outframe->height = inframe->height;
	outframe->format = inframe->format;
}

HEAD void* CallingConvention AVFrameClone(void* dst, void* src, Frame* &frame)
{
	AVFrame* dstFrame = (AVFrame*)dst;
	AVFrame* srcFrame = (AVFrame*)src;
	av_frame_free(&dstFrame);
	dstFrame = av_frame_clone(srcFrame);
	ConvertFrame(frame, dstFrame);
	return dstFrame;
}

HEAD void* CallingConvention AVFrameMoveRef(void* dst, void* src, Frame* &frame)
{
	AVFrame* dstFrame = (AVFrame*)dst;
	AVFrame* srcFrame = (AVFrame*)src;
	av_frame_unref(dstFrame);
	av_frame_move_ref(dstFrame, srcFrame);
	ConvertFrame(frame, dstFrame);
	return dstFrame;
}

#ifdef ENABLE_OPENCV
void __show__(int width, int height, uint8_t* data)
{
	cv::Mat show = cv::Mat(height, width, CV_8UC1, data);
	cv::namedWindow("show", cv::WINDOW_NORMAL);
	cv::imshow("show", show);
	cv::waitKey(1);
}
HEAD void CallingConvention CVDisplayAVFrame_Red(AVFrame* frame)
{
	if (frame->format != 23) return;
	uint8_t c1 = frame->data[0][0];
	uint8_t c2 = frame->data[0][2];
	__show__(frame->width, frame->height, frame->data[0]);
}

HEAD void CallingConvention CVDisplayFrame_Red(Frame* frame)
{
	if (frame->format != 23) return;
	uint8_t c1 = frame->data[0][0];
	uint8_t c2 = frame->data[0][2];
	__show__(frame->width, frame->height, frame->data[0]);
}
#endif
#pragma endregion Utility