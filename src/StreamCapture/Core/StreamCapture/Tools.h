#pragma once
#include <codecvt>
#include "../StreamCaptureDef.h"

#define SHOW_PKT_COUNT_
#define SHOW_FRAME_COUNT_


struct AVDictionary;

#ifdef SHOW_PKT_COUNT
#define AV_PACKET_ALLOC AV_Packet_Alloc();
#define AV_PACKET_FREE AV_Packet_Free();
void AV_Packet_Alloc();
void AV_Packet_Free();
#else
#define AV_PACKET_ALLOC
#define AV_PACKET_FREE
#endif // SHOW_PKT_COUNT

#ifdef SHOW_FRAME_COUNT
#define AV_FRAME_ALLOC AV_Frame_Alloc();
#define AV_FRAME_FREE AV_Frame_Free();
void AV_Frame_Alloc();
void AV_Frame_Free();
#else
#define AV_FRAME_ALLOC
#define AV_FRAME_FREE
#endif // SHOW_FRAME_COUNT

void TraversalAVDictionary(AVDictionary* opts);


#ifdef _WIN32
std::string static UnicodeToUTF8(const std::wstring & wstr)
{
	std::string ret;
	try {
		std::wstring_convert< std::codecvt_utf8<wchar_t> > wcv;
		ret = wcv.to_bytes(wstr);
	}
	catch (const std::exception & e) {
		printf("%s", e.what());
	}
	return ret;
}

std::wstring static UTF8ToUnicode(const std::string & str)
{
	std::wstring ret;
	try {
		std::wstring_convert< std::codecvt_utf8<wchar_t> > wcv;
		ret = wcv.from_bytes(str);
	}
	catch (const std::exception & e) {
		printf("%s", e.what());
	}
	return ret;
}

std::string static UnicodeToANSI(const std::wstring & wstr)
{
	std::string ret;
	std::mbstate_t state = {};
	const wchar_t *src = wstr.data();
	setlocale(LC_CTYPE, "");
	size_t len = std::wcsrtombs(nullptr, &src, 0, &state);
	if (static_cast<size_t>(-1) != len) {
		std::unique_ptr< char[] > buff(new char[len + 1]);
		len = std::wcsrtombs(buff.get(), &src, len, &state);
		if (static_cast<size_t>(-1) != len) {
			ret.assign(buff.get(), len);
		}
	}
	return ret;
}

std::wstring static ANSIToUnicode(const std::string & str)
{
	std::wstring ret;
	std::mbstate_t state = {};
	const char *src = str.data();
	setlocale(LC_CTYPE, "");
	size_t len = std::mbsrtowcs(nullptr, &src, 0, &state);
	if (static_cast<size_t>(-1) != len) {
		std::unique_ptr< wchar_t[] > buff(new wchar_t[len + 1]);
		len = std::mbsrtowcs(buff.get(), &src, len, &state);
		if (static_cast<size_t>(-1) != len) {
			ret.assign(buff.get(), len);
		}
	}
	return ret;
}

#endif // _WIN32

void CopyAudioStream(const AudioStream* srcas, AudioStream* &destas);