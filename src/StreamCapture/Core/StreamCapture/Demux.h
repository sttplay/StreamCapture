#pragma once
#include "ThreadManager.h"
#include <atomic>
extern "C"
{
#include <libavutil/avutil.h>
}
struct AVFormatContext;
struct AVStream;
struct AVCodecParameters;
class Session;
class PacketQueue;
class Demux
{
public:
	enum DemuxState
	{
		DS_None,
		DS_Open,
		DS_Read
	};
	enum DemuxError
	{
		DE_None,
		DE_OpenTimeout,
		DE_ReadTimeout,
		DE_ForceQuit
	};
public:
	Demux(Session* session);
	~Demux();
	int Open(const char* url, char* errbuf_ptr);
	void ReadProcess();
	AVCodecParameters* GetParameters(AVMediaType mediatype);
	AVStream* GetStream(AVMediaType mediatype);
	double GetDuration(AVMediaType mediatype);

	void MakeDemuxInfo();

	/*
	 * 跳转到指定进度
	 * @param time_ratio:[0~1]
	 */
	void StreamSeek(double percent);

	void SetAudioTrack(int index);

	void Release();
private:
	static int interrupt_callback(void* opaque);

	bool HasEnoughPackets(PacketQueue* queue);
	
	bool is_realtime(AVFormatContext* ic);

	void OnInterrupt(int ffret, int retCode);
public:
	PacketQueue* audioQueue = NULL;

	PacketQueue* videoQueue = NULL;

	bool endOfFile = false;

	AVStream* streams[5][5];

	bool clearCaches[5];

private:
	

	int audioTrackIndex = -1;

	AVFormatContext* ic = NULL;
	DemuxState demuxState = DemuxState::DS_None;
	DemuxError demuxError = DemuxError::DE_None;
	long long enterTS = 0;

	Session* session = NULL;

	//////
	int streamIndex[5][5];

	bool last_pause = false;

	MUTEX wait_mutex;
	CONDITION continue_read_thread;

	MUTEX wait_eof_proc;
	CONDITION continue_process;

	std::atomic<bool> isRelease;

	//////////////////////////////////////////////////////////////////////////
	//seek
	bool needSeek = false;
	int64_t seek_min = 0;
	int64_t seek_target = 0;
	int64_t seek_max = 0;

	double lastSeekPercent;
	bool reSeek = false;

	bool infinite_buffer = false;

	std::string mediaDescribe = "";
};

