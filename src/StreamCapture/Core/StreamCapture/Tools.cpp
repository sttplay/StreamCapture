#include "Tools.h"
#include <atomic>
#include "../SCModuleManager.h"
extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/time.h>
}


#ifdef SHOW_PKT_COUNT
std::atomic<int> alloc_pkt_count;
std::atomic<int> free_pkt_count;

void AV_Packet_Alloc()
{
	alloc_pkt_count++;
}

void AV_Packet_Free()
{
	free_pkt_count++;
	printf("alloc pkt:%d  free pkt:%d %s\n", alloc_pkt_count.load(), free_pkt_count.load(), (free_pkt_count == alloc_pkt_count ? "Yes pkt" : "No pkt"));
}
#endif //SHOW_PKT_COUNT

#ifdef SHOW_FRAME_COUNT
std::atomic<int> alloc_frame_count;
std::atomic<int> free_frame_count;

void AV_Frame_Alloc()
{
	alloc_frame_count++;
}

void AV_Frame_Free()
{
	free_frame_count++;
	printf("alloc frame:%d  free frame:%d %s\n", alloc_frame_count.load(), free_frame_count.load(), (free_frame_count == alloc_frame_count ? "Yes" : "No"));
}

#endif // SHOW_FRAME_COUNT

void TraversalAVDictionary(AVDictionary* opts)
{
	SCModuleManager::DebugLog(LEVEL_INFO, "Option dict count:%d", av_dict_count(opts));
	AVDictionaryEntry *t = NULL;
	while ((t = av_dict_get(opts, "", t, AV_DICT_IGNORE_SUFFIX))) {
		SCModuleManager::DebugLog(LEVEL_INFO, "%s: %s", t->key, t->value);
	}
}

void CopyAudioStream(const AudioStream* srcas, AudioStream* &destas)
{
	destas->channel_layout = srcas->channel_layout;
	destas->channels = srcas->channels;
	destas->samplefmt = srcas->samplefmt;
	destas->samplerate = srcas->samplerate;
	destas->streamCount = srcas->streamCount;
}
