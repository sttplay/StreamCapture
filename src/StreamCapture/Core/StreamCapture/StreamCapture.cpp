#include "StreamCapture.h"
#include "Session.h"
#include <thread>


StreamCapture::StreamCapture(void* owner)
{
	++SCModuleManager::GlobalSessionRef.streamCapture_c;
	this->owner = owner;
	sessionCount = new SessionCount();
}

StreamCapture::~StreamCapture()
{
	Close(true);
	++SCModuleManager::GlobalSessionRef.streamCapture_d;
}

void StreamCapture::OpenReset()
{
	Close();
	session = new Session(this);
	session->SyncSetting(&setting);
	++sessionCount->createCount;
}

void StreamCapture::OpenAsync(const char* url, int openIndex, open_callback cb)
{
	OpenReset();
	//pOpenCb = cb;
	session->OpenAsync(url, openIndex, cb);
}

StreamsParameters* StreamCapture::Open(const char* url)
{
	OpenReset();
	return session->Open(url);
}

void StreamCapture::RegisterInterruptCallback(interrupt_callback cb)
{
	pInterruptCb = cb;
	if (session)
		session->RegisterInterruptCallback(cb);
}

void StreamCapture::Close(bool isClear)
{
	if (!session)
		return;
	Session* s1 = session;
	SessionCount* sc = sessionCount;
	s1->Release();
	std::thread([s1, sc, isClear] {
		delete s1;
		++sc->deleteCount;
		if (isClear) delete sc;
	}).detach();
	session = NULL;
}

Frame* StreamCapture::TryGrabFrame(int frameType)
{
	if (session)
		return session->TryGrabFrame(frameType);
	return NULL;
}

Frame* StreamCapture::TryGrabLastFrame(int frameType)
{
	if (session)
		return session->TryGrabLastFrame(frameType);
	return NULL;
}

//Frame* StreamCapture::GrabFrame(int frameType)
//{
//	if (session)
//		return session->GrabFrame(frameType);
//	return NULL;
//}

void StreamCapture::RemoveFrame(int frameType)
{
	if (session)
		session->RemoveFrame(frameType);
}

void StreamCapture::LockFrame(int frameType)
{
	if (session)
		session->LockFrame(frameType);
}

void StreamCapture::UnlockFrame(int frameType)
{
	if (session)
		session->UnlockFrame(frameType);
}

PFCounts* StreamCapture::GetPFCounts()
{
	if (session)
		return session->GetPFCounts();
	return NULL;
}

double StreamCapture::GetCurrentProgress()
{
	if (session)
		return session->GetCurrentProgress();
	return 0;
}

int64_t StreamCapture::GetDuration()
{
	if (session)
		return session->GetDuration();
	return 0;
}

void StreamCapture::Seek(double percent)
{
	if (session)
		session->Seek(percent);
}

int StreamCapture::SetOption(int optionType, int value)
{
	Setting* setting = &this->setting;
	int ret = 0;
	switch (optionType)
	{
	case OptionType::OT_OpenTimeout:
	{
		if (value < 1000)
		{
			setting->openTimeout = 10000;
			ret = -1;
			break;
		}
		setting->openTimeout = value;
		ret = 0;
		break;
	}
	case OptionType::OT_ReadTimeout:
	{
		if (value < 500)
		{
			setting->readTimeout = 10000;
			ret = -1;
			break;
		}
		setting->readTimeout = value;
		ret = 0;
		break;
	}
	case OptionType::OT_EnableVideo:
	{
		setting->enableVideo = value;
		ret = 0;
		break;
	}
	case OptionType::OT_EnableAudio:
	{
		setting->enableAudio = value;
		ret = 0;
		break;
	}
	case OptionType::OT_OpenModel:
	{
		if (value != OPEN_MODEL_URL && value != OPEN_MODEL_CAMERA)
		{
			setting->openModel = OPEN_MODEL_URL;
			ret = -1;
			break;
		}
		setting->openModel = value;
		ret = 0;
		break;
	}
	case OptionType::OT_CameraWidth:
	{
		if (value < 0)
		{
			setting->cameraWidth = 640;
			ret = -1;
			break;
		}
		setting->cameraWidth = value;
		ret = 0;
		break;
	}
	case OptionType::OT_CameraHeight:
	{
		if (value < 0)
		{
			setting->cameraHeight = 480;
			ret = -1;
			break;
		}
		setting->cameraHeight = value;
		ret = 0;
		break;
	}
	case OptionType::OT_CameraFrameRate:
	{
		if (value < 0)
		{
			setting->cameraFrameRate = 30;
			ret = -1;
			break;
		}
		setting->cameraFrameRate = value;
		ret = 0;
		break;
	}
	case OptionType::OT_CameraInputFormatPriority:
	{
		if (value < CIFP_AUTO || value > CIFP_BGR0)
		{
			setting->cameraInputFormatPriority = CIFP_AUTO;
			ret = -1;
			break;
		}
		setting->cameraInputFormatPriority = value;
		ret = 0;
		break;
	}
	case OptionType::OT_NoBuffer:
	{
		setting->nobuffer = value;
		ret = 0;
		break;
	}
	case OptionType::OT_CodecForceLowDelay:
	{
		setting->codecforceLowDelay = value;
		ret = 0;
		break;
	}
	case OptionType::OT_OutputPixelformat:
	{
		if (value < -1)
		{
			setting->outputPixelformat = -1;
			ret = -1;
			break;
		}
		//��֤���ظ�ʽ
		//TODO
		setting->outputPixelformat = value;
		ret = 0;
		break;
	}
	case OptionType::OT_RTSPTransport:
	{
		if (value != RTSP_TRANSPORT_UDP && value != RTSP_TRANSPORT_TCP)
		{
			setting->rtspTransport = RTSP_TRANSPORT_UDP;
			ret = -1;
			break;
		}
		setting->rtspTransport = value;
		ret = 0;
		break;
	}
	case OptionType::OT_TryHardwareAccel:
	{
		if (value < HW_ACCEL_NONE || value > HW_ACCEL_QSV)
		{
			setting->tryHardwareAccel = HW_ACCEL_NONE;
			return -1;
		}
		setting->tryHardwareAccel = value;
		break;
	}
	default:
		ret = -1;
		break;
	}
	if (session)
		session->SyncSetting(setting);
	return ret;
}

void StreamCapture::ClearCache(int cacheType)
{
	if (session)
		session->ClearCache(cacheType);
}

void StreamCapture::SetAudioTrack(int index)
{
	if (session)
		session->SetAudioTrack(index);
}

