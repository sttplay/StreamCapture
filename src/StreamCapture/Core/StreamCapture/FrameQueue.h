#pragma once
#include <list>
#include "ThreadManager.h"
struct AVFrame;
struct AVStream;
struct FrameNode
{
	AVFrame* frame;
	AVFrame* outframe;
	int64_t pts;
	int serial;
	double duration;
};

#define FRAME_MAX_SIZE 16
class FrameQueue
{
public:
	FrameQueue(int maxSize);
	~FrameQueue();

	void Release();

	int Enqueue(AVFrame* frame, int64_t pts, double duration, int serial);

	FrameNode* PeekLast();
	FrameNode* Peek();
	FrameNode* PeekNext();
	void QueueNextSafe();
	int QueueRemaining();

	int Denqueue(FrameNode* &node);
	void ClearQueue();
	
public:
	int size = 0;
private:
	void QueueNext();
private:
	bool isExit = false;
	
	int maxSize = 0;

	MUTEX mutex;
	CONDITION condition;

	FrameNode frames[FRAME_MAX_SIZE];

	int windex = 0;
	//上一个显示的帧索引
	int rindex = 0;
	int rindex_shown = 0;

	AVFrame* storageFrame = NULL;
};