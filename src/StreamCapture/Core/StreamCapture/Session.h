#pragma once
#include <thread>
#include "../SCModuleManager.h"
#include "ThreadManager.h"

struct Frame;
struct FrameNode;
struct AVFrame;
struct SwsContext;
class StreamCapture;
class Demux;
class Decoder;
class Session
{
public:
	Session(StreamCapture* sc);
	~Session();

	void Release();

	void OpenAsync(const char* url, int openIndex, open_callback cb);

	StreamsParameters* Open(const char* url);

	void RegisterInterruptCallback(interrupt_callback cb);

	void SyncSetting(Setting* setting);

	/*
	 * 尝试抓取一帧数据
	 * @param frameType: 要抓取帧的类型
	 * @return 抓取成功返回Frame否则返回NULL
	 */
	Frame* TryGrabFrame(int frameType);

	Frame* TryGrabLastFrame(int frameType);

	//Frame* GrabFrame(int frameType);

	void RemoveFrame(int frameType);

	void LockFrame(int frameType);

	void UnlockFrame(int frameType);

	PFCounts* GetPFCounts();

	double GetCurrentProgress();

	int64_t GetDuration();

	void Seek(double percent);

	void ClearCache(int cacheType);

	void ClearFrame(int cacheType);

	void SetAudioTrack(int index);

	void OnInterrupt(int code);
private:

	void MainProcess();

	void UpdatePts(int frameType, FrameNode* node);

	bool ConvertPixfmt(AVFrame* &frame);

	bool CheckSupportSeek();

	static void copy_avframe_to_frame(AVFrame* src, Frame* &frame);
public:
	StreamCapture* capture = NULL;
	Setting* setting = NULL;
	Setting* baseSetting = NULL;

	bool isFinished = false;

	bool isPause = false;

	//总时长 单位ms
	int64_t duration = 0;
	//当前播放进度 单位ms
	int64_t crtpts = -1;

	int seekIndex = 0;

	StreamsParameters* streamsParams = NULL;

	bool supportSeek = false;
private:

	std::thread process_t;

	char* url_bk = NULL;

	Demux* demux = NULL;
	Decoder* adecoder = NULL;
	Decoder* vdecoder = NULL;

	PFCounts fpCounts;
	Frame frames[5];
	Frame lastFrames[5];
	bool endOfFiles[5];

	int lastOutputPixfmt = -1;
	AVFrame* targetFrame = NULL;
	SwsContext *swsCtx = NULL;
	Setting _setting;

	open_callback pOpenCb = NULL;
	interrupt_callback pInterruptCb = NULL;
	void* owner = NULL;
	int oindex = 0;

	//同步打开
	MUTEX syncOpenMutex;
	CONDITION syncOpenCond;

	char openErrorBuf[LOG_BUFFER_MAX_SIZE];
};

