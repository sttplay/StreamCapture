#pragma once
#include <list>
#include "ThreadManager.h"

extern "C"
{
#include <libavcodec/avcodec.h>
}

struct AVStream;
struct PacketNode
{
	AVPacket* pkt;
	int serial;
};
class PacketQueue
{
public:
	PacketQueue(AVRational tb);
	~PacketQueue();

	void Release();

	int EnqueueEmptyPkt();

	int EnqueueFlushPkt();

	/*
	 * ret 0 执行成功
	 * < 0 程序退出
	 */
	int Enqueue(AVPacket* pkt);

	/*
	 * ret 0 执行成功
	 * < 0 程序退出
	 */
	int Denqueue(AVPacket* &pkt, int *serial);

	void ClearQueue();

	int byteSize = 0;
	int size = 0;
	int serial = 0;
	int duration = 0;
	AVRational time_base;
	AVPacket* flush_pkt = NULL;
private:
	AVPacket flushPkt;
	std::list<PacketNode*> pktlist;
	
	MUTEX mutex;
	CONDITION condition;


	bool isExit = false;
	
};

