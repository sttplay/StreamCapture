#include "Demux.h"
#include "Session.h"
#include "StreamCapture.h"
#include "../SCModuleManager.h"
#include "PacketQueue.h"
#include "Tools.h"
#include "Dump.h"
extern "C"
{
#include <libavformat/avformat.h>
#include <libavutil/time.h>
	//#include <libavutil/avutil.h>
#include <libavutil/error.h>
}

#define INTERRUPT 1
#define CONTINUE 0

#define MIN_FRAMES 26
#define MAX_QUEUE_SIZE (15 * 1024 * 1024)

Demux::Demux(Session* session)
{
	this->session = session;
	memset(streamIndex, -1, sizeof(streamIndex));
	memset(streams, 0, sizeof(streams));
	memset(clearCaches, 0, sizeof(clearCaches));

	Init_Mutex(wait_mutex);
	Init_Condition(continue_read_thread);

	Init_Mutex(wait_eof_proc);
	Init_Condition(continue_process);

	isRelease = false;
}
Demux::~Demux()
{

	audioQueue = NULL;
	videoQueue = NULL;

	if (ic)
		avformat_close_input(&ic);

	session = NULL;

	Destroy_Mutex(wait_mutex);
	Destroy_Condition(continue_read_thread);

	Destroy_Mutex(wait_eof_proc);
	Destroy_Condition(continue_process);
}
int Demux::interrupt_callback(void* opaque)
{

	Demux* demux = (Demux*)opaque;
	if (demux->session->isFinished)
	{
		//SCModuleManager::DebugLog(LEVEL_INFO, "Session is closed!");
		demux->demuxError = DE_ForceQuit;
		return INTERRUPT;
	}
	if (demux->demuxState == DemuxState::DS_Open)
	{
		if (av_gettime() / 1000 - demux->enterTS > demux->session->setting->openTimeout)
		{
			demux->demuxError = DE_OpenTimeout;
			//SCModuleManager::DebugLog(LEVEL_WARNING, "Open time out!");
			return INTERRUPT;
		}
		else
			return CONTINUE;
	}
	else if (demux->demuxState == DemuxState::DS_Read)
	{
		if (av_gettime() / 1000 - demux->enterTS > demux->session->setting->readTimeout)
		{
			demux->demuxError = DE_ReadTimeout;
			SCModuleManager::DebugLog(LEVEL_WARNING, "Read pkt time out!");
			return INTERRUPT;
		}
		else
			return CONTINUE;

	}
	else
	{
		SCModuleManager::DebugLog(LEVEL_ERROR, "DemuxState set Error!");
		return INTERRUPT;
	}
	SCModuleManager::DebugLog(LEVEL_ERROR, "Unknow Error!");
	return CONTINUE;
}
int Demux::Open(const char* url, char* errbuf_ptr)
{
	//开始解封装
	//callback中的retcode，回调给用户的返回值
	int retcode = RETCODE_SUCCESS;
	//ffmpeg函数返回值
	int ffret = 0;

	std::string finalUrl;

	AVInputFormat *iformat = NULL;
	AVDictionary *format_opts = NULL;

	ic = avformat_alloc_context();
	Setting* setting = session->setting;
	if (!ic)
	{
		retcode = RETCODE_ALLOC_CONTEXT_FAILED;
		goto error;
	}

	ic->interrupt_callback.callback = interrupt_callback;
	ic->interrupt_callback.opaque = this;

	//AVDictionary *opts = NULL;
	//设置rtsp流已tcp协议打开
	//av_dict_set(&format_opts, "rtsp_transport", "tcp", 0);
	//网络延时时间
	//av_dict_set(&opts, "max_delay", "500", 0);
	//av_dict_set_int(&opts, "stimeout", 5000, 0);


	if (setting->openModel == OPEN_MODEL_URL)
	{
		finalUrl = std::string(url);
	}
	else if (setting->openModel == OPEN_MODEL_CAMERA)
	{
		int camW = setting->cameraWidth;
		int camH = setting->cameraHeight;
		int frameRate = setting->cameraFrameRate;
#ifdef _WIN32
		//查看支持dshow的设备
		//ffmpeg -list_devices true -f dshow -i dummy
		//ffplay -f dshow -i "video=camera name"
		//ffplay -f dshow -i "audio=microphone name"

		iformat = av_find_input_format("dshow");
		finalUrl = std::string("video=") + std::string(url);
#else
		//Plughw 后面的两个数字分别表示设备号和次设备（subdevice）号
		//ffplay -f alsa -i plughw:1,0
		// 摄像头驱动正确加载后，会自动生成/dev/video0-/dev/videon设备

		//ffplay -f video4linux2 -i /dev/video0
		//ffplay -f alsa -i plughw:1,0


		//输入的内容为列出来的条目
		//ls /dev | grep video*
		// /dev/video0	/dev/video1

		iformat = av_find_input_format("video4linux2");
		finalUrl = std::string(url);
#endif // _WIN32

		if (camW > 0 && camH > 0)
		{
			char rectsize[20] = { 0 };
			sprintf(rectsize, "%d:%d", camW, camH);
			av_dict_set(&format_opts, "video_size", rectsize, AV_DICT_DONT_OVERWRITE);
		}

		if (frameRate > 0)
		{
			av_dict_set_int(&format_opts, "framerate", frameRate, AV_DICT_DONT_OVERWRITE);
		}
	}

	demuxState = DemuxState::DS_Open;
	enterTS = av_gettime() / 1000;

	if (setting->cameraInputFormatPriority != CIFP_AUTO)
	{
		if (setting->cameraInputFormatPriority == CIFP_MJPEG)
		{
			av_dict_set(&format_opts,
#ifdef _WIN32
				"vcodec",
#else
				"input_format",
#endif // _WIN32
				"mjpeg", AV_DICT_DONT_OVERWRITE);
		}
		if (setting->cameraInputFormatPriority == CIFP_YUYV422)
		{
			av_dict_set(&format_opts,
#ifdef _WIN32
				"pixel_format",
#else
				"input_format",
#endif // _WIN32
				"yuyv422", AV_DICT_DONT_OVERWRITE);
		}
		if (setting->cameraInputFormatPriority == CIFP_NV12)
		{
			av_dict_set(&format_opts,
#ifdef _WIN32
				"pixel_format",
#else
				"input_format",
#endif // _WIN32
				"nv12", AV_DICT_DONT_OVERWRITE);
		}
		if (setting->cameraInputFormatPriority == CIFP_BGR0)
		{
			av_dict_set(&format_opts,
#ifdef _WIN32
				"pixel_format",
#else
				"input_format",
#endif // _WIN32
				"bgr0", AV_DICT_DONT_OVERWRITE);
		}
	}

	if (setting->rtspTransport == RTSP_TRANSPORT_TCP)
		av_dict_set(&format_opts, "rtsp_transport", "tcp", AV_DICT_DONT_OVERWRITE);

	if (setting->nobuffer)
		av_dict_set(&format_opts, "fflags", "nobuffer", AV_DICT_DONT_OVERWRITE);

	if (!av_dict_get(format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE))
		av_dict_set(&format_opts, "scan_all_pmts", "1", AV_DICT_DONT_OVERWRITE);

	//TraversalAVDictionary(format_opts);

	ffret = avformat_open_input(&ic, finalUrl.c_str(), iformat, &format_opts);

	if (ffret != 0)
	{
		//SCModuleManager::DebugLog(LEVEL_ERROR, "avformat_open_input error!");
		retcode = RETCODE_FFMPEG_LOG;
		goto error;
	}

	//读取一段视频 获取流信息
	ffret = avformat_find_stream_info(ic, NULL);
	if (ffret < 0)
	{
		//SCModuleManager::DebugLog(LEVEL_ERROR, "avformat_find_stream_info error!");
		retcode = RETCODE_FFMPEG_LOG;
		goto error;
	}
	if (!strcmp(ic->iformat->name, "swf"))
	{
		retcode = RETCODE_NOT_SUPPORT_SWF;
		goto error;
	}
	if (demuxError != DE_None)
		goto error;

	MakeDemuxInfo();
	infinite_buffer = is_realtime(ic);
	av_dict_free(&format_opts);
	mediaDescribe = AV_Dump_Format(ic, 0, ic->url, 0);
	session->streamsParams->describe = (char*)mediaDescribe.c_str();
	errbuf_ptr = NULL;
	return retcode;
error:

	if (demuxError == DE_None)
	{
		if (av_strerror(ffret, errbuf_ptr, LOG_BUFFER_MAX_SIZE) < 0)
		{
			memset(errbuf_ptr, 0, LOG_BUFFER_MAX_SIZE);
			sprintf(errbuf_ptr, "%s", strerror(AVUNERROR(ffret)));
		}
		if (retcode == RETCODE_NOT_SUPPORT_SWF)
		{
			memset(errbuf_ptr, 0, LOG_BUFFER_MAX_SIZE);
			sprintf(errbuf_ptr, "%s", "not support swf");
		}
		//SCModuleManager::DebugLog(LEVEL_ERROR, "[%s] %s!", url, errbuf_ptr);
	}
	else
	{
		if (demuxError == DE_OpenTimeout)
			retcode = RETCODE_OPEN_TIMEOUT;
		else if (demuxError == DE_ForceQuit)
			retcode = -1;
		else
			assert(false);
	}
	av_dict_free(&format_opts);
	return retcode;
}

void Demux::MakeDemuxInfo()
{
	//给streams和streamIndex赋值
	for (int i = 0; i < ic->nb_streams; i++)
	{

		AVStream* stream = ic->streams[i];
		if (stream->codecpar->codec_type == AVMEDIA_TYPE_UNKNOWN)
		{
			SCModuleManager::DebugLog(LogLevel::LEVEL_INFO, "Stream index[%d]:AVMEDIA_TYPE_UNKNOWN", i);
		}
		else if (stream->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			SCModuleManager::DebugLog(LogLevel::LEVEL_INFO, "Stream index[%d]:AVMEDIA_TYPE_VIDEO", i);
			for (int j = 0; j < 5; j++)
			{
				if (streamIndex[AVMEDIA_TYPE_VIDEO][j] == -1)
				{
					streamIndex[AVMEDIA_TYPE_VIDEO][j] = i;
					streams[AVMEDIA_TYPE_VIDEO][j] = stream;
					break;
				}
			}
		}
		else if (stream->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
		{
			SCModuleManager::DebugLog(LogLevel::LEVEL_INFO, "Stream index[%d]:AVMEDIA_TYPE_AUDIO", i);
			for (int j = 0; j < 5; j++)
			{
				if (streamIndex[AVMEDIA_TYPE_AUDIO][j] == -1)
				{
					if (audioTrackIndex == -1)
					{
						audioTrackIndex = i;
					}
					streamIndex[AVMEDIA_TYPE_AUDIO][j] = i;
					streams[AVMEDIA_TYPE_AUDIO][j] = stream;
					break;
				}
			}

		}
		else if (stream->codecpar->codec_type == AVMEDIA_TYPE_DATA)
		{
			SCModuleManager::DebugLog(LogLevel::LEVEL_INFO, "Stream index[%d]:AVMEDIA_TYPE_DATA", i);
		}
		else if (stream->codecpar->codec_type == AVMEDIA_TYPE_SUBTITLE)
		{
			SCModuleManager::DebugLog(LogLevel::LEVEL_INFO, "Stream index[%d]:AVMEDIA_TYPE_SUBTITLE", i);
		}
		else if (stream->codecpar->codec_type == AVMEDIA_TYPE_ATTACHMENT)
		{
			SCModuleManager::DebugLog(LogLevel::LEVEL_INFO, "Stream index[%d]:AVMEDIA_TYPE_ATTACHMENT", i);
		}
		else if (stream->codecpar->codec_type == AVMEDIA_TYPE_NB)
		{
			SCModuleManager::DebugLog(LogLevel::LEVEL_INFO, "Stream index[%d]:AVMEDIA_TYPE_NB", i);
		}
	}

	Setting* setting = session->setting;
	VideoStream* videoStream = NULL;
	if (streamIndex[AVMEDIA_TYPE_VIDEO][0] != -1 && setting->enableVideo)
	{
		videoStream = new VideoStream();
		videoStream->pixelwidth = streams[AVMEDIA_TYPE_VIDEO][0]->codecpar->width;
		videoStream->pixelheight = streams[AVMEDIA_TYPE_VIDEO][0]->codecpar->height;
		videoStream->fps = av_q2d(streams[AVMEDIA_TYPE_VIDEO][0]->avg_frame_rate);
		videoStream->pixelfmt = streams[AVMEDIA_TYPE_VIDEO][0]->codecpar->format;
	}

	AudioStream* audioStream = NULL;
	if (streamIndex[AVMEDIA_TYPE_AUDIO][0] != -1 && setting->enableAudio)
	{
		audioStream = new AudioStream();
		audioStream->samplerate = streams[AVMEDIA_TYPE_AUDIO][0]->codecpar->sample_rate;
		//audioStream->samplesize = 16;
		audioStream->channels = streams[AVMEDIA_TYPE_AUDIO][0]->codecpar->channels;
		audioStream->samplefmt = streams[AVMEDIA_TYPE_AUDIO][0]->codecpar->format;
		audioStream->channel_layout = streams[AVMEDIA_TYPE_AUDIO][0]->codecpar->channel_layout;

		for (int i = 0; i < 5; i++)
			if (streamIndex[AVMEDIA_TYPE_AUDIO][i] != -1) audioStream->streamCount++;
	}

	setting->enableVideo = videoStream ? 1 : 0;
	setting->enableAudio = audioStream ? 1 : 0;

	session->duration = (double)ic->duration / AV_TIME_BASE * 1000;

	session->streamsParams->vs = videoStream;
	session->streamsParams->as = audioStream;

}
void Demux::ReadProcess()
{
	demuxState = DS_Read;
	Setting* setting = session->setting;
	while (true)
	{
		if (session->isFinished)
			break;

		if (session->isPause != last_pause)
		{
			last_pause = session->isPause;
			if (session->isPause)
				av_read_pause(ic);
			else
				av_read_play(ic);
		}
		int isrtsp = !strcmp(ic->iformat->name, "rtsp");
		int ismmsh = !strncmp(ic->url, "mmsh:", 5);
		bool np = (isrtsp || (ic->pb && ismmsh));
		if (session->isPause && np) {
			/* wait 10 ms to avoid trying to get another packet */
			/* XXX: horrible */
			av_usleep(10000);
			continue;
		}

		//seek operation
		if (needSeek)
		{
			int seek_flags = 0;
			//int ret = av_seek_frame(ic, streamIndex, seekPos, seekflags);
			int ret = avformat_seek_file(ic, -1, seek_min, seek_target, seek_max, seek_flags);
			if (ret < 0)
			{
				char errbuf[128];
				char *errbuf_ptr = errbuf;

				if (av_strerror(ret, errbuf, sizeof(errbuf)) < 0)
					errbuf_ptr = strerror(AVUNERROR(ret));
				SCModuleManager::DebugLog(LEVEL_WARNING, "avformat_seek_file failed:%s", errbuf_ptr);
				/*needSeek = false;
				continue;*/
				reSeek = true;
			}

			if (setting->enableVideo) {

				videoQueue->ClearQueue();
				videoQueue->EnqueueFlushPkt();
			}
			if (setting->enableAudio)
			{
				audioQueue->ClearQueue();
				audioQueue->EnqueueFlushPkt();
			}
			needSeek = false;
		}

		if (reSeek)
		{
			StreamSeek(lastSeekPercent -= 0.02f);
			reSeek = false;
			continue;
		}

		if (clearCaches[CACHE_TYPE_VIDEO])
		{
			if (setting->enableVideo && videoQueue)
				videoQueue->ClearQueue();
			clearCaches[CACHE_TYPE_VIDEO] = false;
			session->ClearFrame(CACHE_TYPE_VIDEO);
		}

		if (clearCaches[CACHE_TYPE_AUDIO])
		{
			if (setting->enableAudio && audioQueue)
				audioQueue->ClearQueue();
			clearCaches[CACHE_TYPE_AUDIO] = false;
			session->ClearFrame(CACHE_TYPE_AUDIO);
		}

		//数据充足的时候不需要读取数据
		int byteSize = audioQueue ? audioQueue->byteSize : 0 + videoQueue ? videoQueue->byteSize : 0;
		if (!infinite_buffer && (byteSize > MAX_QUEUE_SIZE || HasEnoughPackets(audioQueue) && HasEnoughPackets(videoQueue)))
		{

			Mutex_Lock(wait_mutex);
			Cond_Wait_Timeout(continue_read_thread, wait_mutex, 10);
			Mutex_Unlock(wait_mutex);
			continue;
		}


#pragma region 读取
		AV_PACKET_ALLOC
			AVPacket* pkt = av_packet_alloc();
		enterTS = av_gettime() / 1000;
		//< 0 on error or end of file
		//printf("av_read_frame\n");
		int ret = av_read_frame(ic, pkt);
		if (demuxError != DemuxError::DE_None)
		{
			if (!session->isFinished)
			{
				if (demuxError == DemuxError::DE_ReadTimeout) OnInterrupt(0, INTERRUPT_CODE_READ_TIMEOUT);
				else if (demuxError == DemuxError::DE_ForceQuit) OnInterrupt(0, INTERRUPT_CODE_FORCE_QUIT);
				else OnInterrupt(0, INTERRUPT_CODE_UKNOW);
			}

			AV_PACKET_FREE
				av_packet_free(&pkt);
			break;
		}
		if (ret < 0)
		{
			if (ret == AVERROR_EOF || avio_feof(ic->pb) && !endOfFile)
			{

				if (videoQueue)
				{
					if (videoQueue->EnqueueEmptyPkt() < 0)
					{
						AV_PACKET_FREE
							av_packet_free(&pkt);
						OnInterrupt(0, INTERRUPT_CODE_ENQUEUE_FAILED);

						break;
					}
				}


				if (audioQueue)
				{
					if (audioQueue->EnqueueEmptyPkt() < 0)
					{
						AV_PACKET_FREE
							av_packet_free(&pkt);
						OnInterrupt(0, INTERRUPT_CODE_ENQUEUE_FAILED);
						break;
					}
				}

				endOfFile = true;
				if (!session->supportSeek)
				{
					OnInterrupt(0, INTERRUPT_CODE_EOF);
					break;
				}
			}
			else
			{

				OnInterrupt(ret, -1);
				AV_PACKET_FREE
					av_packet_free(&pkt);
				break;
			}

#pragma region FIX ME TODO
			//if (ic->pb && ic->pb->error)
			//{
			//	//There is no effect on decoding here
			//	//FIX ME TODO
			//	
			//	OnInterrupt(ret, -1);
			//	AV_PACKET_FREE
			//		av_packet_free(&pkt);
			//	break;
			//}
#pragma endregion FIX ME TODO

			Mutex_Lock(wait_mutex);
			Cond_Wait_Timeout(continue_read_thread, wait_mutex, 10);
			Mutex_Unlock(wait_mutex);
			continue;
		}
		else
		{
			endOfFile = false;
		}

		//过滤除了音频视频以外的流
		if (setting->enableAudio && pkt->stream_index == audioTrackIndex)
		{
			audioQueue->Enqueue(pkt);
			//session->internalData->audioPacketSize = audioQueue->size;
		}
		else if (setting->enableVideo && pkt->stream_index == streamIndex[AVMEDIA_TYPE_VIDEO][0])
		{
			videoQueue->Enqueue(pkt);
			//session->internalData->videoPacketSize = videoQueue->size;
		}
		else
		{
			AV_PACKET_FREE
				av_packet_free(&pkt);
		}
	}

	if (!isRelease)
	{
		Mutex_Lock(wait_eof_proc);
		Cond_Wait(continue_process, wait_eof_proc);
		Mutex_Unlock(wait_eof_proc);
	}
}

void Demux::OnInterrupt(int ffret, int retCode)
{
	if (retCode >= 0)
	{
		session->OnInterrupt(retCode);
		return;
	}
	char errbuf[LOG_BUFFER_MAX_SIZE]{ 0 };
	char* errbuf_ptr = errbuf;
	if (av_strerror(ffret, errbuf_ptr, LOG_BUFFER_MAX_SIZE) < 0)
		errbuf_ptr = strerror(AVUNERROR(ffret));
	SCModuleManager::DebugLog(LEVEL_ERROR, "%s", errbuf_ptr);
	session->OnInterrupt(INTERRUPT_CODE_UKNOW);
}


bool Demux::HasEnoughPackets(PacketQueue* queue)
{
	if (!queue) return true;
	bool isEnough = queue->size > MIN_FRAMES && (!queue->duration || av_q2d(queue->time_base) * queue->duration > 1.0);
	return isEnough;
}

bool Demux::is_realtime(AVFormatContext* ic)
{
	if (!strcmp(ic->iformat->name, "rtp")
		|| !strcmp(ic->iformat->name, "rtsp")
		|| !strcmp(ic->iformat->name, "sdp")
		)
		return true;

	if (ic->pb && (!strncmp(ic->url, "rtp:", 4)
		|| !strncmp(ic->url, "udp:", 4)
		)
		)
		return true;
	return false;
}


AVCodecParameters* Demux::GetParameters(AVMediaType mediatype)
{
	AVCodecParameters *para = NULL;
	para = avcodec_parameters_alloc();
	if (mediatype == AVMEDIA_TYPE_VIDEO)
		avcodec_parameters_copy(para, streams[mediatype][0]->codecpar);
	else if (mediatype == AVMEDIA_TYPE_AUDIO)
		avcodec_parameters_copy(para, streams[mediatype][0]->codecpar);
	else
	{
		avcodec_parameters_free(&para);
		para = NULL;
	}
	return para;
}

AVStream* Demux::GetStream(AVMediaType mediatype)
{

	if (mediatype == AVMEDIA_TYPE_VIDEO)
		return streams[mediatype][0];
	else if (mediatype == AVMEDIA_TYPE_AUDIO)
		return streams[mediatype][0];
	return NULL;
}

double Demux::GetDuration(AVMediaType mediatype)
{
	double duration = 0;
	if (mediatype == AVMEDIA_TYPE_VIDEO)
	{
		AVRational frame_rate = av_guess_frame_rate(ic, streams[mediatype][0], NULL);
		duration = (frame_rate.num && frame_rate.den ? av_q2d(AVRational{ frame_rate.den, frame_rate.num }) : 0);
	}
	else if (mediatype == AVMEDIA_TYPE_AUDIO)
	{
		AVRational frame_rate = av_guess_frame_rate(ic, streams[mediatype][0], NULL);
		duration = (frame_rate.num && frame_rate.den ? av_q2d(AVRational{ frame_rate.den, frame_rate.num }) : 0);
	}
	else
	{

	}
	return duration * 1000;
}

void Demux::StreamSeek(double percent)
{
	if (needSeek)
		return;
	percent = SCMIN(percent, 0.99);
	percent = SCMAX(percent, 0);
	lastSeekPercent = percent;

	int seek_rel = 0;
	int crt_us = session->duration * AV_TIME_BASE / 1000.0 * ((double)session->crtpts / session->duration);
	seek_target = session->duration * AV_TIME_BASE / 1000.0 * percent;
	seek_rel = seek_target - crt_us;
	/*if (seek_rel > 0) seek_rel = AV_TIME_BASE;
	else seek_rel = -AV_TIME_BASE;*/
	seek_min = seek_rel > 0 ? seek_target - seek_rel + 2 : INT64_MIN;
	seek_max = seek_rel < 0 ? seek_target - seek_rel - 2 : INT64_MAX;

	needSeek = true;
	Cond_Signal_One(continue_read_thread);
}

void Demux::SetAudioTrack(int index)
{
	if (index < 5 && streamIndex[AVMEDIA_TYPE_AUDIO][index] >= 0)
		audioTrackIndex = streamIndex[AVMEDIA_TYPE_AUDIO][index];
}

void Demux::Release()
{
	isRelease = true;
	Mutex_Lock(wait_eof_proc);
	Cond_Signal_All(continue_process);
	Mutex_Unlock(wait_eof_proc);
}
