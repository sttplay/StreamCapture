#include "Session.h"
#include "StreamCapture.h"
#include "Demux.h"
#include "Decoder.h"
#include "FrameQueue.h"
#include "PacketQueue.h"
#include "Tools.h"
extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavutil/time.h>
}
Session::Session(StreamCapture* sc)
{
	++SCModuleManager::GlobalSessionRef.streamCapture_session_c;
	this->capture = sc;
	this->owner = capture->owner;
	this->pInterruptCb = capture->pInterruptCb;
	setting = &_setting;
	baseSetting = &sc->setting;
	streamsParams = new StreamsParameters();
	streamsParams->retcode = RETCODE_UNKNOW;
	memset(frames, 0, sizeof(frames));
	memset(lastFrames, 0, sizeof(lastFrames));
	memset(endOfFiles, 0, sizeof(endOfFiles));
	Init_Mutex(syncOpenMutex);
	Init_Condition(syncOpenCond);
	//SCModuleManager::DebugLog(LEVEL_WARNING, "Session Create");
}
Session::~Session()
{
	Mutex_Lock(syncOpenMutex);
	Cond_Signal_One(syncOpenCond);
	Mutex_Unlock(syncOpenMutex);
	if (process_t.joinable())
		process_t.join();

	if (streamsParams->vs)
		delete streamsParams->vs;
	if (streamsParams->as)
		delete streamsParams->as;
	delete streamsParams;
	av_free((void*)url_bk);

	if (targetFrame)
		av_frame_free(&targetFrame);

	Destroy_Condition(syncOpenCond);
	Destroy_Mutex(syncOpenMutex);
	SCModuleManager::DebugLog(LEVEL_INFO, "Session Finished");
	++SCModuleManager::GlobalSessionRef.streamCapture_session_d;
}

void Session::Release()
{
	baseSetting = NULL;
	isFinished = true;
	if (demux) demux->Release();
}

void Session::OpenAsync(const char* url, int openIndex, open_callback cb)
{
	url_bk = av_strdup(url);
	pOpenCb = cb;
	oindex = openIndex;
	process_t = std::thread(&Session::MainProcess, this);
}

StreamsParameters* Session::Open(const char* url)
{
	OpenAsync(url, 0, NULL);
	Mutex_Lock(syncOpenMutex);
	Cond_Wait(syncOpenCond, syncOpenMutex);
	Mutex_Unlock(syncOpenMutex);
	return streamsParams;
}

void Session::RegisterInterruptCallback(interrupt_callback cb)
{
	pInterruptCb = cb;
}

void Session::SyncSetting(Setting* setting)
{
	memcpy(&_setting, setting, sizeof(Setting));
}

void Session::UpdatePts(int frameType, FrameNode* node)
{
	//更新pts
	if (frameType == FRAME_TYPE_VIDEO && setting->enableVideo)
	{
		if (node->serial == seekIndex)
			crtpts = node->pts;

		if (crtpts > duration)
			crtpts = duration;
	}
	else if (frameType == FRAME_TYPE_AUDIO && !setting->enableVideo)
	{
		if (node->serial == seekIndex)
			crtpts = node->pts;

		if (crtpts > duration)
			crtpts = duration;
	}
}



Frame* Session::TryGrabFrame(int frameType)
{
	FrameNode* node = NULL;
	Decoder* decoder = NULL;
	Frame* frame = &frames[frameType];
	if (frameType == FRAME_TYPE_VIDEO) decoder = vdecoder;
	else if (frameType == FRAME_TYPE_AUDIO) decoder = adecoder;
	if (!decoder) return NULL;

	if (!decoder->frameQueue) return NULL;
	if (decoder->frameQueue->QueueRemaining() <= 0)
	{
		node = decoder->frameQueue->PeekLast();
		if (!decoder->endOfFile /*|| decoder->last_pts != node->frame->pts*/ || endOfFiles[frameType])
			return NULL;

		if (!supportSeek) return NULL;
		//decoder->last_pts = 0;
		frame->flags = FRAME_FLAG_EOF;
		if (frameType == FRAME_TYPE_VIDEO && setting->enableVideo)
			crtpts = duration;
		else if (frameType == FRAME_TYPE_AUDIO && !setting->enableVideo)
			crtpts = duration;
		endOfFiles[frameType] = true;
		SCModuleManager::DebugLog(LEVEL_INFO, "%s end of file!", frameType == 0 ? "Video stream" : "Audio stream");
		return frame;
	}
	node = decoder->frameQueue->Peek();
	//CVDisplayAVFrame_Red(storageFrame[frameType]);
	if (node->serial != seekIndex)
	{
		RemoveFrame(frameType);
		return NULL;
	}
	endOfFiles[frameType] = false;
	UpdatePts(frameType, node);

	//LockFrame(frameType);
	if (frameType == FRAME_TYPE_VIDEO)
	{
		if (ConvertPixfmt(node->outframe)) copy_avframe_to_frame(targetFrame, frame);
		else copy_avframe_to_frame(node->outframe, frame);
	}
	else copy_avframe_to_frame(node->outframe, frame);
	frame->frameType = frameType;
	frame->pts = node->pts;
	frame->flags = FRAME_FLAG_NONE;
	frame->duration = node->duration;
	frame->avframe = node->outframe;
	//UnlockFrame(frameType);
	return frame;
}

Frame* Session::TryGrabLastFrame(int frameType)
{
	FrameNode* node = NULL;
	Decoder* decoder = NULL;
	Frame* frame = &lastFrames[frameType];
	if (frameType == FRAME_TYPE_VIDEO) decoder = vdecoder;
	else if (frameType == FRAME_TYPE_AUDIO) decoder = adecoder;
	if (!decoder) return NULL;
	node = decoder->frameQueue->PeekLast();

	copy_avframe_to_frame(node->frame, frame);
	frame->frameType = frameType;
	frame->pts = node->pts;
	frame->flags = FRAME_FLAG_NONE;
	frame->duration = node->duration;
	return frame;
}

bool Session::ConvertPixfmt(AVFrame* &frame)
{
	int outfmt = setting->outputPixelformat;
	if (outfmt < 0 || outfmt == frame->format)
		return false;

	//像素格式转换
	if (outfmt != lastOutputPixfmt)
	{
		//重置像素转换器
		lastOutputPixfmt = outfmt;

		if (targetFrame)
		{
			AV_FRAME_FREE
				av_frame_free(&targetFrame);
		}
		AV_FRAME_ALLOC
			targetFrame = av_frame_alloc();
		targetFrame = av_frame_alloc();
		targetFrame->format = (AVPixelFormat)outfmt;
		targetFrame->width = frame->width;
		targetFrame->height = frame->height;
		int ret = av_frame_get_buffer(targetFrame, 0);
		assert(!ret);


		if (swsCtx)
		{
			sws_freeContext(swsCtx);
			swsCtx = NULL;
		}
		swsCtx = sws_getCachedContext(
			swsCtx,							//传NULL会新创建 如果和之前相同会直接返回
			frame->width, frame->height,	// 输入的宽高
			(AVPixelFormat)frame->format,	// 输入的格式
			frame->width, frame->height,	// 输出的宽高
			(AVPixelFormat)outfmt,			// 输出的格式
			SWS_POINT,						// 尺寸变换的算法 临近插值算法
			0, 0, 0
		);
	}

	int line = frame->width * 4;
	int ret = sws_scale(
		swsCtx,
		frame->data,				//输入数据
		frame->linesize,			//输入行大小 考虑对齐
		0,							//从0开始
		frame->height,				//输入的高度
		targetFrame->data,			//输出的数据 
		targetFrame->linesize		//输出的大小
	);
	frame->format = outfmt;
	memcpy(frame->data, targetFrame->data, sizeof(frame->data));
	memcpy(frame->linesize, targetFrame->linesize, sizeof(frame->linesize));
	return true;
}

bool Session::CheckSupportSeek()
{
	if (setting->openModel == OPEN_MODEL_CAMERA)
		return false;
	bool supportSeek = false;
	bool ishttp = !strncmp(url_bk, "http", 4);

	const char* url = NULL;
#ifdef _WIN32
	std::string asciCode = UnicodeToANSI(UTF8ToUnicode(std::string(url_bk)));
	url = asciCode.c_str();
#else
	url = url_bk;
#endif // _WIN32

	FILE* probeFile = NULL;

	probeFile = fopen(url, "r");
	if (probeFile)
		fclose(probeFile);

	//returns true if and only if http or local file
	if (probeFile || ishttp)
		supportSeek = true;
	return supportSeek;
}

void Session::copy_avframe_to_frame(AVFrame* src, Frame* &frame)
{
	frame->width = src->width;
	frame->height = src->height;
	frame->channel_layout = src->channel_layout;
	frame->channels = src->channels;
	frame->nb_samples = src->nb_samples;
	frame->sample_rate = src->sample_rate;
	frame->extended_data = src->extended_data;
	frame->format = src->format;
	memcpy(frame->data, src->data, sizeof(frame->data));
	memcpy(frame->linesize, src->linesize, sizeof(frame->linesize));
}

//Frame* Session::GrabFrame(int frameType)
//{
//	Frame* frame = NULL;
//	while (!isFinished)
//	{
//		frame = TryGrabFrame(frameType);
//		if (frame) break;
//
//		if(endOfFiles[frameType])
//			break;
//		av_usleep(5000);
//	}
//	return frame;
//}

void Session::RemoveFrame(int frameType)
{
	LockFrame(frameType);
	if (frameType == FRAME_TYPE_VIDEO)
	{
		if (vdecoder && vdecoder->frameQueue->QueueRemaining() > 0)
		{
			vdecoder->frameQueue->QueueNextSafe();
		}
	}
	else if (frameType == FRAME_TYPE_AUDIO)
	{
		if (adecoder && adecoder->frameQueue->QueueRemaining() > 0)
		{
			adecoder->frameQueue->QueueNextSafe();
		}
	}
	UnlockFrame(frameType);
}

void Session::LockFrame(int frameType)
{

}

void Session::UnlockFrame(int frameType)
{

}

PFCounts* Session::GetPFCounts()
{

	memset(&fpCounts, 0, sizeof(PFCounts));
	if (adecoder)
	{
		fpCounts.audioPktCount = adecoder->pktQueue->size;
		fpCounts.audioFrameCount = adecoder->frameQueue->size;
	}
	if (vdecoder)
	{
		fpCounts.videoPktCount = vdecoder->pktQueue->size;
		fpCounts.videoFrameCount = vdecoder->frameQueue->size;
	}
	return &fpCounts;
}

double Session::GetCurrentProgress()
{
	double d = (double)crtpts / duration;
	return SCMAX(d, 0.0000);
}

int64_t Session::GetDuration()
{
	return duration;
}

void Session::Seek(double percent)
{
	if (!supportSeek || !demux) return;
	seekIndex++;
	crtpts = duration * percent;
	demux->StreamSeek(percent);
}

void Session::ClearCache(int cacheType)
{
	if (demux)
		demux->clearCaches[cacheType] = true;
}

void Session::ClearFrame(int cacheType)
{
	if (cacheType == CACHE_TYPE_VIDEO && vdecoder)
		vdecoder->clearFrameCache = true;
	if (cacheType == CACHE_TYPE_AUDIO && adecoder)
		adecoder->clearFrameCache = true;
}

void Session::SetAudioTrack(int index)
{
	if (demux)
		demux->SetAudioTrack(index);
}

void Session::OnInterrupt(int code)
{
	if (pInterruptCb)
		pInterruptCb(owner, code);
}

void Session::MainProcess()
{
	//防止打开线程堆积
	while (capture->sessionCount->createCount - capture->sessionCount->deleteCount > 1)
	{
		if (isFinished)
		{
			SCModuleManager::DebugLog(LEVEL_WARNING, "Create new session, force quit old session!");
			return;
		}
		av_usleep(10000);
	}

	demux = new Demux(this);
	char* errBuf = openErrorBuf;
	memset(errBuf, 0, LOG_BUFFER_MAX_SIZE);
	int retcode = demux->Open(url_bk, errBuf);
	streamsParams->retcode = retcode;
	streamsParams->errLog = errBuf;
	//if (baseSetting)
	//{
	//	baseSetting->enableAudio = setting->enableAudio;
	//	baseSetting->enableVideo = setting->enableVideo;
	//}
	//通知应用层
	if (pOpenCb && retcode != -1 && !isFinished)
		pOpenCb(owner, oindex, streamsParams);

	if (!pOpenCb)
	{
		Mutex_Lock(syncOpenMutex);
		Cond_Signal_One(syncOpenCond);
		Mutex_Unlock(syncOpenMutex);
	}

	if (retcode != RETCODE_SUCCESS)
		goto finished;
	
	//判断是否支持seek
	supportSeek = CheckSupportSeek();

	//准备解码器
	//视频解码器
	if (setting->enableVideo)
	{
		vdecoder = new Decoder(this, demux->streams[AVMEDIA_TYPE_VIDEO][0], 3);
		vdecoder->duration = demux->GetDuration(AVMEDIA_TYPE_VIDEO);
		vdecoder->Start(demux->GetStream(AVMEDIA_TYPE_VIDEO));
		demux->videoQueue = vdecoder->pktQueue;

	}
	//音频解码器
	if (setting->enableAudio)
	{
		adecoder = new Decoder(this, demux->streams[AVMEDIA_TYPE_AUDIO][0], 9);
		adecoder->duration = demux->GetDuration(AVMEDIA_TYPE_AUDIO);
		adecoder->Start(demux->GetStream(AVMEDIA_TYPE_AUDIO));
		demux->audioQueue = adecoder->pktQueue;
	}
	demux->ReadProcess();

finished:
	if (demux) delete demux;
	demux = NULL;

	if (adecoder) delete adecoder;
	adecoder = NULL;

	if (vdecoder) delete vdecoder;
	vdecoder = NULL;
}

