#pragma once
#include <list>
#include <thread>
#include "ThreadManager.h"
extern "C"
{
#include <libavutil/avutil.h>
}

struct AVCodecContext;
struct AVFrame;
struct AVStream;
struct AVCodecParameters;
struct AVCodec;
struct AVBufferRef;
class Demux;
class PacketQueue;
class FrameQueue;
class Session;
class Decoder
{
public:
	Decoder(Session* session, AVStream* stream, int maxFrameSize);
	~Decoder();

	void Start(AVStream* stream);

private:

#ifdef _WIN32
	AVCodec* CheckVP8_VP9(AVStream* stream);

	void InitHardwareDecoder(AVCodec* codec, AVCodecContext* ctx);

	bool HasHardwareType(AVCodec* codec, enum AVHWDeviceType type, AVPixelFormat* hw_format);
#endif

	bool Open(AVStream* stream);

	void StartProcess(AVStream* stream);

	void Close();


	//int GetVideoFrame(AVFrame* &frame);
	int DecodeFrame(AVFrame* &frame);

public:
	PacketQueue* pktQueue = NULL;
	FrameQueue* frameQueue = NULL;
	bool endOfFile = false;
	//int64_t last_pts = 0;
	MUTEX frameMux;
	double duration = 0;
	bool clearFrameCache = false;
private:

	AVCodecContext* ctx = NULL;

	Session* session = NULL;

	AVMediaType decodecType;

	MUTEX processMux;



	std::thread process_t;

	int pkt_serial = -1;

	AVFrame* receiveGPUFrame = NULL;
	AVPixelFormat hw_format = AVPixelFormat::AV_PIX_FMT_NONE;
	AVBufferRef *hw_ctx = NULL;
};