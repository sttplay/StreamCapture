#include "Decoder.h"
#include "../SCModuleManager.h"
#include <thread>
#include "Demux.h"
#include <assert.h>
#include "PacketQueue.h"
#include "FrameQueue.h"
#include "Session.h"
#include "Tools.h"
#include "StreamCapture.h"
extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/time.h>
}

Decoder::Decoder(Session* session, AVStream* stream, int maxFrameSize)
{
	this->session = session;
	pktQueue = new PacketQueue(stream->time_base);
	frameQueue = new FrameQueue(maxFrameSize);
	pktQueue->EnqueueFlushPkt();
	Init_Mutex(processMux);
	Init_Mutex(frameMux);
}

Decoder::~Decoder()
{
	Close();
	printf("~Decoder\n");
}

/*
 * 打开解码器
 */
bool Decoder::Open(AVStream* stream)
{
	decodecType = stream->codecpar->codec_type;

	//查找解码器
	AVCodec *avcodec = NULL;
	//SCModuleManager::DebugLog(LEVEL_INFO, "HW %d", session->setting->tryHardwareAccel);
#ifdef _WIN32
	if (decodecType == AVMEDIA_TYPE_VIDEO && session->setting->tryHardwareAccel != HW_ACCEL_NONE)
		avcodec = CheckVP8_VP9(stream);
#endif // _WIN32
	if (!avcodec)
		avcodec = avcodec_find_decoder(stream->codecpar->codec_id);
	if (!avcodec)
	{
		//avcodec_parameters_free(&params);
		SCModuleManager::DebugLog(LEVEL_ERROR, "can't find video AVCodec: id=%d", stream->codecpar->codec_id);
		return false;
	}

	//创建解码器上下文
	ctx = avcodec_alloc_context3(avcodec);
	//复制解码器上下文参数
	avcodec_parameters_to_context(ctx, stream->codecpar);
	//avcodec_parameters_free(&params);

#ifdef _WIN32
	if (decodecType == AVMEDIA_TYPE_VIDEO && session->setting->tryHardwareAccel != HW_ACCEL_NONE)
	{
		InitHardwareDecoder(avcodec, ctx);
		receiveGPUFrame = av_frame_alloc();
	}
#endif // _WIN32

	if (ctx->thread_count == 1 && ctx->codec_id != AV_CODEC_ID_PNG &&
		ctx->codec_id != AV_CODEC_ID_TIFF &&
		ctx->codec_id != AV_CODEC_ID_JPEG2000 &&
		ctx->codec_id != AV_CODEC_ID_MPEG4 && ctx->codec_id != AV_CODEC_ID_WEBP)
		ctx->thread_count = 0;

	if (session->setting->codecforceLowDelay)
		ctx->flags |= AV_CODEC_FLAG_LOW_DELAY;

	int ret = avcodec_open2(ctx, NULL, NULL);
	if (ret != 0)
	{

		char errbuf[128];
		char *errbuf_ptr = errbuf;

		if (av_strerror(ret, errbuf, sizeof(errbuf)) < 0)
			errbuf_ptr = strerror(AVUNERROR(ret));

		SCModuleManager::DebugLog(LEVEL_INFO, "avcodec_open2 failed! %s", errbuf_ptr);
		return false;
	}

	if (decodecType == AVMEDIA_TYPE_VIDEO) {
		SCModuleManager::DebugLog(LEVEL_INFO, "Open video decodec success!");
	}
	else if (decodecType == AVMEDIA_TYPE_AUDIO) {
		SCModuleManager::DebugLog(LEVEL_INFO, "Open audio decodec success!");
	}
	else
	{
		SCModuleManager::DebugLog(LEVEL_INFO, "Open other decodec success!");
	}

	return true;
}

#ifdef _WIN32

AVCodec* Decoder::CheckVP8_VP9(AVStream* stream)
{
	AVCodec* codec = NULL;
	if (stream->codecpar->codec_id == AV_CODEC_ID_VP8 || stream->codecpar->codec_id == AV_CODEC_ID_VP9)
	{
		AVDictionaryEntry *tag = NULL;
		tag = av_dict_get(stream->metadata, "alpha_mode", tag,
			AV_DICT_IGNORE_SUFFIX);

		if (tag && strcmp(tag->value, "1") == 0) {
			const char *codecName = (stream->codecpar->codec_id == AV_CODEC_ID_VP8) ? "libvpx"
				: "libvpx-vp9";
			codec = avcodec_find_decoder_by_name(codecName);
		}
	}
	return codec;
}

void Decoder::InitHardwareDecoder(AVCodec* codec, AVCodecContext* ctx)
{
	AVHWDeviceType hw_priority[] = {
	AV_HWDEVICE_TYPE_D3D11VA, AV_HWDEVICE_TYPE_CUDA, AV_HWDEVICE_TYPE_DXVA2,
	AV_HWDEVICE_TYPE_VAAPI,   AV_HWDEVICE_TYPE_VDPAU,
	AV_HWDEVICE_TYPE_QSV,     AV_HWDEVICE_TYPE_NONE,
	};
	AVHWDeviceType *priority = hw_priority;
	hw_format = AVPixelFormat::AV_PIX_FMT_NONE;
	int hwtype = session->setting->tryHardwareAccel;
	if (hwtype == HW_ACCEL_AUTO)
		goto autofind;

	else
	{
		if (HasHardwareType(codec, hw_priority[hwtype - 2], &hw_format)) {
			int ret = av_hwdevice_ctx_create(&hw_ctx, hw_priority[hwtype - 2],
				NULL, NULL, 0);
			if (ret == 0)
				goto end;
		}
	}

autofind:
	
	while (*priority != AV_HWDEVICE_TYPE_NONE) {
		if (HasHardwareType(codec, *priority, &hw_format)) {
			int ret = av_hwdevice_ctx_create(&hw_ctx, *priority,
				NULL, NULL, 0);
			if (ret == 0)
				break;
		}

		priority++;
	}

end:
	if (hw_ctx) {
		ctx->hw_device_ctx = av_buffer_ref(hw_ctx);
		ctx->opaque = NULL;
	}
}

bool Decoder::HasHardwareType(AVCodec* codec, enum AVHWDeviceType type, AVPixelFormat* hw_format)
{
	for (int i = 0;; i++) {
		const AVCodecHWConfig *config = avcodec_get_hw_config(codec, i);
		if (!config) {
			break;
		}

		if (config->methods & AV_CODEC_HW_CONFIG_METHOD_HW_DEVICE_CTX &&
			config->device_type == type) {
			*hw_format = config->pix_fmt;
			return true;
		}
	}

	return false;
}
#endif // _WIN32
void Decoder::Start(AVStream* stream)
{
	process_t = std::thread(&Decoder::StartProcess, this, stream);
}


/*
 * 清理数据并释放
 */
void Decoder::Close()
{
	pktQueue->Release();
	frameQueue->Release();
	//等待线程退出
	if (process_t.joinable()) process_t.join();

	Mutex_Lock(processMux);

	if (pktQueue) delete pktQueue;
	pktQueue = NULL;
	if (frameQueue) delete frameQueue;
	frameQueue = NULL;
	if (receiveGPUFrame)
		av_frame_free(&receiveGPUFrame);
	session = NULL;
	av_buffer_unref(&hw_ctx);
	Mutex_Unlock(processMux);
	Destroy_Mutex(processMux);
	Destroy_Mutex(frameMux);
}

void Decoder::StartProcess(AVStream* stream)
{
	if (session->isFinished)
		return;

	int ret = 0;
	AV_FRAME_ALLOC
		AVFrame *frame = av_frame_alloc();
	double timebase = 0;
	if (!Open(stream))
	{
		goto finish;
	}

	timebase = av_q2d(pktQueue->time_base) * 1000;
	while (!session->isFinished)
	{
		ret = DecodeFrame(frame);
		if (ret < 0)
			break;

		if (!ret)
		{
			av_usleep(10000);
			continue;
		}

		if (clearFrameCache)
		{
			frameQueue->ClearQueue();
			clearFrameCache = false;
			continue;
		}

		if (decodecType == AVMediaType::AVMEDIA_TYPE_VIDEO)
		{

			if (receiveGPUFrame && hw_format == frame->format)
			{
				ret = av_hwframe_transfer_data(receiveGPUFrame, frame, 0);
				
				if (ret != 0) {
					av_frame_unref(frame);
					SCModuleManager::DebugLog(LEVEL_ERROR, "Hardware frame transfer data failed!");
					break;
				}
				receiveGPUFrame->pts = frame->pts;
				ret = frameQueue->Enqueue(receiveGPUFrame, receiveGPUFrame->pts * timebase, duration, session->seekIndex);
				av_frame_unref(frame);
			}
			else
				ret = frameQueue->Enqueue(frame, frame->pts * timebase, duration, session->seekIndex);
		}
		else
			ret = frameQueue->Enqueue(frame, frame->pts * timebase, duration, session->seekIndex);
		if (ret < 0)
			break;

	}

	printf("DecodeAVPacketThread exit!\n");

finish:
	AV_FRAME_FREE
		av_frame_free(&frame);
	avcodec_free_context(&ctx);
}
//int Decoder::GetVideoFrame(AVFrame*&frame)
//{
//	int got_picture;
//	if ((got_picture = DecodeFrame(frame)) < 0)
//		return -1;
//	return got_picture;
//}

int Decoder::DecodeFrame(AVFrame* &frame)
{
	int ret = AVERROR(EAGAIN);

	for (;;)
	{
		AVPacket *pkt = NULL;
		if (pktQueue->serial == pkt_serial)
		{
			do
			{
				switch (decodecType)
				{
				case AVMEDIA_TYPE_VIDEO:
					ret = avcodec_receive_frame(ctx, frame);
					if (ret >= 0) {
						frame->pts = frame->best_effort_timestamp;
						//printf("vpts:%lf\n", frame->pts * av_q2d(pktQueue->stream->time_base));
					}
					break;
				case  AVMEDIA_TYPE_AUDIO:
					ret = avcodec_receive_frame(ctx, frame);
					if (ret >= 0) {
						//printf("apts:%lf\n", frame->pts * av_q2d(pktQueue->stream->time_base));
						AVRational tb = { 1, frame->sample_rate };
						//if (frame->pts != AV_NOPTS_VALUE)
						//	//将以 "时钟基c" 表示的 数值a 转换成以 "时钟基b" 来表示
						//	frame->pts = av_rescale_q(frame->pts, d->avctx->pkt_timebase, tb);
						//else if (d->next_pts != AV_NOPTS_VALUE)
						//	frame->pts = av_rescale_q(d->next_pts, d->next_pts_tb, tb);
						//if (frame->pts != AV_NOPTS_VALUE) {
						//	d->next_pts = frame->pts + frame->nb_samples;
						//	d->next_pts_tb = tb;
						//}
					}
					break;
				}
				if (ret == AVERROR_EOF)
				{
					//printf("===============================eof=\n");
					avcodec_flush_buffers(ctx);
					endOfFile = true;
					return 0;
				}
				endOfFile = false;
				if (ret >= 0)
				{
					//last_pts = frame->pts;
					return 1;
				}
			} while (ret != AVERROR(EAGAIN));
		}

		bool invaild = false;
		do {

			if (pktQueue->Denqueue(pkt, &pkt_serial) < 0)
				return -1;
			if (pktQueue->serial != pkt_serial) {
				printf("invaild!\n");
			}
		} while (invaild);
		assert(pkt);
		//printf("pkt:%lld\n", pkt->pts);
		if (pkt->data == pktQueue->flush_pkt->data)
		{
			avcodec_flush_buffers(ctx);
			frameQueue->ClearQueue();
		}
		else
		{
			if (session->isFinished)
			{
				AV_PACKET_FREE
					av_packet_free(&pkt);
				return -1;
			}
			if (ret = avcodec_send_packet(ctx, pkt) == AVERROR(EAGAIN)) {
				assert(false);
				AV_PACKET_FREE
					av_packet_free(&pkt);
				SCModuleManager::DebugLog(LEVEL_ERROR, "Receive_frame and send_packet both returned EAGAIN, which is an API violation");
				/*d->packet_pending = 1;
				av_packet_move_ref(d->pkt, pkt);*/
			}
			AV_PACKET_FREE
				av_packet_free(&pkt);
		}
	}
}