#pragma once
#include "../StreamCaptureDef.h"

struct SessionCount
{
	int createCount;
	int deleteCount;
};
class Session;
class StreamCapture
{
public:
	StreamCapture(void* owner);
	~StreamCapture();

	void OpenAsync(const char* url, int openIndex, open_callback cb);

	StreamsParameters* Open(const char* url);

	void RegisterInterruptCallback(interrupt_callback cb);

	void Close(bool isClear = false);

	Frame* TryGrabFrame(int frameType);

	Frame* TryGrabLastFrame(int frameType);

	//Frame* GrabFrame(int frameType);

	void RemoveFrame(int frameType);

	void LockFrame(int frameType);

	void UnlockFrame(int frameType);

	PFCounts* GetPFCounts();

	double GetCurrentProgress();

	int64_t GetDuration();

	void Seek(double percent);

	int SetOption(int optionType, int value);

	void ClearCache(int cacheType);

	void SetAudioTrack(int index);

private:

	void OpenReset();

public:
	/*open_callback pOpenCb = NULL;*/
	interrupt_callback pInterruptCb = NULL;
	void* owner = NULL;

	SessionCount* sessionCount;
	Setting setting;
private:
	Session* session = NULL;
	
};

