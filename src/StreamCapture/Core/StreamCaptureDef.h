#pragma once
#include <iostream>

#define ENABLE_OPENCV_

#define CVTLL(x) (((long long)(x)) & 0xffffffff)
#define MAKEUSHORT(a, b) ((unsigned short)((CVTLL(a)) << 8 | CVTLL(b)))
#define MAKEUINT(a,b,c,d)((unsigned int)((CVTLL(MAKEUSHORT(a,b)) << 16) | CVTLL(MAKEUSHORT(c,d))))

#define FIELDOFFSET(TYPE, MEMBER) (int)(&(((TYPE*)0)->MEMBER))

#define SCMAX(a,b) ((a) > (b) ? (a) : (b))
#define SCMIN(a,b) ((a) > (b) ? (b) : (a))

#define LOG_BUFFER_MAX_SIZE 512


#pragma region 日志等级
enum LogLevel
{
	LEVEL_INFO = 0,
	LEVEL_WARNING,
	LEVEL_ERROR
};
#pragma endregion 日志等级

//Camera input format priority 相机输入格式优先级
#define CIFP_AUTO 0
#define CIFP_YUYV422 1
#define CIFP_MJPEG 2
#define CIFP_NV12 3
#define CIFP_BGR0 4

struct CameraDescription
{
	int yuyv422;
	int mjpeg;
	int nv12;
	int bgr0;
	char* desc;
};

#define OPEN_MODEL_URL 0
#define OPEN_MODEL_CAMERA 1

#define RTSP_TRANSPORT_UDP 0
#define RTSP_TRANSPORT_TCP 1
enum OptionType
{
	OT_OpenTimeout = 0,
	OT_ReadTimeout,
	OT_EnableVideo,
	OT_EnableAudio,
	OT_OpenModel,
	OT_CameraWidth,
	OT_CameraHeight,
	OT_CameraFrameRate,
	OT_CameraInputFormatPriority,
	OT_NoBuffer,
	OT_CodecForceLowDelay,
	OT_OutputPixelformat,
	OT_RTSPTransport,
	OT_TryHardwareAccel
};

enum OutPixFmt
{
	PIX_FMT_AUTO = -1,

	PIX_FMT_YUV420P = 0,
	PIX_FMT_YUYV422 = 1,
	PIX_FMT_YUV422P = 4,
	PIX_FMT_YUV444P = 5,

	PIX_FMT_GRAY8 = 8,

	PIX_FMT_YUVJ420P = 12,
	PIX_FMT_YUVJ422P = 13,
	PIX_FMT_YUVJ444P = 14,

	PIX_FMT_NV12 = 23,
	PIX_FMT_NV21 = 24,

	PIX_FMT_RGB24 = 2,
	PIX_FMT_BGR24 = 3,

	PIX_FMT_ARGB = 25,
	PIX_FMT_RGBA = 26,
	PIX_FMT_ABGR = 27,
	PIX_FMT_BGRA = 28,

};

#define HW_ACCEL_NONE		0
#define HW_ACCEL_AUTO		1
#define HW_ACCEL_D3D11VA	2
#define HW_ACCEL_CUDA		3
#define HW_ACCEL_DXVA2		4
#define HW_ACCEL_VAAPI		5
#define HW_ACCEL_VDPAU		6
#define HW_ACCEL_QSV		7

struct Setting
{
	int openTimeout = 10000;
	int readTimeout = 10000;
	int enableVideo = 1;
	int enableAudio = 1;
	int openModel = OPEN_MODEL_URL;
	int cameraWidth = 640;
	int cameraHeight = 480;
	int cameraFrameRate = 30;
	int cameraInputFormatPriority = CIFP_AUTO;
	int nobuffer = 0;
	int codecforceLowDelay = 0;
	int outputPixelformat = PIX_FMT_AUTO;
	int rtspTransport = RTSP_TRANSPORT_UDP;
	int tryHardwareAccel = HW_ACCEL_NONE;
};


#define FRAME_TYPE_VIDEO 0
#define FRAME_TYPE_AUDIO 1

#define FRAME_FLAG_NONE 0
#define FRAME_FLAG_EOF 1


#define CACHE_TYPE_VIDEO 0
#define CACHE_TYPE_AUDIO 1
struct Frame
{

	int frameType;

	int width;

	int height;

	long long channel_layout;

	int channels;

	int nb_samples;

	int sample_rate;

	uint8_t **extended_data;

	uint8_t *data[8];

	int linesize[8];

	int64_t pts;

	int format;

	int flags;

	double duration;

	struct AVFrame* avframe;
};


struct PFCounts
{
	int audioPktCount;
	int videoPktCount;
	int audioFrameCount;
	int videoFrameCount;
};

struct VideoStream
{
	/*
	 * 视频像素宽
	 */
	int pixelwidth;
	/*
	 * 视频像素高
	 */
	int pixelheight;
	/*
	 * 视频fps
	 */
	double fps;
	/*
	 * 视频像素格式
	 * 在使用硬件解码的时候并不是最终输出的像素格式
	 */
	int pixelfmt;
};

struct AudioStream
{

	int streamCount;

	/*
	 * 音频采样率
	 */
	int samplerate;

	/*
	 * 音频采样率
	 */
	//int samplesize;

	/*
	 * 音频通道数
	 */
	int channels;

	int channel_layout;

	/*
	 * 音频格式
	 */
	int samplefmt;
};

struct AudioPlayParams
{
	AudioStream* desas;
	int hwSize;
};

#pragma region 打开文件的返回值定义
#define RETCODE_UNKNOW					-10
#define RETCODE_SUCCESS					0
#define RETCODE_ALLOC_CONTEXT_FAILED	10
#define RETCODE_FFMPEG_LOG				20
#define RETCODE_OPEN_TIMEOUT			30
#define RETCODE_NOT_SUPPORT_SWF			40
#pragma endregion 打开文件的返回值定义

struct StreamsParameters
{
	int retcode;
	char* errLog;
	char* describe;
	VideoStream* vs;
	AudioStream* as;
};

typedef void(*log_callback)(int, const char*);

typedef void(*open_callback)(void*, int, StreamsParameters*);

typedef void(*interrupt_callback)(void*, int);

typedef void(*audio_play_callback)(void*, uint8_t*, int);

typedef void(*openaudioplay_callback)(void*, int, AudioPlayParams*);


struct ResampleData
{
	int len;
	unsigned char* data;
};

#pragma region 中断码定义
#define INTERRUPT_CODE_UKNOW 0
#define INTERRUPT_CODE_READ_TIMEOUT 1
#define INTERRUPT_CODE_FORCE_QUIT 2

#define INTERRUPT_CODE_ENQUEUE_FAILED 3
#define INTERRUPT_CODE_EOF 4
#pragma endregion 中断码定义