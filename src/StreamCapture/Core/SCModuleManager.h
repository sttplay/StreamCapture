#pragma once
#include "IStreamCapture.h"
#include <stdarg.h>
struct AVFormatContext;
struct SessionOfStatistical
{
	int streamCapture_c;
	int streamCapture_d;
	int streamCapture_session_c;
	int streamCapture_session_d;
	int audioPlay_c;
	int audioPlay_d;
	int audioPlay_session_c;
	int audioPlay_session_d;
	int resampler_c;
	int resampler_d;
};
class SCModuleManager
{
public:
	~SCModuleManager();
	static inline SCModuleManager* GetInstance()
	{
		static SCModuleManager instance;
		return &instance;
	}

	static void DebugLog(LogLevel level, const char* _Format, ...);

	void InitializeStreamCapture(log_callback cb);

	void TerminateStreamCapture();

	void UninstallStreamCapture();

	void* CreateStreamCapture(void* owner);

	void DeleteStreamCapture(void* capture);

	void OpenAsync(void* capture, int openIndex, const char* url, open_callback cb);

	StreamsParameters* Open(void* capture, const char* url);

	void RegisterInterruptCallback(void* capture, interrupt_callback cb);

	void Close(void* capture);

	Frame* TryGrabFrame(void* capture, int frameType);

	Frame* TryGrabLastFrame(void* capture, int frameType);

	//Frame* GrabFrame(void* capture, int frameType);

	void RemoveFrame(void* capture, int frameType);

	void LockFrame(void* capture, int frameType);

	void UnlockFrame(void* capture, int frameType);

	PFCounts* GetPFCounts(void* capture);

	double GetCurrentProgress(void* capture);

	long long GetDuration(void* capture);

	void Seek(void* capture, double percent);

	int SetOption(void* capture, int optionType, int value);

	void ClearCache(void* capture, int cacheType);

	void SetAudioTrack(void* capture, int index);

	CameraDescription* GetCameraInfo(const char* camera);

public:
	static SessionOfStatistical GlobalSessionRef;

private:
	static void AV_LOG_CALLBACK(void *avcl, int level, const char *fmt, va_list vl);
	void OnReceiveCameraInfo(char* buff);
private:
	SCModuleManager();
	static log_callback Logfunc;
	AVFormatContext * assist = NULL;
	std::string cameraInfo;
	CameraDescription cameraDesc;
};

