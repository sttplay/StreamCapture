#include "AudioPlay.h"
#include <QAudioFormat>
#include <QAudioOutput>
#include <QDebug>
#include <QThread>
#include <QtConcurrent/QtConcurrent>
#include <QIODevice>
AudioPlay::AudioPlay()
{
}

AudioPlay::~AudioPlay()
{
	if (play_t.joinable()) play_t.join();
	if (io)
	{
		io->close();
		io = NULL;
	}
	if (output)
	{
		output->stop();
		delete output;
		output = NULL;
	}
}


bool AudioPlay::Open(int sampleRate, int sampleSize, int channels)
{

	this->channels = channels;
	this->sampleSize = sampleSize;
	this->sampleRate = sampleRate;
	QAudioFormat fmt;
	fmt.setSampleRate(sampleRate);
	fmt.setSampleSize(sampleSize);
	fmt.setChannelCount(channels);
	fmt.setCodec("audio/pcm");
	fmt.setByteOrder(QAudioFormat::LittleEndian);
	fmt.setSampleType(QAudioFormat::SignedInt);

	output = new QAudioOutput(fmt);
	//output->setBufferSize(35280 * 2);
	io = output->start();
	/*bufferSize = output->bufferSize();
	periodSize = output->periodSize();*/

	play_t = std::thread(&AudioPlay::run, this);
	return true;
}

//
//int getPcmDB(const unsigned char *pcmdata, size_t size) {
//
//	int db = 0;
//	short int value = 0;
//	double sum = 0;
//
//	for (int i = 0; i < size; i += 2)
//	{
//		memcpy(&value, pcmdata + i, 2); //获取2个字节的大小（值）
//		sum += abs(value); //绝对值求和
//	}
//	sum = sum / (size / 2); //求平均值（2个字节表示一个振幅，所以振幅个数为：size/2个）
//
//	if (sum > 0)
//	{
//		db = (int)(20.0*log10(sum));
//	}
//	return db;
//
//}
//
//#define VOLUMEMAX   32767
//int SimpleCalculate_DB(short* pcmData, int sample)
//{
//	signed short ret = 0;
//	if (sample > 0) {
//		int sum = 0;
//		signed short* pos = (signed short *)pcmData;
//		for (int i = 0; i < sample; i++) {
//			sum += abs(*pos);
//			pos++;
//		}
//		ret = sum * 500.0 / (sample * VOLUMEMAX);
//		/*if (ret >= 100) {
//			ret = 100;
//		}*/
//	}
//	return ret;
//}

void AudioPlay::Suspend()
{

	if (!output)
	{
		return;
	}
	output->suspend();

}

void AudioPlay::Resume()
{

	if (!output)
	{
		return;
	}
	output->resume();
}

void AudioPlay::ClearCache()
{
	clearCache = true;
	QMutexLocker locker(&pcmMux);
	if (!output || !io)
	{
		return;
	}
	output->setVolume(0);

	pcmCache.clear();
	cond.wakeOne();

	io->reset();

	io->close();
	output->reset();
	io = output->start();
	output->setVolume(1);
}

/*
 * 阻塞模式
 * 返回false说明程序结束
 */
bool AudioPlay::Write(unsigned char* data, int len)
{
	QMutexLocker locker(&pcmMux);
	//空余空间 小于data长度
	while (bufferSize - pcmCache.size() < len && !isExit)
		cond.wait(&pcmMux);

	if (clearCache)
	{
		clearCache = false;
		return true;
	}
	if (isExit) return false;
	pcmCache.append((char*)data, len);
	cond.wakeOne();
	return true;
}


void AudioPlay::Write2(unsigned char* data, int len)
{
	QMutexLocker locker(&pcmMux);
	while (output->bytesFree() < len)
	{
		QThread::msleep(1);
	}
	io->write((char*)data, len);
}

void AudioPlay::Release()
{
	isExit = true;
	cond.wakeAll();
	qDebug() << "audio play is release!";
}
int AudioPlay::GetRemain()
{
	if (!output)
	{
		return 0;
	}
	return bufferSize - output->bytesFree() + pcmCache.size();
}
void AudioPlay::run()
{

	periodSize = output->periodSize();
	bufferSize = output->bufferSize();
	while (!isExit)
	{
		//printf("[%d]", bufferSize -  output->bytesFree());
		QMutexLocker locker(&pcmMux);
		if (pcmCache.size() < periodSize)
		{
			cond.wait(&pcmMux);
			continue;
		}
		if (output->bytesFree() < periodSize)
		{
			QThread::msleep(1);
			continue;
		}
		char* pcm = pcmCache.data();
		int size = io->write(pcm, periodSize);
		Q_ASSERT(periodSize == size);

		////计算分贝大小
		//if (calcdb)
		//{
		//	int seg = periodSize / DECIBEL_ARRAY_SIZE;
		//	for (int i = 0; i < DECIBEL_ARRAY_SIZE; i++)
		//	{
		//		decibels[i] = SimpleCalculate_DB(((short*)pcm) + i * seg, seg);
		//	}
		//}

		pcmCache.remove(0, periodSize);
		cond.wakeOne();

	}
	qDebug() << "audio thread quit!";
}

