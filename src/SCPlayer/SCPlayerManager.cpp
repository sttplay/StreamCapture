#include "SCPlayerManager.h"
#include "SCPlayer.h"
#include <QTimer>

SCPlayerManager::SCPlayerManager(QWidget *parent)
	: QWidget(parent)
{

	ui.setupUi(this);

	connect(this, &SCPlayerManager::PlayerQuit_Signal, this, &SCPlayerManager::PlayerQuit);
	QTimer* timer = new QTimer();
	connect(timer, &QTimer::timeout, this, &SCPlayerManager::timeout);
	//timer->start(2000);
	setWindowIcon(QIcon(":/Resources/img/logo.png"));
}

SCPlayerManager::~SCPlayerManager()
{
	connect(this, &SCPlayerManager::PlayerQuit_Signal, this, &SCPlayerManager::PlayerQuit);
}


void SCPlayerManager::PlayerButtonClicked(int index, QPushButton* btn)
{
	if (players[index])
	{
		btn->setText("CreatePlayer" + QString::number(index + 1));
		players[index]->Close();
		players[index]->deleteLater();
		players[index] = NULL;
	}
	else
	{
		btn->setText("DeletePlayer" + QString::number(index + 1));
		players[index] = new SCPlayer({ this, index, btn });
		players[index]->show();
	}
}


void SCPlayerManager::closeEvent(QCloseEvent *event)
{
	for (int i = 0; i < 3; i++)
	{
		if (!players[i]) continue;
		players[i]->Close();
		players[i]->deleteLater();
		players[i] = NULL;
	}
}


void SCPlayerManager::timeout()
{
	
	int randValue = (rand() % 3);
	if(randValue == 0)
		on_PlayerButton1_clicked();
	else if (randValue == 1)
		on_PlayerButton2_clicked();
	else if (randValue == 2)
		on_PlayerButton3_clicked();
}
