#pragma once
#include "../StreamCapture/Core/StreamCaptureDef.h"
#include <QString>
class Utility
{
public:
	static Frame* CloneNewFrame(const Frame* srcFrame);

	static void CopyFrame(Frame* &destFrame, const Frame* srcFrame);

	static void ReleaseFrame(const Frame** frame);

	static QString ConvertToTime(int ms, bool accurate);
};

