#pragma once
#include <QWidget>
#include <QAbstractSlider>
#include <QSlider>
#include <QMouseEvent>
#include <QStyle>
#include <QStyleOptionSlider>


class QSliderPro : public QSlider
{
	Q_OBJECT

public:
	explicit QSliderPro(QWidget *parent = 0);

	~QSliderPro();

	void setValue(int value);

signals:
	void mouseRelease(double value);

protected:
	void mousePressEvent(QMouseEvent *event);

	void mouseReleaseEvent(QMouseEvent *ev);

private slots:

signals:
	void onClick(int value);


private:

	bool isPress = false;
	
};
