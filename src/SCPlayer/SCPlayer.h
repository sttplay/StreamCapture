#pragma once

#include <QWidget>
#include "ui_SCPlayer.h"
#include "../StreamCapture/Core/IStreamCapture.h"
#include <QTime>
#include <thread>
#include <QMutex>

class QPushButton;
class SCPlayerManager;
class OpenGLCanvas;
class Clock;
class StreamInfoWidget;

struct BaseInfo
{
	SCPlayerManager* controller;
	int btnIndex;
	QPushButton* btn;
};
class SCPlayer : public QWidget
{
	Q_OBJECT

public:
	SCPlayer(BaseInfo info, QWidget *parent = Q_NULLPTR);
	~SCPlayer();

	void Close();

signals:
	void CanvasQuit_Signal();
	void OpenCallback_Signal(int openIndex, StreamsParameters* info);
	void InterruptCallback_Signal(int interruptCode);
	void OpenAudioPlayCallback_Signal(int openIndex, AudioPlayParams* params);
	void RequestCreateCanvas_Signal(int width, int height, int format);
protected:

	void dragEnterEvent(QDragEnterEvent *event);
	void dropEvent(QDropEvent *event);

private:

	void closeEvent(QCloseEvent *event);
	QString GetLastOpenDir();
	QString GetLastOpenFile();
	void WriteToLocal(QString str);

	static void opencb(void* owner, int openIndex, StreamsParameters* info)
	{
		SCPlayer* player = (SCPlayer*)owner;
		emit player->OpenCallback_Signal(openIndex, info);
		
	}

	static void interruptcb(void* owner, int interruptCode)
	{
		SCPlayer* player = (SCPlayer*)owner;
		emit player->InterruptCallback_Signal(interruptCode);
	}
	static void audiocb(void *owner, uint8_t *stream, int len)
	{
		SCPlayer* player = (SCPlayer*)owner;
		player->AudioPlayCallback(stream, len);
	}

	static void openaudiocb(void* owner, int openIndex, AudioPlayParams* param)
	{
		SCPlayer* player = (SCPlayer*)owner;
		emit player->OpenAudioPlayCallback_Signal(openIndex, param);
	}
	void AudioPlayCallback(uint8_t *stream, int len);

	void CreateCanvas(VideoStream* vs);

	void VideoLoop();
	void RefreshVideo(int *remaining_time);

	double LastDuration(Frame* lastFrame, Frame* frame);
	double ComputeTargetDelay(double delay);

	void ClampWidgetSize(const int in_width, const int in_height, int* out_width, int* out_height);

	void Seek(double per);

	void PrepareFinish();

	void Test();
private slots:
	void timeout();
	void CanvasQuit();
	void on_SelectFileBtn_clicked();
	void on_OpenBtn_clicked();
	void on_CloseBtn_clicked();
	void on_PauseBtn_clicked();
	void on_ClearCacheBtn_clicked();
	void on_OpenModelBtn_clicked();
	void on_ShowInfoBtn_clicked();
	void OpenCallback(int openIndex, StreamsParameters* info);
	void InterruptCallback(int interruptCode);
	void OpenAudioPlayCallback(int openIndex, AudioPlayParams* param);
	void OnSeekSliderRelease(double per);
	void on_AudioTrackComboBox_currentIndexChanged(int index);
	void on_OutputPixfmtComboBox_currentIndexChanged(int index);
	//void on_FormatPriorityComboBox_currentIndexChanged(int index);
	void on_VolumeSlider_valueChanged(int value);

	void on_PanoramicCB_toggled(bool state);
	void on_IsLoopCB_toggled(bool state);

	void on_CameraInfoBtn_clicked();

	void RequestCreateCanvas(int width, int height, int format);
public:
	void* capture = NULL;
	StreamInfoWidget* infoWidget = NULL;
	StreamInfoWidget* cameraInfoWidget = NULL;
private:
	Ui::SCPlayer ui;
	BaseInfo baseInfo;

	
	void* audioPlay = NULL;
	void* resampler = NULL;

	OpenGLCanvas* canvas = NULL;

	QTime calcTime;
	QByteArray pcm;

	std::thread videoLoop_t;
	bool isExit = true;
	QMutex seekMux;
	//float frameInterval = 0;
	Clock* videoClock = NULL;
	Clock* audioClock = NULL;
	Clock* masterClock = NULL;

	int bytes_per_sec = 0;
	int audio_hw_buf_size = 0;
	int64_t frame_timer = 0;

	bool isPause = false;

	QRect screenRect;

	Setting defaultSetting;

	int64_t totalMs;

	float volume = 0.5f;
	bool nosync = false;
	bool isLoop = false;

	bool firstFramePause = false;
	bool isStep = false;

	int outputpixfmt;

	///�ļ���Ϣ
	QString fileInfo;

	AudioStream* srcas = NULL;

	int captureOindex = 0;
	int apOindex = 0;

	bool prepareVideoOk = false;
	bool prepareAudioOk = false;

	long long nextts = 0;
};
