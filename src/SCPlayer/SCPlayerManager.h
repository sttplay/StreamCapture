#pragma once

#include <QtWidgets/QWidget>
#include "ui_SCPlayerManager.h"
#include <QTime>
#include <QMutex>

class AudioPlay;
class SCPlayer;

struct StreamsParameters;
class SCPlayerManager : public QWidget
{
	Q_OBJECT

public:
	SCPlayerManager(QWidget *parent = Q_NULLPTR);
	~SCPlayerManager();

private slots:
	void timeout();

private:
	Ui::SCPlayerManagerClass ui;

public:
signals:
	void PlayerQuit_Signal(int index, QPushButton* btn);

private:
	void PlayerButtonClicked(int index, QPushButton* btn);

private slots:

	void on_PlayerButton1_clicked() { PlayerButtonClicked(0, ui.PlayerButton1); }

	void on_PlayerButton2_clicked() { PlayerButtonClicked(1, ui.PlayerButton2); }

	void on_PlayerButton3_clicked() { PlayerButtonClicked(2, ui.PlayerButton3); };

	void closeEvent(QCloseEvent *event);

	void PlayerQuit(int index, QPushButton* btn) { PlayerButtonClicked(index, btn); }

private:
	SCPlayer* players[3] = { 0 };
};
