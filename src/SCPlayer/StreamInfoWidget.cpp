#include "StreamInfoWidget.h"
#include "SCPlayer.h"
StreamInfoWidget::StreamInfoWidget()
{
	ui.setupUi(this);
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowIcon(QIcon(":/Resources/img/logo.png"));
}

StreamInfoWidget::~StreamInfoWidget()
{
}

void StreamInfoWidget::ShowInfo(QString msg)
{
	ui.plainTextEdit->setPlainText(msg);
}

void StreamInfoWidget::closeEvent(QCloseEvent *event)
{
	*selfOnBase = NULL;
}
