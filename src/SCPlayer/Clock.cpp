#include "Clock.h"
#include <QtNumeric>
#include "../StreamCapture/Core/IStreamCapture.h"
#define  AV_NOSYNC_THRESHOLD 25
Clock::Clock()
{

	set_clock(NAN);
	paused = false;
}


void Clock::set_clock_at(double pts, double time)
{
	this->pts = pts;
	last_updated = time;
	pts_drift = pts - time;
	//av_usleep(10000);
	double dt = GetTimestampUTC() / 1000.0 + pts_drift;
}

void Clock::set_clock(double crtts)
{
	//utc ʱ���
	double time = GetTimestampUTC() / 1000.0;
	set_clock_at(crtts, time);
}

double Clock::get_clock()
{
	if (paused) {
		return pts;
	}
	else {
		double time = GetTimestampUTC() / 1000.0;
		double d = pts_drift + time;
		return pts_drift + time;
	}
}

void Clock::sync_clock_to_slave(Clock *slave, bool force)
{
	
	double clock = get_clock();
	double slave_clock = slave->get_clock();
	if (!qIsNaN(slave_clock) && (qIsNaN(clock) || fabs(clock - slave_clock) > AV_NOSYNC_THRESHOLD) || force)
	{
		set_clock(slave_clock);
	}
}