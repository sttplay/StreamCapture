#include "CanvasFactory.h"
#include "../../StreamCapture/Core/IStreamCapture.h"
#include "OpenGLYUV420P.h"
#include "OpenGLYUYV422.h"
#include "OpenGLYUV422P.h"
#include "OpenGLYUV444P.h"
#include "OpenGLGray.h"
#include "OpenGLNV12.h"
#include "OpenGLNV21.h"
#include "OpenGLRGB24.h"
#include "OpenGLBGR24.h"
#include "OpenGLARGB.h"
#include "OpenGLRGBA.h"
#include "OpenGLABGR.h"
#include "OpenGLBGRA.h"

OpenGLCanvas* CanvasFactory::CreateCanvas(int pixfmt, SCPlayer* owner)
{
	OpenGLCanvas* canvas = NULL;
	if (pixfmt == PIX_FMT_YUV420P || pixfmt == PIX_FMT_YUVJ420P)
		canvas = new OpenGLYUV420P(owner);
	else if (pixfmt == PIX_FMT_YUYV422)
		canvas = new OpenGLYUYV422(owner);
	else if (pixfmt == PIX_FMT_YUV422P || pixfmt == PIX_FMT_YUVJ422P)
		canvas = new OpenGLYUV422P(owner);
	else if (pixfmt == PIX_FMT_YUV444P || pixfmt == PIX_FMT_YUVJ444P)
		canvas = new OpenGLYUV444P(owner);
	else if (pixfmt == PIX_FMT_GRAY8)
		canvas = new OpenGLGray(owner);
	else if (pixfmt == PIX_FMT_NV12)
		canvas = new OpenGLNV12(owner);
	else if (pixfmt == PIX_FMT_NV21)
		canvas = new OpenGLNV21(owner);
	else if (pixfmt == PIX_FMT_RGB24)
		canvas = new OpenGLRGB24(owner);
	else if (pixfmt == PIX_FMT_BGR24)
		canvas = new OpenGLBGR24(owner);
	else if (pixfmt == PIX_FMT_ARGB)
		canvas = new OpenGLARGB(owner);
	else if (pixfmt == PIX_FMT_RGBA)
		canvas = new OpenGLRGBA(owner);
	else if (pixfmt == PIX_FMT_ABGR)
		canvas = new OpenGLABGR(owner);
	else if (pixfmt == PIX_FMT_BGRA)
		canvas = new OpenGLBGRA(owner);

	if (canvas) canvas->pixfmt = pixfmt;
	return canvas;
}
