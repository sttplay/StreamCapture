#pragma once
#include "OpenGLCanvas.h"
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

class OpenGLBGR24 : public OpenGLCanvas
{
	Q_OBJECT
public:
	OpenGLBGR24(QWidget *parent = Q_NULLPTR);
	virtual ~OpenGLBGR24();

	virtual bool InitCanvas(int width, int height);

protected:

	//初始化GL
	virtual void initializeGL();

	//刷新显示
	virtual void paintGL();


private:
	//从shader获取中的yuv变量索引
	GLuint unis[1] = { 0 };
	//opengltexture地址
	GLuint texs[1] = { 0 };
};
