#include "Camera.h"

#define PI 3.1415926

Camera::Camera()
{
	up.setX(0);
	up.setY(1);
	up.setZ(0);
	SetPosition(0, 0, 0);
	SetRotation(0, 0, 0);
}

void Camera::SetPosition(float x, float y, float z)
{
	position.setX(x);
	position.setY(y);
	position.setZ(z);
}

void Camera::SetRotation(float pitch, float yaw, float raw)
{
	float max = 85;
	if (pitch > max) pitch = max;
	if (pitch < -max) pitch = -max;
	rotation.setX(pitch);
	rotation.setY(yaw);
	rotation.setZ(raw);

	forward.setX(cos(pitch * PI / 180) * sin(yaw * PI / 180));
	forward.setY(sin(pitch * PI / 180));
	forward.setZ(cos(pitch * PI / 180) * cos(yaw * PI / 180));
	forward = forward.normalized();
}

void Camera::Translate(float deltaX, float deltaY, float deltaZ)
{
	position += forward * deltaZ;
	position += QVector3D::crossProduct(forward, up) * deltaX;
	position += up * deltaY;
}

void Camera::Rotate(float deltaPitch, float deltaYaw, float deltaRaw)
{
	SetRotation(rotation.x() + deltaPitch, rotation.y() + deltaYaw, deltaRaw);
}

QMatrix4x4 Camera::GetViewMat()
{
	QMatrix4x4 mat;
	mat.lookAt(position, position + forward, up);
	return mat;
}
