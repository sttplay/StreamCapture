#pragma once

class OpenGLCanvas;
class SCPlayer;
class CanvasFactory
{
public:
	
	static OpenGLCanvas* CreateCanvas(int pixfmt, SCPlayer* owner);
};

