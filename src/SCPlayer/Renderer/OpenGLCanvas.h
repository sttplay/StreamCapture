/*
 *说明：2020.2.20
 *
 *2020.2.22修改为可靠版本
 *
 */

#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QGLShaderProgram>
#include <QMutex>
#include <QWaitCondition>
#include "Camera.h"
#include "SCPlayer.h"
#include "Utility.h"
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

#define GET_STR(x) #x
#define INCR_Y_ANGLE 5
#define INCR_X_ANGLE 5

#define ENABLE_MOVE_

class QTimer;
class OpenGLCanvas : public QOpenGLWidget, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	OpenGLCanvas(QWidget *parent = Q_NULLPTR);
	virtual ~OpenGLCanvas();

	virtual void Set360VR(bool is360);

	virtual void closeEvent(QCloseEvent *event);

	virtual bool InitCanvas(int width, int height);

	virtual void Repaint(Frame* frame);

	static void ResetGridData(unsigned char* &data, int width, int height, int luminance_bg, int luminance);

public:
	int pixfmt = PIX_FMT_AUTO;
	int width;
	int height;
	void* avframe = NULL;
protected:

	//初始化GL
	virtual void initializeGL();

	//刷新显示
	virtual void paintGL();

	void mouseDoubleClickEvent(QMouseEvent *event);
private:

	//窗口尺寸变化
	//void resizeGL(int w, int h);

#ifdef ENABLE_MOVE
	bool isPressW = false;
	bool isPressS = false;
	bool isPressA = false;
	bool isPressD = false;
	bool isPressQ = false;
	bool isPressE = false;
	void keyPressEvent(QKeyEvent *event);
	void keyReleaseEvent(QKeyEvent *event);
#endif // ENABLE_MOVE

	QPoint lastPoint;
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);

	void CalcSphere();

private slots:
	void FlushWindowTimeout();

protected:

	//shader程序
	QGLShaderProgram program;

	QString fSCode;
	QString vSCode;

	Frame* crtFrame = NULL;

	bool is360VR = false;

	SCPlayer* owner = NULL;

	QTimer* flushWinsowTimer = NULL;

	QMutex mutex;
private:

	Camera* camera = NULL;

	QOpenGLBuffer quad_vbo, sphere_vbo;
	QOpenGLVertexArrayObject quad_vao, sphere_vao;

	bool isRepainting = false;

	float sphereVerticals[(180 / INCR_Y_ANGLE) * (360 / INCR_X_ANGLE) * 6 * 5];
	float quadVerticals[6 * 5] = {
	1.0f,	-1.0f,	0.0f, 1.0f,	1.0f,
	-1.0f,	1.0f,	0.0f, 0.0f,	0.0f,
	-1.0f,	-1.0f,	0.0f, 0.0f,	1.0f,

	1.0f,	1.0f,	0.0f, 1.0f,	0.0f,
	-1.0f,	1.0f,	0.0f, 0.0f,	0.0f,
	1.0f,	-1.0f,	0.0f, 1.0f,	1.0f
	};

	static int openCount;
};