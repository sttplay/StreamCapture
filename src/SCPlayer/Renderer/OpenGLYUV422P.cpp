#include "OpenGLYUV422P.h"
#include <QThread>
#include <QDebug>
#include <QCoreApplication>

OpenGLYUV422P::OpenGLYUV422P(QWidget *parent)
	: OpenGLCanvas(parent)
{
	qDebug() << "========== Canvase Type:YUV422P ==========";

#pragma region 着色器
	//顶点shader
	vSCode = GET_STR(
		attribute vec4 vertexIn;
	attribute vec2 textureIn;
	varying vec2 textureOut;

	uniform mat4 modelMat;
	uniform mat4 viewMat;
	uniform mat4 projMat;
	void main(void)
	{
		textureOut = textureIn;
		gl_Position = projMat * viewMat * modelMat * vertexIn;
	}
	);


	//片元shader
	fSCode = GET_STR(
		varying vec2 textureOut;
	uniform sampler2D tex_y;
	uniform sampler2D tex_u;
	uniform sampler2D tex_v;
	void main(void)
	{
		vec3 yuv;
		vec3 rgb;
		yuv.x = texture2D(tex_y, textureOut).r - 0.0625;
		yuv.y = texture2D(tex_u, textureOut).r - 0.5;
		yuv.z = texture2D(tex_v, textureOut).r - 0.5;
		/*yuv.x = 0;
		yuv.y = 0;
		yuv.z = 0;*/


		//BT601 UV
		/*rgb.x = yuv.x + 1.4075 * yuv.z;
		rgb.y = yuv.x - 0.3455 * yuv.y - 0.7169 * yuv.z;
		rgb.z = yuv.x + 1.779  * yuv.y;*/

		//BT601
		rgb.x = 1.164 *  yuv.x + 1.596 * yuv.z;
		rgb.y = 1.164 *  yuv.x - 0.392 * yuv.y - 0.812 * yuv.z;
		rgb.z = 1.164 *  yuv.x + 2.016 * yuv.y;

		/*float grey = yuv.x;
		if (grey > 0.2) grey = 1;
		else grey = 0;

		rgb = vec3(grey, grey, grey);*/

		//BT709
		/*rgb.x = 1.164 *  yuv.x + 1.792 * yuv.z;
		rgb.y = 1.164 *  yuv.x - 0.213 * yuv.y - 0.534 * yuv.z;
		rgb.z = 1.164 *  yuv.x + 2.114 * yuv.y;*/
		gl_FragColor = vec4(rgb, 1.0);
	}
	);

#pragma endregion 着色器

}

OpenGLYUV422P::~OpenGLYUV422P()
{
	if (texs[0])
		glDeleteTextures(3, texs);
}


/*
 * 通过OpenGL绘制窗口句柄hwnd不需要使用
 */
bool OpenGLYUV422P::InitCanvas(int width, int height)
{
	OpenGLCanvas::InitCanvas(width, height);
	//创建材质
	glGenTextures(3, texs);
	
	//Y
	glBindTexture(GL_TEXTURE_2D, texs[0]);
	//放大缩小过滤器，线性插值  GL_NEAREST(效率高，但马赛克严重)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//创建材质空间(显卡空间）
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL);

	//U
	glBindTexture(GL_TEXTURE_2D, texs[1]);
	//放大缩小过滤器，线性插值
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//创建材质空间(显卡空间）
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width / 2, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL);

	//V
	glBindTexture(GL_TEXTURE_2D, texs[2]);
	//放大缩小过滤器，线性插值
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//创建材质空间(显卡空间）
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width / 2, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL);

	return true;
}


void OpenGLYUV422P::initializeGL()
{
	OpenGLCanvas::initializeGL();
	//从shader获取材质索引
	unis[0] = program.uniformLocation("tex_y");
	unis[1] = program.uniformLocation("tex_u");
	unis[2] = program.uniformLocation("tex_v");
}

void OpenGLYUV422P::paintGL()
{
	if (!crtFrame) return;

	mutex.lock();
	glActiveTexture(GL_TEXTURE0);
	//绑定到材质
	glBindTexture(GL_TEXTURE_2D, texs[0]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, crtFrame->linesize[0]);
	//修改材质内容
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, crtFrame->width, crtFrame->height, GL_LUMINANCE, GL_UNSIGNED_BYTE, crtFrame->data[0]);
	//与shader变量关联
	glUniform1i(unis[0], 0);

	glActiveTexture(GL_TEXTURE1);
	//绑定到材质
	glBindTexture(GL_TEXTURE_2D, texs[1]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, crtFrame->linesize[1]);
	//修改材质内容
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, crtFrame->width / 2, crtFrame->height, GL_LUMINANCE, GL_UNSIGNED_BYTE, crtFrame->data[1]);
	//与shader变量关联
	glUniform1i(unis[1], 1);

	glActiveTexture(GL_TEXTURE2);
	//绑定到材质
	glBindTexture(GL_TEXTURE_2D, texs[2]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, crtFrame->linesize[2]);
	//修改材质内容
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, crtFrame->width / 2, crtFrame->height, GL_LUMINANCE, GL_UNSIGNED_BYTE, crtFrame->data[2]);
	//与shader变量关联
	glUniform1i(unis[2], 2);
	mutex.unlock();

	OpenGLCanvas::paintGL();
}