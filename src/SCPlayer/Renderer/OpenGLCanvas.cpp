#include "OpenGLCanvas.h"
#include <QDebug>
#include <QCoreApplication>
#include <QThread>
#include <QMouseEvent>
#include <QTimer>

int OpenGLCanvas::openCount = 0;
OpenGLCanvas::OpenGLCanvas(QWidget *parent)
	:QOpenGLWidget(NULL)
{
	owner = (SCPlayer*)parent;
	CalcSphere();

	camera = new Camera();
	camera->SetPosition(0, 0, 0);
	camera->SetRotation(0, 0);

	quad_vbo = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
	sphere_vbo = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
	setWindowIcon(QIcon(":/Resources/img/logo.png"));
	openCount++;
	flushWinsowTimer = new QTimer();
	connect(flushWinsowTimer, &QTimer::timeout, this, &OpenGLCanvas::FlushWindowTimeout);
	flushWinsowTimer->start(1000);
}

OpenGLCanvas::~OpenGLCanvas()
{
	disconnect(flushWinsowTimer, &QTimer::timeout, this, &OpenGLCanvas::FlushWindowTimeout);
	flushWinsowTimer->stop();
	flushWinsowTimer->deleteLater();
	flushWinsowTimer = NULL;

	delete camera;
	camera = NULL;

	quad_vao.destroy();
	sphere_vao.destroy();
	quad_vbo.destroy();
	sphere_vbo.destroy();

	ReleaseAVFrame(avframe);
	if (crtFrame)
		delete crtFrame;
}


void OpenGLCanvas::CalcSphere()
{
	//x = sin(Y) * cos(X)
	//y = cos(Y)
	//z = sin(Y) * sin(X)
	const float PI = 3.141593f;
	float radius = 100.0f;
	int index = 0;
	int indexUV = 0;
	float incrYradian = PI / 180 * INCR_Y_ANGLE;
	float incrXradian = PI / 180 * INCR_X_ANGLE;
	bool mirrorU = false;
	bool mirrorV = false;
	for (int i = 0; i < 180; i += INCR_Y_ANGLE)
	{
		//从+Y轴向-Y轴的偏移弧度
		float radianY = i * PI / 180.0f;
		for (int j = 0; j < 360; j += INCR_X_ANGLE)
		{
			//从+X轴向-X轴的偏移弧度
			float radianX = j * PI / 180.0f;
			//point1 右下
			sphereVerticals[index++] = radius * sin(radianY + incrYradian) * cos(radianX + incrXradian);
			sphereVerticals[index++] = radius * cos(radianY + incrYradian);
			sphereVerticals[index++] = radius * sin(radianY + incrYradian) * sin(radianX + incrXradian);
			sphereVerticals[index++] = mirrorU ? 1.0f - (j + INCR_X_ANGLE) / 360.0f : (j + INCR_X_ANGLE) / 360.0f;
			sphereVerticals[index++] = mirrorV ? 1.0f - (i + INCR_Y_ANGLE) / 180.0f : (i + INCR_Y_ANGLE) / 180.0f;

			//point3 左下
			sphereVerticals[index++] = radius * sin(radianY) * cos(radianX + incrXradian);
			sphereVerticals[index++] = radius * cos(radianY);
			sphereVerticals[index++] = radius * sin(radianY) * sin(radianX + incrXradian);
			sphereVerticals[index++] = mirrorU ? 1.0f - (j + INCR_X_ANGLE) / 360.0f : (j + INCR_X_ANGLE) / 360.0f;
			sphereVerticals[index++] = mirrorV ? 1.0f - i / 180.0f : i / 180.0f;

			//point2 左上 标准
			sphereVerticals[index++] = radius * sin(radianY) * cos(radianX);
			sphereVerticals[index++] = radius * cos(radianY);
			sphereVerticals[index++] = radius * sin(radianY) * sin(radianX);
			sphereVerticals[index++] = mirrorU ? 1.0f - j / 360.0f : j / 360.0f;
			sphereVerticals[index++] = mirrorV ? 1.0f - i / 180.0f : i / 180.0f;



			//point4 右下
			sphereVerticals[index++] = radius * sin(radianY + incrYradian) * cos(radianX + incrXradian);
			sphereVerticals[index++] = radius * cos(radianY + incrYradian);
			sphereVerticals[index++] = radius * sin(radianY + incrYradian) * sin(radianX + incrXradian);
			sphereVerticals[index++] = mirrorU ? 1.0f - (j + INCR_X_ANGLE) / 360.0f : (j + INCR_X_ANGLE) / 360.0f;
			sphereVerticals[index++] = mirrorV ? 1.0f - (i + INCR_Y_ANGLE) / 180.0f : (i + INCR_Y_ANGLE) / 180.0f;

			//point6 左上 标准
			sphereVerticals[index++] = radius * sin(radianY) * cos(radianX);
			sphereVerticals[index++] = radius * cos(radianY);
			sphereVerticals[index++] = radius * sin(radianY) * sin(radianX);
			sphereVerticals[index++] = mirrorU ? 1.0f - j / 360.0f : j / 360.0f;
			sphereVerticals[index++] = mirrorV ? 1.0f - i / 180.0f : i / 180.0f;

			//point5 右上
			sphereVerticals[index++] = radius * sin(radianY + incrYradian) * cos(radianX);
			sphereVerticals[index++] = radius * cos(radianY + incrYradian);
			sphereVerticals[index++] = radius * sin(radianY + incrYradian) * sin(radianX);
			sphereVerticals[index++] = mirrorU ? 1.0f - j / 360.0f : j / 360.0f;
			sphereVerticals[index++] = mirrorV ? 1.0f - (i + INCR_Y_ANGLE) / 180.0f : (i + INCR_Y_ANGLE) / 180.0f;
		}
	}
}


void OpenGLCanvas::FlushWindowTimeout()
{
	//qDebug() << "flush" << this->width() << "   " << this->height();
	resize(QWidget::width(), QWidget::height());
}

void OpenGLCanvas::initializeGL()
{
	CalcSphere();
	initializeOpenGLFunctions();

	if (QCoreApplication::testAttribute(Qt::AA_UseOpenGLES))
	{
		fSCode = fSCode.prepend(GET_STR(
			precision mediump float;
		));
	}
	//加载shader脚本
	bool isSuccess = program.addShaderFromSourceCode(QGLShader::Vertex, vSCode);
	if (!isSuccess) return;
	isSuccess = program.addShaderFromSourceCode(QGLShader::Fragment, fSCode);
	if (!isSuccess) return;
	//将属性名绑定到指定位置
	//program.bindAttributeLocation("vertexIn", vertexInLocation);
	//设将属性名绑定到指定位置
	//program.bindAttributeLocation("textureIn", textureInLocation);

	isSuccess = program.link();
	if (!isSuccess) return;
	isSuccess = program.bind();
	if (!isSuccess) return;

	//对齐像素字节
	//例如一个包含3个分量的rgb图像，每个分量存储在一个字节中，如果图像有199个像素，那么一行需要597个像素。如果硬件本身的体系结构是4字节排列，那么图像每一行的末尾将由额外的3个空字符进行填充达到600字节。
	//使用下列函数改变像素的存储方式
	//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);


	QOpenGLVertexArrayObject::Binder quadVAO(&quad_vao);
	quad_vbo.create();
	quad_vbo.bind();
	quad_vbo.allocate(quadVerticals, sizeof(quadVerticals));

	int attr1 = program.attributeLocation("vertexIn");
	program.setAttributeBuffer(attr1, GL_FLOAT, sizeof(GLfloat) * 0, 3, sizeof(GLfloat) * 5);
	program.enableAttributeArray(attr1);

	int attr2 = program.attributeLocation("textureIn");
	program.setAttributeBuffer(attr2, GL_FLOAT, sizeof(GLfloat) * 3, 2, sizeof(GLfloat) * 5);
	program.enableAttributeArray(attr2);
	quad_vbo.release();
	quadVAO.release();

	QOpenGLVertexArrayObject::Binder sphereVAO(&sphere_vao);

	sphere_vbo.create();
	sphere_vbo.bind();
	sphere_vbo.allocate(sphereVerticals, sizeof(sphereVerticals));

	attr1 = program.attributeLocation("vertexIn");
	program.setAttributeBuffer(attr1, GL_FLOAT, sizeof(GLfloat) * 0, 3, sizeof(GLfloat) * 5);
	program.enableAttributeArray(attr1);

	attr2 = program.attributeLocation("textureIn");
	program.setAttributeBuffer(attr2, GL_FLOAT, sizeof(GLfloat) * 3, 2, sizeof(GLfloat) * 5);
	program.enableAttributeArray(attr2);
	sphere_vbo.release();
	sphereVAO.release();

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

}

void OpenGLCanvas::paintGL()
{
	if (!crtFrame)
		return;

	//glClearColor(0, 1, 1, 1);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	QMatrix4x4 modelMat, viewMat, projMat;
	if (is360VR)
	{
#ifdef ENABLE_MOVE
		float speed = 10.0f;
		if (isPressW) camera->Translate(0, 0, speed);
		if (isPressS) camera->Translate(0, 0, -speed);
		if (isPressA) camera->Translate(-speed, 0, 0);
		if (isPressD) camera->Translate(speed, 0, 0);
		if (isPressQ) camera->Translate(0, -speed, 0);
		if (isPressE) camera->Translate(0, speed, 0);
#endif // ENABLE_MOVE
		projMat.perspective(45.0f, (float)QWidget::width() / QWidget::height(), 0.0f, 1000.0f);
		program.setUniformValue("modelMat", modelMat);
		program.setUniformValue("viewMat", camera->GetViewMat());
		program.setUniformValue("projMat", projMat);

		QOpenGLVertexArrayObject::Binder VAO(&sphere_vao);
		int drawelement = sizeof(sphereVerticals) / sizeof(sphereVerticals[0]) / 5;
		glDrawArrays(GL_TRIANGLES, 0, drawelement);
	}
	else
	{
		program.setUniformValue("modelMat", modelMat);
		program.setUniformValue("viewMat", viewMat);
		program.setUniformValue("projMat", projMat);

		QOpenGLVertexArrayObject::Binder VAO(&quad_vao);
		int drawelement = sizeof(quadVerticals) / sizeof(quadVerticals[0]) / 5;
		glDrawArrays(GL_TRIANGLES, 0, drawelement);
	}

	isRepainting = false;
}


void OpenGLCanvas::Repaint(Frame* frame)
{
	if (isRepainting) return;
	mutex.lock();
	if (!crtFrame)
	{
		avframe = CreateAVFrame();
		crtFrame = new Frame();
	}
	avframe = AVFrameMoveRef(avframe, frame->avframe, crtFrame);
	mutex.unlock();
	update();
	isRepainting = true;
}

void OpenGLCanvas::ResetGridData(unsigned char* &data, int width, int height, int luminance_bg, int luminance)
{
	memset(data, luminance_bg, width * height);
	int gridSize = 40;
	int halfOfGridSize = gridSize / 2;
	for (int y = 0; y < height; y++)
	{
		if (y % gridSize > halfOfGridSize)
		{
			int index = 0;
			for (int x = 0; x < width; x++)
			{
				if (x % gridSize < halfOfGridSize)
					*(data + (y * width + x)) = luminance;
			}
		}
		else
		{
			int index = 0;
			for (int x = 0; x < width; x++)
			{
				if (x % gridSize > halfOfGridSize)
					*(data + (y * width + x)) = luminance;
			}
		}
	}
}

void OpenGLCanvas::mouseDoubleClickEvent(QMouseEvent *event)
{
	if (isFullScreen())
		showNormal();
	else
		showFullScreen();
}

#ifdef ENABLE_MOVE
void OpenGLCanvas::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_W)
		isPressW = true;
	if (event->key() == Qt::Key_S)
		isPressS = true;
	if (event->key() == Qt::Key_A)
		isPressA = true;
	if (event->key() == Qt::Key_D)
		isPressD = true;
	if (event->key() == Qt::Key_Q)
		isPressQ = true;
	if (event->key() == Qt::Key_E)
		isPressE = true;
}

void OpenGLCanvas::keyReleaseEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_W)
		isPressW = false;
	if (event->key() == Qt::Key_S)
		isPressS = false;
	if (event->key() == Qt::Key_A)
		isPressA = false;
	if (event->key() == Qt::Key_D)
		isPressD = false;
	if (event->key() == Qt::Key_Q)
		isPressQ = false;
	if (event->key() == Qt::Key_E)
		isPressE = false;
}
#endif // ENABLE_MOVE

void OpenGLCanvas::mousePressEvent(QMouseEvent *event)
{
	lastPoint = event->pos();
}

void OpenGLCanvas::mouseMoveEvent(QMouseEvent *event)
{
	if (!is360VR) return;
	QPoint delta = event->pos() - lastPoint;
	camera->Rotate(delta.y() / 10.0f, delta.x() / 10.0f);
	lastPoint = event->pos();
	update();
}


void OpenGLCanvas::Set360VR(bool is360)
{
	is360VR = is360;
	update();
}

void OpenGLCanvas::closeEvent(QCloseEvent *event)
{
	if (!owner) return;
	emit owner->CanvasQuit_Signal();
}

bool OpenGLCanvas::InitCanvas(int width, int height)
{
	this->width = width;
	this->height = height;
	return true;
}
