#pragma once
#include "OpenGLCanvas.h"
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

class OpenGLYUV422P : public OpenGLCanvas
{
	Q_OBJECT

public:
	OpenGLYUV422P(QWidget *parent = Q_NULLPTR);
	virtual ~OpenGLYUV422P();

	virtual bool InitCanvas(int width, int height);

protected:

	//初始化GL
	virtual void initializeGL();

	//刷新显示
	virtual void paintGL();


private:
	//从shader获取中的yuv变量索引
	GLuint unis[3] = { 0 };
	//opengltexture地址
	GLuint texs[3] = { 0 };
};

