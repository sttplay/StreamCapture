#pragma once
#include "OpenGLCanvas.h"
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

class OpenGLARGB : public OpenGLCanvas
{
	Q_OBJECT

public:
	OpenGLARGB(QWidget *parent = Q_NULLPTR);
	virtual ~OpenGLARGB();

	virtual bool InitCanvas(int width, int height);

protected:

	//初始化GL
	virtual void initializeGL();

	//刷新显示
	virtual void paintGL();

private:
	//从shader获取中的yuv变量索引
	GLuint unis[2] = { 0 };
	//opengltexture地址
	GLuint texs[2] = { 0 };

	unsigned char* bgdata = NULL;
};
