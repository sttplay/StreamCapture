#include "OpenGLYUYV422.h"
#include <QThread>
#include <QDebug>
#include <QCoreApplication>


OpenGLYUYV422::OpenGLYUYV422(QWidget *parent)
	: OpenGLCanvas(parent)
{
	qDebug() << "========== Canvase Type:YUYV422 ==========";

#pragma region 着色器
	//顶点shader
	vSCode = GET_STR(
		attribute vec4 vertexIn;
	attribute vec2 textureIn;
	varying vec2 textureOut;

	uniform mat4 modelMat;
	uniform mat4 viewMat;
	uniform mat4 projMat;
	void main(void)
	{
		textureOut = textureIn;
		gl_Position = projMat * viewMat * modelMat * vertexIn;
	}
	);


	//片元shader 方式一
	fSCode = GET_STR(
		varying vec2 textureOut;
	uniform sampler2D tex_yuyv_y;
	uniform sampler2D tex_yuyv_uv;
	void main(void)
	{
		vec3 yuv;
		vec3 rgb;
		yuv.x = texture2D(tex_yuyv_y, textureOut).r - 0.0625;
		yuv.y = texture2D(tex_yuyv_uv, textureOut).g - 0.5;
		yuv.z = texture2D(tex_yuyv_uv, textureOut).a - 0.5;

		//BT601
		rgb.x = 1.164 *  yuv.x + 1.596 * yuv.z;
		rgb.y = 1.164 *  yuv.x - 0.392 * yuv.y - 0.812 * yuv.z;
		rgb.z = 1.164 *  yuv.x + 2.016 * yuv.y;

		gl_FragColor = vec4(rgb, 1.0);
	}
	);

	//片元shader 方式二
	//fSCode = GET_STR(
	//	varying vec2 textureOut;
	//uniform sampler2D tex_yuyv_y;
	//uniform sampler2D tex_yuyv_uv;
	//void main(void)
	//{
	//	vec3 yuv;
	//	vec3 rgb;

	//	ivec2 texSize = textureSize(tex_yuyv_uv, 0);
	//	int texCoordX = (int)(textureOut.x * texSize.x * 2);
	//	if (mod(texCoordX, 2.0) == 0)
	//		yuv.x = texture2D(tex_yuyv_uv, textureOut).r - 0.0625;
	//	else
	//		yuv.x = texture2D(tex_yuyv_uv, textureOut).b - 0.0625;
	//	yuv.y = texture2D(tex_yuyv_uv, textureOut).g - 0.5;
	//	yuv.z = texture2D(tex_yuyv_uv, textureOut).a - 0.5;

	//	//BT601
	//	rgb.x = 1.164 *  yuv.x + 1.596 * yuv.z;
	//	rgb.y = 1.164 *  yuv.x - 0.392 * yuv.y - 0.812 * yuv.z;
	//	rgb.z = 1.164 *  yuv.x + 2.016 * yuv.y;

	//	gl_FragColor = vec4(rgb, 1.0);
	//}
	//);

#pragma endregion 着色器

}

OpenGLYUYV422::~OpenGLYUYV422()
{
	if (texs[0])
		glDeleteTextures(1, texs);
}


/*
 * 通过OpenGL绘制窗口句柄hwnd不需要使用
 */
bool OpenGLYUYV422::InitCanvas(int width, int height)
{
	OpenGLCanvas::InitCanvas(width, height);
	//创建材质
	glGenTextures(2, texs);

	glBindTexture(GL_TEXTURE_2D, texs[0]);
	//放大缩小过滤器，线性插值  GL_NEAREST(效率高，但马赛克严重)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//创建材质空间(显卡空间）
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE_ALPHA, width, height, 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, NULL);


	glBindTexture(GL_TEXTURE_2D, texs[1]);
	//放大缩小过滤器，线性插值  GL_NEAREST(效率高，但马赛克严重)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//创建材质空间(显卡空间）
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width / 2, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	return true;
}


void OpenGLYUYV422::initializeGL()
{
	OpenGLCanvas::initializeGL();
	//从shader获取材质索引
	unis[0] = program.uniformLocation("tex_yuyv_y");
	unis[1] = program.uniformLocation("tex_yuyv_uv");
}


void OpenGLYUYV422::paintGL()
{
	if (!crtFrame) return;
	mutex.lock();
	glActiveTexture(GL_TEXTURE0);
	//绑定到材质
	glBindTexture(GL_TEXTURE_2D, texs[0]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, crtFrame->linesize[0] / 2);
	//修改材质内容
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, crtFrame->width, crtFrame->height, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, crtFrame->data[0]);
	//与shader变量关联
	glUniform1i(unis[0], 0);

	glActiveTexture(GL_TEXTURE1);
	//绑定到材质
	glBindTexture(GL_TEXTURE_2D, texs[1]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, crtFrame->linesize[0] / 4);
	//修改材质内容
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, crtFrame->width / 2, crtFrame->height, GL_RGBA, GL_UNSIGNED_BYTE, crtFrame->data[0]);
	//与shader变量关联
	glUniform1i(unis[1], 1);
	mutex.unlock();

	OpenGLCanvas::paintGL();
}