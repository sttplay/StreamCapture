#include "OpenGLABGR.h"
#include <QCoreApplication>


OpenGLABGR::OpenGLABGR(QWidget *parent)
	: OpenGLCanvas(parent)
{
	qDebug() << "========== Canvase Type:ABGR ==========";

#pragma region 着色器
	//顶点shader
	vSCode = GET_STR(
		attribute vec4 vertexIn;
	attribute vec2 textureIn;
	varying vec2 textureOut;

	uniform mat4 modelMat;
	uniform mat4 viewMat;
	uniform mat4 projMat;
	void main(void)
	{
		textureOut = textureIn;
		gl_Position = projMat * viewMat * modelMat * vertexIn;
	}
	);



	//片元shader
	fSCode = GET_STR(
		varying vec2 textureOut;
	uniform sampler2D tex_rgba;
	uniform sampler2D tex_bg;
	void main(void)
	{
		vec4 rgba = texture2D(tex_rgba, textureOut).abgr;
		vec4 bgcolor = texture2D(tex_bg, textureOut);

		//gl_FragColor = rgba;

		//使用混合模式
		vec4 fcolor;
		fcolor.x = rgba.x * rgba.w + bgcolor.x * (1.0 - rgba.w);
		fcolor.y = rgba.y * rgba.w + bgcolor.y * (1.0 - rgba.w);
		fcolor.z = rgba.z * rgba.w + bgcolor.z * (1.0 - rgba.w);
		fcolor.w = rgba.w * rgba.w + bgcolor.w * (1.0 - rgba.w);
		gl_FragColor = fcolor;
	}
	);

#pragma endregion 着色器

}

OpenGLABGR::~OpenGLABGR()
{
	if (texs[0])
		glDeleteTextures(2, texs);

	if (bgdata)
		delete bgdata;
}


/*
 * 通过OpenGL绘制窗口句柄hwnd不需要使用
 */
bool OpenGLABGR::InitCanvas(int width, int height)
{
	OpenGLCanvas::InitCanvas(width, height);
	//创建材质
	glGenTextures(2, texs);

	//RGBA
	glBindTexture(GL_TEXTURE_2D, texs[0]);
	//放大缩小过滤器，线性插值  GL_NEAREST(效率高，但马赛克严重)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//创建材质空间(显卡空间）
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	//背景
	glBindTexture(GL_TEXTURE_2D, texs[1]);
	//放大缩小过滤器，线性插值  GL_NEAREST(效率高，但马赛克严重)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//创建材质空间(显卡空间）
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL);

	//glEnable(GL_BLEND);
	//这里把将要画上去的颜色称为“源颜色”，把原来的颜色称为“目标颜色”
	//glBlendFunc("源颜色/画布色", "目标颜色/底色")
	//glBlendFunc(GL_ONE, GL_ZERO); //默认模式，只使用源颜色，不使用目标颜色
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //常用
	//glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ONE_MINUS_DST_COLOR);

	return true;
}


void OpenGLABGR::initializeGL()
{
	OpenGLCanvas::initializeGL();
	//从shader获取材质索引
	unis[0] = program.uniformLocation("tex_rgba");
	unis[1] = program.uniformLocation("tex_bg");
}

void OpenGLABGR::paintGL()
{
	if (!crtFrame) return;

	if (!bgdata)
	{
		bgdata = new unsigned char[crtFrame->linesize[0] * height];
		OpenGLCanvas::ResetGridData(bgdata, crtFrame->linesize[0] / 4, height, 255, 204);
	}
	mutex.lock();
	glActiveTexture(GL_TEXTURE0);
	//绑定到材质
	glBindTexture(GL_TEXTURE_2D, texs[0]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, crtFrame->linesize[0] / 4);
	//修改材质内容
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, crtFrame->width, crtFrame->height, GL_RGBA, GL_UNSIGNED_BYTE, crtFrame->data[0]);
	//与shader变量关联
	glUniform1i(unis[0], 0);


	glActiveTexture(GL_TEXTURE1);
	//绑定到材质
	glBindTexture(GL_TEXTURE_2D, texs[1]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, crtFrame->linesize[0] / 4);
	//修改材质内容
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, crtFrame->width, crtFrame->height, GL_LUMINANCE, GL_UNSIGNED_BYTE, bgdata);
	//与shader变量关联
	glUniform1i(unis[1], 1);
	mutex.unlock();

	OpenGLCanvas::paintGL();
}
