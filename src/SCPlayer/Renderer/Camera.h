#pragma once
#include "QMatrix4x4"
class Camera
{

public:

	Camera();

	void SetPosition(float x, float y, float z);
	void SetRotation(float pitch, float yaw, float raw = 0);

	void Translate(float deltaX, float deltaY, float deltaZ);
	void Rotate(float deltaPitch, float deltaYaw, float deltaRaw = 0);

	QMatrix4x4 GetViewMat();
private:

	QVector3D up;
	QVector3D position;
	QVector3D rotation;
	QVector3D forward;
};

