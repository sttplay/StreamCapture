#include "OpenGLGray.h"
#include <QCoreApplication>


OpenGLGray::OpenGLGray(QWidget *parent)
	: OpenGLCanvas(parent)
{
	qDebug() << "========== Canvase Type:Gray ==========";

#pragma region 着色器
	//顶点shader
	vSCode = GET_STR(
		attribute vec4 vertexIn;
	attribute vec2 textureIn;
	varying vec2 textureOut;

	uniform mat4 modelMat;
	uniform mat4 viewMat;
	uniform mat4 projMat;
	void main(void)
	{
		textureOut = textureIn;
		gl_Position = projMat * viewMat * modelMat * vertexIn;
	}
	);


	//片元shader
	fSCode = GET_STR(
		varying vec2 textureOut;
	uniform sampler2D tex_gray;
	void main(void)
	{
		vec4 rgb;
		rgb = texture2D(tex_gray, textureOut);

		gl_FragColor = vec4(rgb.xyz, 1.0);
	}
	);

#pragma endregion 着色器

}

OpenGLGray::~OpenGLGray()
{
	if (texs[0])
		glDeleteTextures(1, texs);
}


/*
 * 通过OpenGL绘制窗口句柄hwnd不需要使用
 */
bool OpenGLGray::InitCanvas(int width, int height)
{
	OpenGLCanvas::InitCanvas(width, height);
	//创建材质
	glGenTextures(1, texs);

	//gray
	glBindTexture(GL_TEXTURE_2D, texs[0]);
	//放大缩小过滤器，线性插值  GL_NEAREST(效率高，但马赛克严重)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//创建材质空间(显卡空间）
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL);

	return true;
}


void OpenGLGray::initializeGL()
{
	OpenGLCanvas::initializeGL();
	//从shader获取材质索引
	unis[0] = program.uniformLocation("tex_gray");
}

void OpenGLGray::paintGL()
{
	if (!crtFrame) return;
	mutex.lock();
	glActiveTexture(GL_TEXTURE0);
	//绑定到材质
	glBindTexture(GL_TEXTURE_2D, texs[0]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, crtFrame->linesize[0]);
	//修改材质内容
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, crtFrame->width, crtFrame->height, GL_LUMINANCE, GL_UNSIGNED_BYTE, crtFrame->data[0]);
	//与shader变量关联
	glUniform1i(unis[0], 0);
	mutex.unlock();

	OpenGLCanvas::paintGL();
}