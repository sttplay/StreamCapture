#include "SCPlayer.h"
#include "SCPlayerManager.h"
#include <QDropEvent>
#include <QMimeData>
#include <QFileDialog>
#include <QTimer>
#include <QThread>
#include "Clock.h"
#include <QScreen>
#include <QMessageBox>
#ifdef _WIN32
#include <QCameraInfo>
#include <QAudioDeviceInfo>
#endif // _WIN32
#include <qglobal.h>
#include "StreamInfoWidget.h"
#include "Renderer/OpenGLCanvas.h"
#include "Renderer/CanvasFactory.h"

#include <opencv2/core.hpp>			//Mat
#include <opencv2/imgcodecs.hpp>	//imread
#include <opencv2/highgui.hpp>		//namedWindow imshow waitKey

#ifdef _DEBUG
#pragma comment(lib, "opencv_world430d.lib")
#else
#pragma comment(lib, "opencv_world430.lib")
#endif // _DEBUG

#define DISABLE_VIDEO_
#define DISABLE_AUDIO_

#define ASYNC_OPEN_AUDIO_DEVICE

//最小阈值
#define AV_SYNC_THRESHOLD_MIN 40
//最大阈值
#define AV_SYNC_THRESHOLD_MAX 100

#define AV_SYNC_FRAMEDUP_THRESHOLD 100

const int fmtCount = 17;
struct fmtinfo
{
	int fmt;
	const char* desc;
};
const fmtinfo pixfmts[fmtCount]
{
	{PIX_FMT_AUTO,		"AUTO"},
	{PIX_FMT_YUV420P,	"YUV420P"},
	{PIX_FMT_YUYV422,	"YUYV422"},
	{PIX_FMT_YUV422P,	"YUV422P"},
	{PIX_FMT_YUV444P,	"YUV444P"},
	{PIX_FMT_GRAY8,		"GRAY8"},
	{PIX_FMT_YUVJ420P,	"YUVJ420P"},
	{PIX_FMT_YUVJ422P,	"YUVJ422P"},
	{PIX_FMT_YUVJ444P,	"YUVJ444P"},
	{PIX_FMT_NV12,		"NV12"},
	{PIX_FMT_NV21,		"NV21"},
	{PIX_FMT_RGB24,		"RGB24"},
	{PIX_FMT_BGR24,		"BGR24"},
	{PIX_FMT_ARGB,		"ARGB"},
	{PIX_FMT_RGBA,		"RGBA"},
	{PIX_FMT_ABGR,		"ABGR"},
	{PIX_FMT_BGRA,		"BGRA"},
};
int index2fffmt(int index)
{
	if (index > fmtCount - 1) return pixfmts[0].fmt;
	else return pixfmts[index].fmt;
}
int fffmt2index(int fmt)
{
	for (int i = 0; i < fmtCount; i++)
	{
		if (fmt == pixfmts[i].fmt)
			return i;
	}
	return -1;
}

SCPlayer::SCPlayer(BaseInfo info, QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
#ifdef _WIN32
	defaultSetting.tryHardwareAccel = HW_ACCEL_AUTO;
#else
	ui.TryHWAccelTypeComboBox->hide();
	ui.TryHWAccelTypeLabel->hide();
#endif
	setWindowIcon(QIcon(":/Resources/img/logo.png"));
	//Test();
	this->baseInfo = info;
	ui.UrlLineEdit->setText(GetLastOpenFile());

	QTimer* timer = new QTimer(this);
	connect(this, &SCPlayer::CanvasQuit_Signal, this, &SCPlayer::CanvasQuit);
	connect(this, &SCPlayer::OpenCallback_Signal, this, &SCPlayer::OpenCallback, Qt::BlockingQueuedConnection);
	connect(this, &SCPlayer::InterruptCallback_Signal, this, &SCPlayer::InterruptCallback);
	connect(this, &SCPlayer::OpenAudioPlayCallback_Signal, this, &SCPlayer::OpenAudioPlayCallback);
	connect(ui.SeekSlider, &QSliderPro::mouseRelease, this, &SCPlayer::OnSeekSliderRelease);
	connect(timer, &QTimer::timeout, this, &SCPlayer::timeout);
	connect(this, &SCPlayer::RequestCreateCanvas_Signal, this, &SCPlayer::RequestCreateCanvas, Qt::BlockingQueuedConnection);

	timer->start(40);

	this->setAcceptDrops(true);

	capture = ::CreateStreamCapture(this);

	screenRect = QGuiApplication::primaryScreen()->availableGeometry();

#pragma region 设置默认值
	//defaultSetting.outputPixelformat = PIX_FMT_YUV422P;
	/*defaultSetting.cameraWidth = 1920;
	defaultSetting.cameraHeight = 1080;
	defaultSetting.cameraInputFormatPriority = CIFP_MJPEG;
	defaultSetting.openModel = OPEN_MODEL_CAMERA;*/

	//defaultSetting.openTimeout = 20000;
	//defaultSetting.readTimeout = 500;
	//defaultSetting.openModel = OPEN_MODEL_CAMERA;
#pragma endregion 设置默认值
	ui.FormatPriorityComboBox->addItem("AUTO");

	ui.OpenModelBtn->setText(defaultSetting.openModel == OPEN_MODEL_URL ? "U" : "C");
	ui.OpenTimeoutLineEdit->setText(QString::number(defaultSetting.openTimeout));
	ui.ReadTimeoutLineEdit->setText(QString::number(defaultSetting.readTimeout));
	ui.EnableVideoCB->setChecked(defaultSetting.enableVideo);
	ui.EnableAudioCB->setChecked(defaultSetting.enableAudio);
	ui.CamWidthLineEdit->setText(QString::number(defaultSetting.cameraWidth));
	ui.CamHeightLineEdit->setText(QString::number(defaultSetting.cameraHeight));
	ui.FrameRateLineEdit->setText(QString::number(defaultSetting.cameraFrameRate));
	ui.LowDelayCB->setChecked(defaultSetting.codecforceLowDelay);
	ui.NoBufferCB->setChecked(defaultSetting.nobuffer);
	ui.RTSPTransportCB->setChecked(defaultSetting.rtspTransport);
	ui.OutputPixfmtComboBox->setCurrentIndex(fffmt2index(defaultSetting.outputPixelformat));
	ui.FormatPriorityComboBox->setCurrentIndex(0);
	ui.TryHWAccelTypeComboBox->setCurrentIndex(defaultSetting.tryHardwareAccel);
	outputpixfmt = defaultSetting.outputPixelformat;

	ui.AudioTrackComboBox->setEnabled(false);
	ui.VolumeSlider->setValue(ui.VolumeSlider->maximum() * volume);

}

SCPlayer::~SCPlayer()
{
	disconnect(this, &SCPlayer::CanvasQuit_Signal, this, &SCPlayer::CanvasQuit);
	disconnect(this, &SCPlayer::OpenCallback_Signal, this, &SCPlayer::OpenCallback);
	disconnect(this, &SCPlayer::InterruptCallback_Signal, this, &SCPlayer::InterruptCallback);
	disconnect(this, &SCPlayer::OpenAudioPlayCallback_Signal, this, &SCPlayer::OpenAudioPlayCallback);
	disconnect(ui.SeekSlider, &QSliderPro::mouseRelease, this, &SCPlayer::OnSeekSliderRelease);
	disconnect(this, &SCPlayer::RequestCreateCanvas_Signal, this, &SCPlayer::RequestCreateCanvas);
}


void SCPlayer::dragEnterEvent(QDragEnterEvent *event)
{
	event->acceptProposedAction();
}

void SCPlayer::dropEvent(QDropEvent *event)
{
	//获取文件路径 (QString)
	QList<QUrl> urls = event->mimeData()->urls();
	if (urls.isEmpty()) return;
	//url为unicode
	QString url = urls.first().toString();
	if (url.startsWith("file:///"))
		url.remove(0, 8);
	WriteToLocal(url);
}

/*
 * 通知创建窗体删除自己
 */
void SCPlayer::closeEvent(QCloseEvent *event)
{

	Close();
	if (baseInfo.controller)
		emit baseInfo.controller->PlayerQuit_Signal(baseInfo.btnIndex, baseInfo.btn);
}
/*
 * 创建窗体调用删除
 */
void SCPlayer::Close()
{
	if (infoWidget)
		infoWidget->close();
	infoWidget = NULL;

	if (cameraInfoWidget)
		cameraInfoWidget->close();
	cameraInfoWidget = NULL;

	CanvasQuit();

	if (capture) ::DeleteStreamCapture(capture);
	capture = NULL;
}

QString SCPlayer::GetLastOpenDir()
{
	QString lastOpenFile = GetLastOpenFile();
	int index = lastOpenFile.lastIndexOf("/");
	lastOpenFile.remove(index, lastOpenFile.size() - index);
	return lastOpenFile;
}

QString SCPlayer::GetLastOpenFile()
{
	//读取上次打开路径
	QString lastDir = NULL;
	QFile file("./url" + QString::number(baseInfo.btnIndex) + ".rcd");
	bool b = file.exists();
	if (file.open(QIODevice::ReadOnly))
	{
		lastDir = QString::fromUtf8(file.readAll());
		file.close();
	}
	return lastDir;
}
void SCPlayer::WriteToLocal(QString str)
{
	if (!str.isEmpty())
	{
		//写入路径
		QFile file("./url" + QString::number(baseInfo.btnIndex) + ".rcd");
		if (file.open(QIODevice::Truncate | QIODevice::WriteOnly))
		{
			int a = file.write(str.trimmed().toUtf8());
			file.close();
		}

		ui.UrlLineEdit->setText(str);
	}
}


void SCPlayer::CanvasQuit()
{
	on_CloseBtn_clicked();
}

void SCPlayer::timeout()
{
	if (!capture) return;
	double d = ::GetCurrentProgress(capture);
	ui.SeekSlider->setValue(d * ui.SeekSlider->maximum());
	ui.RuntimeLabel->setText(Utility::ConvertToTime(d * totalMs, false));

	PFCounts* pf = ::GetPFCounts(capture);
	if (!pf) return;
	ui.VPCLabel->setText(QString::number(pf->videoPktCount));
	ui.VFCLabel->setText(QString::number(pf->videoFrameCount));
	ui.APCLabel->setText(QString::number(pf->audioPktCount));
	ui.AFCLable->setText(QString::number(pf->audioFrameCount));
}


void SCPlayer::on_SelectFileBtn_clicked()
{
	QString url = QFileDialog::getOpenFileName(this, QString::fromUtf8("Select file"), GetLastOpenDir());
	WriteToLocal(url);
}

void SCPlayer::on_OpenBtn_clicked()
{
	//防止过度快速打开， 1s最多两次
	if (GetTimestamp() < nextts) return;
	nextts = GetTimestamp() + 500 * 1000;
	if (!capture) return;
	on_CloseBtn_clicked();
	ui.SettingWidget->setEnabled(false);
	isPause = false;
	firstFramePause = false;
	if (!ui.AutoPlayCB->isChecked())
	{
		firstFramePause = true;
		ui.PauseBtn->setText("Play");
	}
	::SetOption(capture, OT_OpenModel, ui.OpenModelBtn->text() == "U" ? OPEN_MODEL_URL : OPEN_MODEL_CAMERA);
	::SetOption(capture, OT_OpenTimeout, ui.OpenTimeoutLineEdit->text().toInt());
	::SetOption(capture, OT_ReadTimeout, ui.ReadTimeoutLineEdit->text().toInt());
	::SetOption(capture, OT_EnableVideo, ui.EnableVideoCB->isChecked());
	::SetOption(capture, OT_EnableAudio, ui.EnableAudioCB->isChecked());
	::SetOption(capture, OT_CameraWidth, ui.CamWidthLineEdit->text().toInt());
	::SetOption(capture, OT_CameraHeight, ui.CamHeightLineEdit->text().toInt());
	::SetOption(capture, OT_CameraFrameRate, ui.FrameRateLineEdit->text().toInt());
	::SetOption(capture, OT_CodecForceLowDelay, ui.LowDelayCB->isChecked());
	::SetOption(capture, OT_NoBuffer, ui.NoBufferCB->isChecked());
	::SetOption(capture, OT_RTSPTransport, ui.RTSPTransportCB->isChecked());
	::SetOption(capture, OT_OutputPixelformat, index2fffmt(ui.OutputPixfmtComboBox->currentIndex()));
	::SetOption(capture, OT_TryHardwareAccel, ui.TryHWAccelTypeComboBox->currentIndex());
	QString crtText = ui.FormatPriorityComboBox->currentText();
	int CIFP = CIFP_AUTO;
	if (crtText == "AUTO") CIFP = CIFP_AUTO;
	else if (crtText == "YUYV422") CIFP = CIFP_YUYV422;
	else if (crtText == "MJPEG") CIFP = CIFP_MJPEG;
	else if (crtText == "NV12") CIFP = CIFP_NV12;
	else if (crtText == "BGR0") CIFP = CIFP_BGR0;
	::SetOption(capture, OT_CameraInputFormatPriority, CIFP);

	::RegisterInterruptCallback(capture, interruptcb);
	::OpenAsync(capture, ++captureOindex, ui.UrlLineEdit->text().toUtf8().data(), opencb);
	WriteToLocal(ui.UrlLineEdit->text());
}

void SCPlayer::on_CloseBtn_clicked()
{
	Q_ASSERT(QCoreApplication::instance()->thread() == QThread::currentThread());
	isExit = true;
	fileInfo = "";
	ui.SettingWidget->setEnabled(true);
	ui.AudioTrackComboBox->clear();
	ui.AudioTrackComboBox->setEnabled(false);
	ui.VPCLabel->setText(QString::number(0));
	ui.VFCLabel->setText(QString::number(0));
	ui.APCLabel->setText(QString::number(0));
	ui.AFCLable->setText(QString::number(0));
	ui.RuntimeLabel->setText(Utility::ConvertToTime(0, false));
	ui.TotalTimeLabel->setText(Utility::ConvertToTime(0, false));
	ui.PauseBtn->setText("Pause");
	if (videoLoop_t.joinable())
		videoLoop_t.join();
	if (audioPlay)
	{
		DeleteAudioPlay(audioPlay);
		audioPlay = NULL;
	}
	if (resampler)
	{
		DeleteResampler(resampler);
		resampler = NULL;
	}
	if (capture)
	{
		::Close(capture);
	}
	if (canvas)
	{
		delete canvas;
		canvas = NULL;
	}

	if (audioClock)
		delete audioClock;
	audioClock = NULL;

	if (videoClock)
		delete videoClock;
	videoClock = NULL;

	pcm.clear();
}

void SCPlayer::on_PauseBtn_clicked()
{
	if (ui.PauseBtn->text() == QString("Pause"))
	{
		ui.PauseBtn->setText("Play");
		isPause = true;
	}
	else
	{
		ui.PauseBtn->setText("Pause");
		isPause = false;
	}
}

void SCPlayer::on_ClearCacheBtn_clicked()
{
	if (!capture) return;
	ClearCache(capture, CACHE_TYPE_VIDEO);
	ClearCache(capture, CACHE_TYPE_AUDIO);
}

void SCPlayer::on_OpenModelBtn_clicked()
{
	if (ui.OpenModelBtn->text() == QString("U"))
		ui.OpenModelBtn->setText("C");
	else
		ui.OpenModelBtn->setText("U");
}

void SCPlayer::on_ShowInfoBtn_clicked()
{
	if (infoWidget) infoWidget->close();
	infoWidget = new StreamInfoWidget();
	infoWidget->selfOnBase = &infoWidget;
	QStringList msgList;
	msgList << "========== Software ==========\n";
	msgList << "WXY Collector's Edition ";
	msgList << "version:" << QString::number(::GetStreamCaptureVersion()) << "\n";
	infoWidget->ShowInfo(msgList.join("") + fileInfo);
	infoWidget->show();
}

void SCPlayer::OpenCallback(int openIndex, StreamsParameters* info)
{
	//Q_ASSERT(QCoreApplication::instance()->thread() == QThread::currentThread());
	if (openIndex != captureOindex) return;
	if (info->retcode != RETCODE_SUCCESS)
	{
		QString msg;
		if (info->retcode == RETCODE_ALLOC_CONTEXT_FAILED)
			msg = "ALLOC_CONTEXT_FAILED";
		else if (info->retcode == RETCODE_FFMPEG_LOG)
			msg = QString(info->errLog);
		else if (info->retcode == RETCODE_OPEN_TIMEOUT)
			msg = "OPEN_TIMEOUT";
		else if (info->retcode == RETCODE_NOT_SUPPORT_SWF)
			msg = "NOT_SUPPORT_SWF";
		QMessageBox::warning(this, "Open failed!", msg, QMessageBox::Close);
		on_CloseBtn_clicked();
		return;
	}
	if (!info->vs && !info->as)
	{
		QMessageBox::warning(this, "Tip!", "Does not contain any audio or video streams", QMessageBox::Close);
		on_CloseBtn_clicked();
		return;
	}

	isExit = false;
	videoClock = new Clock();
	audioClock = new Clock();
	prepareVideoOk = false;
	prepareAudioOk = false;
	if (info->vs)
	{
#ifndef DISABLE_VIDEO
		//CreateCanvas(info->vs);
#endif // DISABLE_VIDEO

		fileInfo += QString(info->describe);
		prepareVideoOk = true;
		masterClock = videoClock;
	}

	if (info->as)
	{
		srcas = info->as;
		audioPlay = CreateAudioPlay(this);

#ifdef ASYNC_OPEN_AUDIO_DEVICE
		OpenAudioPlayAsync(audioPlay, ++apOindex, info->as, openaudiocb, audiocb);
#else
		AudioPlayParams* app = OpenAudioPlay(audioPlay, info->as, audiocb);
		OpenAudioPlayCallback(-1, app);
		prepareAudioOk = true;
#endif // ASYNC_OPEN_AUDIO_DEVICE
		masterClock = audioClock;
	}

	if (!info->as)
		PrepareFinish();


	totalMs = ::GetDuration(capture);
	ui.TotalTimeLabel->setText(Utility::ConvertToTime(totalMs, false));

	nosync = ui.NoSyncCB->isChecked();
	isLoop = ui.IsLoopCB->isChecked();
	//ui.EnableVideoCB->setChecked(info->vs);
	//ui.EnableAudioCB->setChecked(info->as);
}

void SCPlayer::InterruptCallback(int interruptCode)
{
	if (canvas == NULL) return;
	QString msg = "";
	if (interruptCode == INTERRUPT_CODE_UKNOW)
		msg = "INTERRUPT_CODE_UKNOW";
	else if (interruptCode == INTERRUPT_CODE_READ_TIMEOUT)
		msg = "INTERRUPT_CODE_READ_TIMEOUT";
	else if (interruptCode == INTERRUPT_CODE_FORCE_QUIT)
	{
		//msg = "INTERRUPT_CODE_FORCE_QUIT";
		return;
	}
	else if (interruptCode == INTERRUPT_CODE_ENQUEUE_FAILED)
		msg = "INTERRUPT_CODE_ENQUEUE_FAILED";
	else if (interruptCode == INTERRUPT_CODE_EOF)
		msg = "INTERRUPT_CODE_EOF";

	QMessageBox::warning(this, "Interrupt!", msg, QMessageBox::Close);
	on_CloseBtn_clicked();
}

void SCPlayer::OpenAudioPlayCallback(int openIndex, AudioPlayParams* param)
{
	if (openIndex >= 0 && openIndex != apOindex)
		return;

	if (isExit || !audioPlay) return;
	if (!param)
	{
		isPause = true;
		QMessageBox::warning(this, "Tip!", "Open AudioPlay failed!", QMessageBox::Close);
		on_CloseBtn_clicked();
		return;
	}
	bytes_per_sec = param->desas->samplerate * (16 / 8) * param->desas->channels;
	audio_hw_buf_size = param->hwSize;

	if (!resampler)
		resampler = CreateResampler();
	if (OpenResampler(resampler, srcas, param->desas) != 0)
	{
		QMessageBox::warning(this, "Tip!", "Open Resampler failed!", QMessageBox::Close);
		on_CloseBtn_clicked();
		return;
	}


	for (int i = 0; i < srcas->streamCount; i++)
	{
		ui.AudioTrackComboBox->addItem("Track" + QString::number(i + 1));
	}
	if (srcas->streamCount > 1)
		ui.AudioTrackComboBox->setEnabled(true);

	prepareAudioOk = true;
	PrepareFinish();
}

void SCPlayer::PrepareFinish()
{
	if (prepareVideoOk) videoLoop_t = std::thread(&SCPlayer::VideoLoop, this);
	if (prepareAudioOk) StartAudioPlay(audioPlay);
}

void SCPlayer::OnSeekSliderRelease(double per)
{
	Seek(per);
}

void SCPlayer::on_AudioTrackComboBox_currentIndexChanged(int index)
{
	if (index < 0 || !capture) return;
	::SetAudioTrack(capture, index);
}

void SCPlayer::on_OutputPixfmtComboBox_currentIndexChanged(int index)
{
	outputpixfmt = pixfmts[index].fmt;
	::SetOption(capture, OT_OutputPixelformat, outputpixfmt);
}

//void Player::on_FormatPriorityComboBox_currentIndexChanged(int index)
//{
//	//qDebug() << index;
//	::SetOption(capture, OT_CameraInputFormatPriority, index);
//}

void SCPlayer::on_VolumeSlider_valueChanged(int value)
{
	volume = (float)value / ui.VolumeSlider->maximum();
}

void SCPlayer::on_PanoramicCB_toggled(bool state)
{
	if (canvas)
		canvas->Set360VR(state);
}

void SCPlayer::on_IsLoopCB_toggled(bool state)
{
	isLoop = state;
}

void SCPlayer::on_CameraInfoBtn_clicked()
{
	CameraDescription* desc = ::GetCameraInfo(ui.UrlLineEdit->text().toUtf8().data());
	QString info = QString("========== %0 ==========\n").arg(ui.UrlLineEdit->text());
	info += desc->desc;
	info += "=====================================\n";
#ifdef _WIN32
	//相机
	QStringList videoDevicesName;
	videoDevicesName << "========== All Camera List ==========\n";
	foreach(const QCameraInfo &cameraInfo, QCameraInfo::availableCameras())
		videoDevicesName << cameraInfo.description() << "\n";
	videoDevicesName << "=====================================\n\n";
	info = videoDevicesName.join("") + info;

	//麦克风
	//QStringList audioDevicesName;
	/*audioDevicesName << "========== All Audio List ==========\n";
	QList<QAudioDeviceInfo> listAudio = QAudioDeviceInfo::availableDevices(QAudio::AudioInput);
	foreach(QAudioDeviceInfo audio, listAudio)
		audioDevicesName << audio.deviceName() << "\n";
	audioDevicesName << "=====================================\n\n";*/
	//info = audioDevicesName.join(" ") + info;

#endif //_WIN32

	ui.FormatPriorityComboBox->clear();
	ui.FormatPriorityComboBox->addItem("AUTO");
	if (desc->yuyv422 > 0) ui.FormatPriorityComboBox->addItem("YUYV422");
	if (desc->mjpeg > 0) ui.FormatPriorityComboBox->addItem("MJPEG");
	if (desc->nv12 > 0) ui.FormatPriorityComboBox->addItem("NV12");
	if (desc->bgr0 > 0) ui.FormatPriorityComboBox->addItem("BGR0");

	if (cameraInfoWidget) cameraInfoWidget->close();
	cameraInfoWidget = new StreamInfoWidget();
	cameraInfoWidget->selfOnBase = &cameraInfoWidget;
	cameraInfoWidget->setWindowTitle("Camera info");
	cameraInfoWidget->resize(cameraInfoWidget->size() + QSize(200, 0));
	cameraInfoWidget->ShowInfo(info);
	cameraInfoWidget->show();
	WriteToLocal(ui.UrlLineEdit->text());
}

void SCPlayer::RequestCreateCanvas(int width, int height, int format)
{
	VideoStream vs;
	vs.pixelfmt = format;
	vs.pixelwidth = width;
	vs.pixelheight = height;
	CreateCanvas(&vs);
}

void SCPlayer::CreateCanvas(VideoStream* vs)
{
	int fmt = outputpixfmt == PIX_FMT_AUTO ? vs->pixelfmt : outputpixfmt;
	canvas = CanvasFactory::CreateCanvas(fmt, this);
	if (!canvas)
	{
		QString msg = "The target pixel format is not supported in the current version\n The target format:" + QString::number(fmt);
		msg += "\n Please refer to FFmpeg AVPixelFormat for format type\n";
		msg += "You can also set the output pixel format to play in other Settings\n";
		msg += "Auto convert to BGRA";
		QMessageBox::warning(this, "Tip!", msg, QMessageBox::Close);
		//当格式不支持的时候自动转为BGRA
		fmt = PIX_FMT_BGRA;
		SetOption(capture, OT_OutputPixelformat, fmt);
		canvas = CanvasFactory::CreateCanvas(fmt, this);
		canvas->setWindowTitle(QString("%1 (%2 %3)").arg(ui.UrlLineEdit->text()).arg(pixfmts[fffmt2index(fmt)].desc).arg("Auto convert pixel format"));
	}
	else
	{
		canvas->setWindowTitle(QString("%1 (%2)").arg(ui.UrlLineEdit->text()).arg(pixfmts[fffmt2index(fmt)].desc));
	}

	canvas->show();
	canvas->Set360VR(ui.PanoramicCB->isChecked());
	canvas->InitCanvas(vs->pixelwidth, vs->pixelheight);
	int w, h;
	ClampWidgetSize(vs->pixelwidth, vs->pixelheight, &w, &h);
	canvas->resize(w, h);
	canvas->move((screenRect.width() - w) / 2, (screenRect.height() - h) / 2);
	return;
}


void SCPlayer::AudioPlayCallback(uint8_t *stream, int len)
{
	int64_t audio_callback_time = GetTimestampUTC();
	int audio_clock = 0;
	while (pcm.size() < len && !isExit && !isPause)
	{
		Frame* frame = ::TryGrabFrame(capture, FRAME_TYPE_AUDIO);
		if (!frame)
		{
			QThread::msleep(5);
			continue;
		}
		if (frame->flags == FRAME_FLAG_EOF)
		{
			if (isLoop && masterClock == audioClock)
				Seek(0);
			break;
		}
		ResampleData* rd = (ResampleData*)Resample(resampler, frame);
		pcm.append((char*)rd->data, rd->len);
		audio_clock = frame->pts + (double)frame->nb_samples / frame->sample_rate * 1000;
		::RemoveFrame(capture, FRAME_TYPE_AUDIO);
	}

	if (isPause)
	{
		memset(stream, 0, len);
		return;
	}
	int minLen = SCMIN(len, pcm.size());

	MixAudioFormat(stream, (unsigned char*)pcm.data(), minLen, volume);
	pcm.remove(0, minLen);

	double noplayBuffSize = audio_hw_buf_size + pcm.size();
	int noplayms = noplayBuffSize / bytes_per_sec * 1000;
	int crtpts = audio_clock - noplayms;
	if (audioClock) audioClock->set_clock_at(crtpts, audio_callback_time / 1000);
}


void SCPlayer::VideoLoop()
{
	int remaining_time = 0;
	frame_timer = 0;
	while (!isExit)
	{
		if (remaining_time > 0)
			QThread::msleep(remaining_time);

		remaining_time = 10;
		if (!isPause)
			RefreshVideo(&remaining_time);
	}

	qDebug() << "VideoLoop quit!";
}


void SCPlayer::RefreshVideo(int *remaining_time)
{
	Frame* frame, *lastFrame;
	double last_duration;
	double delay;
	int64_t time;
	bool forcedraw = false;

	seekMux.lock();
	frame = TryGrabFrame(capture, FRAME_TYPE_VIDEO);
	seekMux.unlock();
	if (!frame)
	{
		QThread::msleep(10);
	}
	else
	{

		lastFrame = TryGrabLastFrame(capture, FRAME_TYPE_VIDEO);
		if (frame->flags == FRAME_FLAG_EOF)
		{
			if (isLoop && masterClock == videoClock)
				Seek(0);
			return;
		}
		last_duration = LastDuration(lastFrame, frame);
		delay = ComputeTargetDelay(last_duration);

		time = GetTimestampUTC() / 1000;
		if (time < frame_timer + delay)
		{
			*remaining_time = SCMIN(frame_timer + delay - time, *remaining_time);
			//goto display;
			return;
		}

		if (!nosync)
		{
			frame_timer += delay;
			if (delay > 0 && time - frame_timer > AV_SYNC_THRESHOLD_MAX)
				frame_timer = time;
		}

		videoClock->set_clock(frame->pts);
		forcedraw = true;

		if (firstFramePause)
		{
			isPause = true;
			firstFramePause = false;
		}

		if (isStep)
		{
			isStep = false;
			isPause = true;
		}
	}

	if (forcedraw)
	{
		if (!canvas)
		{
			emit RequestCreateCanvas_Signal(frame->width, frame->height, frame->format);
			//刷新视频帧，防止像素格式不同
			frame = TryGrabFrame(capture, FRAME_TYPE_VIDEO);
		}
		if (canvas)
		{
			if (canvas->pixfmt == frame->format)
			{
				canvas->Repaint(frame);
				//CVDisplayAVFrame_Red(frame->avframe);
			}
		}
		::RemoveFrame(capture, FRAME_TYPE_VIDEO);
		//qDebug() << calcTime.restart();
	}
}

double SCPlayer::LastDuration(Frame* lastFrame, Frame* frame)
{
	double duration = frame->pts - lastFrame->pts;
	if (qIsNaN(duration) || duration <= 0 || duration > 10000)
		return lastFrame->duration;
	else
		return duration;
}

double SCPlayer::ComputeTargetDelay(double delay)
{
	//视频时钟和主时钟的时间差
	double diff = videoClock->get_clock() - masterClock->get_clock();
	double sync_threshold = SCMAX(AV_SYNC_THRESHOLD_MIN, SCMIN(AV_SYNC_THRESHOLD_MAX, delay));

	//当视频比音频慢，并且超过阈值
	if (diff <= -sync_threshold)
	{
		delay = SCMAX(0, delay + diff);
		printf("Video is too slow, more than the threshold %lf\n", diff);
	}
	else if (diff >= sync_threshold && delay > AV_SYNC_FRAMEDUP_THRESHOLD)
		delay = delay + diff;
	//当视频比音频快
	else if (diff >= sync_threshold)
		delay = 1.5 * delay;


	return delay;
}

void SCPlayer::ClampWidgetSize(const int in_width, const int in_height, int* out_width, int* out_height)
{

	int w = screenRect.width() * 0.7f;
	int h = screenRect.height() * 0.7f;
	if (in_width > w || in_height > h)
	{
		float rate = (float)in_width / in_height;
		if (in_width > in_height)
		{
			//橫屏
			*out_width = w;
			*out_height = *out_width / rate;
		}
		else
		{
			//竖屏
			*out_height = h;
			*out_width = *out_height * rate;
		}
	}
	else
	{
		*out_width = in_width;
		*out_height = in_height;
	}
}

void SCPlayer::Seek(double per)
{
	seekMux.lock();
	::Seek(capture, per);
	if (isPause)
	{
		isStep = true;
		isPause = false;
	}
	seekMux.unlock();
}

void SCPlayer::Test()
{
	QTimer* testTimer = new QTimer(this);
	defaultSetting.outputPixelformat = PIX_FMT_YUVJ422P;
	connect(testTimer, &QTimer::timeout, [this] {
		/*static bool isopen = false;
		if (isopen)
			on_CloseBtn_clicked();
		else
			on_OpenBtn_clicked();
		isopen = !isopen;*/
		on_OpenBtn_clicked();
	});
	testTimer->start(100);
	//defaultSetting.openModel = OPEN_MODEL_CAMERA;
}
