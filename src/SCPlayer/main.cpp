#include "SCPlayerManager.h"
#include "SCPlayer.h"
#include <QtWidgets/QApplication>
#include <QDebug>

#pragma comment(lib, "../../bin/StreamCapture.lib")


#define MULTIPLE_

void sclog(int level, const char* log)
{
	//printf("[%d] %s\n", level, log);
	qDebug() << "[" << level << "]" << log;
}

//编码说明
//如果传入的是char*字符串，默认这个字符串是以utf-8编码的，就类似于调用了QString::fromUtf8。
//所以如果是将char*字符串给QString，要确保字符串是utf - 8编码的，否则QString在转码到Unicode时会出错。
//QString::fromStdString也是将std::string中的字符串当做utf - 8编码来处理的。

void ApplyQss()
{
	QFile file(":/Resources/qss/psblack.css");
	if (file.open(QFile::ReadOnly))
	{
		QString qss = QLatin1String(file.readAll());
		QString paletterColor = qss.mid(20, 7);
		qApp->setPalette(QPalette(QColor(paletterColor)));
		//应用样式表
		qApp->setStyleSheet(qss);
	}
	file.close();
}

int main(int argc, char *argv[])
{
	//int x = FIELDOFFSET(Frame, format);
	QApplication a(argc, argv);
	ApplyQss();
	qDebug() << GetStreamCaptureVersion(); //196865
	

	InitializeStreamCapture(sclog);
#ifdef MULTIPLE
	SCPlayerManager w;
#else
	SCPlayer w({ NULL, 0, NULL });
#endif // MULTIPLE
	w.show();
	int result = a.exec();
	TerminateStreamCapture();
	UninstallStreamCapture();

	return 0;
}
