#-------------------------------------------------
#
# Project created by QtCreator 2020-08-11T16:42:25
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SCPlayer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
    Clock.cpp \
    Player.cpp \
    QSliderPro.cpp \
    StreamInfoWidget.cpp \
    SCPlayer.cpp \
    Renderer/Camera.cpp \
    Renderer/CanvasFactory.cpp \
    Renderer/OpenGLABGR.cpp \
    Renderer/OpenGLARGB.cpp \
    Renderer/OpenGLBGR24.cpp \
    Renderer/OpenGLBGRA.cpp \
    Renderer/OpenGLCanvas.cpp \
    Renderer/OpenGLGray.cpp \
    Renderer/OpenGLNV12.cpp \
    Renderer/OpenGLNV21.cpp \
    Renderer/OpenGLRGB24.cpp \
    Renderer/OpenGLRGBA.cpp \
    Renderer/OpenGLYUV420P.cpp \
    Renderer/OpenGLYUV422P.cpp \
    Renderer/OpenGLYUV444P.cpp \
    Renderer/OpenGLYUYV422.cpp

HEADERS += \
    Clock.h \
    Player.h \
    QSliderPro.h \
    resource.h \
    StreamInfoWidget.h \
    SCPlayer.h \
    Renderer/Camera.h \
    Renderer/CanvasFactory.h \
    Renderer/OpenGLABGR.h \
    Renderer/OpenGLARGB.h \
    Renderer/OpenGLBGR24.h \
    Renderer/OpenGLBGRA.h \
    Renderer/OpenGLCanvas.h \
    Renderer/OpenGLGray.h \
    Renderer/OpenGLNV12.h \
    Renderer/OpenGLNV21.h \
    Renderer/OpenGLRGB24.h \
    Renderer/OpenGLRGBA.h \
    Renderer/OpenGLYUV420P.h \
    Renderer/OpenGLYUV422P.h \
    Renderer/OpenGLYUV444P.h \
    Renderer/OpenGLYUYV422.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

linux{
LIBS += -L/home/stt/Desktop/ffmpeg-4.2.2/build/lib -lStreamCapture \
    -lavcodec \
    -lavdevice \
    -lavfilter \
    -lavformat \
    -lavutil \
    -lpostproc \
    -lswresample \
    -lswscale \
}


RESOURCES += \
    SCPlayer.qrc

FORMS += \
    Player.ui \
    StreamInfoWidget.ui \
    SCPlayer.ui

DISTFILES += \
    Resource.rc
