#pragma once

#include <QObject>
#include <QMutex>
#include <thread>
#include <QWaitCondition>

struct AudioParameters;
class QAudioOutput;
class QIODevice;

typedef int(*pAudioCallback)(void* owner, char *data, long long maxLen, int remainSize);
#define DECIBEL_ARRAY_SIZE 8
class AudioPlay
{

public:
	AudioPlay();
	~AudioPlay();

	bool Open(int sampleRate, int sampleSize, int channels);

	void Suspend();

	void Resume();

	void ClearCache();

	bool Write(unsigned char* data, int len);
	void Write2(unsigned char* data, int len);

	void Release();

	int GetRemain();

private:
	int periodSize = 0;
	int bufferSize = 0;
protected:

	pAudioCallback AudioCallback = NULL;
	void* owner = NULL;

	/*int decibels[DECIBEL_ARRAY_SIZE];

	bool calcdb = false;*/

private:
	void run();
private:
	int sampleRate = 44100;
	int sampleSize = 16;
	int channels = 2;

	QAudioOutput *output = NULL;
	QIODevice * io = NULL;

	QWaitCondition cond;
	QMutex pcmMux;
	QByteArray pcmCache;

	std::thread play_t;

	bool isExit = false;
	bool clearCache = false;

};
