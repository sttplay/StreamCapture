#pragma once
class Clock
{
public:
	Clock();

	/*
	 * 设置时钟，以当前时间为基准
	 * crtts: 设置当前时钟的时间
	 */
	void set_clock(double crtts);

	void set_clock_at(double pts, double time);

	double get_clock();

	void sync_clock_to_slave(Clock *slave, bool force = false);

public:
	bool paused;
private:
	double pts;
	double pts_drift;
	double last_updated;
};

