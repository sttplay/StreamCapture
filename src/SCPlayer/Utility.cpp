#include "Utility.h"
#include <QTime>
Frame* Utility::CloneNewFrame(const Frame* srcFrame)
{
	return new Frame(*srcFrame);
}

void Utility::CopyFrame(Frame* &destFrame, const Frame* srcFrame)
{
	memcpy(destFrame, srcFrame, sizeof(Frame));
	for (int i = 0; i < 8; i++)
	{
		int len = sizeof(*srcFrame->data[i]);
		memcpy(destFrame->data[i], srcFrame->data[i], srcFrame->linesize[i] * srcFrame->height);
		destFrame->linesize[i] = srcFrame->linesize[i];
	}
}

void Utility::ReleaseFrame(const Frame** frame)
{

}

QString Utility::ConvertToTime(int ms, bool accurate)
{
	QTime n(0, 0, 0);
	QTime t;
	t = n.addMSecs(ms);
	if (accurate)
		return t.toString("h:mm:ss.zzz");
	else
		return t.toString("h:mm:ss");
}
