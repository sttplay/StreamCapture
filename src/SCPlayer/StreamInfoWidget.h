#pragma once

#include <QWidget>
#include "ui_StreamInfoWidget.h"

class StreamInfoWidget : public QWidget
{
	Q_OBJECT

public:
	StreamInfoWidget();
	~StreamInfoWidget();

	void ShowInfo(QString msg);

public:
	StreamInfoWidget** selfOnBase = NULL;
protected:
	void closeEvent(QCloseEvent *event);
private:
	Ui::StreamInfoWidget ui;
};
