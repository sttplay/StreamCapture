@echo off

::删除platforms文件夹
::if exist platforms (rd/s/q platforms)
::if exist audio (rd/s/q audio)
if exist avcodec-58.dll (del avcodec-58.dll)
if exist avdevice-58.dll (del avdevice-58.dll)
if exist avfilter-7.dll (del avfilter-7.dll)
if exist avformat-58.dll (del avformat-58.dll)
if exist avutil-56.dll (del avutil-56.dll)
if exist postproc-55.dll (del postproc-55.dll)
if exist swresample-3.dll (del swresample-3.dll)
if exist swscale-5.dll (del swscale-5.dll)
if exist SDL2.dll (del SDL2.dll)
if exist StreamCapture.dll (del StreamCapture.dll)