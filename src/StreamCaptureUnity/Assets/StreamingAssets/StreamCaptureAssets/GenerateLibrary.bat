@echo off

set dllpath=..\..\..\..\..\bin
if not exist avcodec-58.dll.dll (copy %dllpath%\avcodec-58.dll .\)
if not exist avdevice-58.dll (copy %dllpath%\avdevice-58.dll .\)
if not exist avfilter-7.dll (copy %dllpath%\avfilter-7.dll .\)
if not exist avformat-58.dll (copy %dllpath%\avformat-58.dll .\)
if not exist avutil-56.dll (copy %dllpath%\avutil-56.dll .\)
if not exist postproc-55.dll (copy %dllpath%\postproc-55.dll .\)
if not exist swresample-3.dll (copy %dllpath%\swresample-3.dll .\)
if not exist swscale-5.dll (copy %dllpath%\swscale-5.dll .\)
if not exist SDL2.dll (copy %dllpath%\SDL2.dll .\)
if not exist StreamCapture.dll (copy %dllpath%\StreamCapture.dll .\)
