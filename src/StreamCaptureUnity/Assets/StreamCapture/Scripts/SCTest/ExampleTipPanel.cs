﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExampleTipPanel : MonoBehaviour {

    private Text titleText;
    private Text contentText;
    private RectTransform contentRect, contentTextRect;
    // Use this for initialization
    void Start () {
        titleText = transform.Find("TitleText").GetComponent<Text>();
        contentText = transform.Find("Content/Viewport/Content/Text").GetComponent<Text>();
        transform.Find("CloseButton").GetComponent<Button>().onClick.AddListener(() => {
            gameObject.SetActive(false);
            titleText.text = contentText.text = "";
        });
        contentRect = transform.Find("Content/Viewport/Content").GetComponent<RectTransform>();
        contentTextRect = contentText.rectTransform;
	}

    public void MessageTip(LogLevel level, string title, string msg)
    {
        if (titleText == null) Start();
        string color = "FFFFFFFF";
        switch(level)
        {
            case LogLevel.LEVEL_INFO:
                color = "323232FF";
                break;
            case LogLevel.LEVEL_WARNING:
                color = "FFA000FF";
                break;
            case LogLevel.LEVEL_ERROR:
                color = "FF0000FF";
                break;
        }
        titleText.text = string.Format("<color=#{0}>{1}</color>", color, title);
        contentText.text = string.Format("<color=#{0}>{1}</color>", color, msg);
        gameObject.SetActive(true);
    }

    private void Update()
    {
        contentRect.sizeDelta = contentTextRect.sizeDelta;
    }
}
