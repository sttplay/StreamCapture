﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class TestResampler : MonoBehaviour
{
    private SCResampler resampler;
    public Button openBtn, closeBtn;
    // Use this for initialization
    void Start()
    {
        resampler = new SCResampler();
        openBtn.onClick.AddListener(Open);
        closeBtn.onClick.AddListener(Close);
    }

    private void Open()
    {
        AudioStream stream = new AudioStream();
        stream.streamCount = 1;
        stream.channels = 2;
        stream.channel_layout = 3;
        stream.samplerate = 44100;
        stream.samplefmt = 8;
        if(!resampler.Open(stream, stream))
        {
            Debug.LogError("Resampler open failed!");
        }
    }




    private void Close()
    {
        resampler.Close();
    }
  

    private void OnDestroy()
    {
        resampler.Release();
        resampler = null;
    }
}
