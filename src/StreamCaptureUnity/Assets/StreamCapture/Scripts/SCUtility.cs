﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCUtility {

	public static double max(double num1, double num2)
    {
        return num1 > num2 ? num1 : num2;
    }

    public static int String2Int(string strnum, int defaultValue)
    {
        int v = 0;
        if(int.TryParse(strnum, out v))
            return v;
        return defaultValue;
    }

    public static string Int2Time(int ms)
    {
        TimeSpan ts = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(ms));
        string timeStr = ts.ToString(@"hh\:mm\:ss");
        //if (ts.Hours > 0)
        //{
             
        //    timeStr = ts.Hours.ToString() + "小时 " + ts.Minutes.ToString() + "分钟 " + ts.Seconds + "秒";
        //}
        //if (ts.Hours == 0 && ts.Minutes > 0)
        //{
        //    timeStr = ts.Minutes.ToString() + "分钟 " + ts.Seconds + "秒";
        //}
        //if (ts.Hours == 0 && ts.Minutes == 0)
        //{
        //    timeStr = ts.Seconds + "秒";
        //}
        return timeStr;
    }
}
