﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Compilation;
#endif
using UnityEngine;

public class SCInstance : MonoBehaviour
{


    public static bool isPaused = false;
    private static SCInstance instance;
    public static SCInstance Instance
    {
        get { return instance; }
    }
    private List<SCPlayer> scplayers = new List<SCPlayer>();
    public void Init()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this);

        IStreamCapture.Init();
#if UNITY_EDITOR
        EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        EditorApplication.pauseStateChanged += OnPauseModeStateChanged;

#if UNITY_EDITOR && (UNITY_2018_4 || UNITY_2019_4 || UNITY_2020_1_OR_NEWER)
        CompilationPipeline.compilationStarted += OnBeginCompileScripts;
#endif //UNITY_VERSION

#endif
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void UnityAutoInit()
    {
        //UNITY_2017_2_OR_NEWER
#if UNITY_EDITOR && (UNITY_2018_4 || UNITY_2019_4 || UNITY_2020_1_OR_NEWER)
        EditorApplication.wantsToQuit -= UnityEditorQuit;
        EditorApplication.wantsToQuit += UnityEditorQuit;
#endif
    }

#if UNITY_EDITOR
    private void OnPlayModeStateChanged(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.ExitingPlayMode)
        {
            OnDestroy();
            Debug.LogFormat("<b>{0}</b>", "[SCPlayer] Shutdown");
        }
        
    }
    private void OnPauseModeStateChanged(PauseState state)
    {
        isPaused = state == PauseState.Paused ? true : false;
    }
    private static bool UnityEditorQuit()
    {
        instance.OnDestroy();
        IStreamCapture.Uninstall();
        return true;
    }

    private void OnBeginCompileScripts(object obj)
    {
        OnDestroy();
    }
#endif

    // Update is called once per frame
    void Update()
    {
        Dispatcher.WakeAll();
    }
    public void AddPlayer(SCPlayer player)
    {
        scplayers.Add(player);
    }
    public void RemovePlayer(SCPlayer player)
    {
        scplayers.Remove(player);
    }

    public void OnDestroy()
    {
        while (scplayers.Count > 0)
            scplayers[0].OnDestroy();
        IStreamCapture.Deinit();
#if UNITY_EDITOR
#if UNITY_EDITOR && (UNITY_2018_4 || UNITY_2019_4 || UNITY_2020_1_OR_NEWER)
        CompilationPipeline.compilationStarted -= OnBeginCompileScripts;
#endif //UNITY_VERSION
#else
        IStreamCapture.Uninstall();
#endif
        Dispatcher.WakeAll();

    }
}
