﻿using System;
using System.Collections.Generic;
public class Dispatcher
{
    private static List<Action> funcList = new List<Action>();
    public static void Invoke(Action func)
    {
        lock (funcList)
            funcList.Add(func);
    }

    public static void WakeAll()
    {
        for (int i = 0; i < funcList.Count; i++)
            funcList[i]();
        lock (funcList)
            funcList.Clear();
    }
}