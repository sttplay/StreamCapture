﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCCanvas
{
    protected Material mat;
    protected Texture2D[] texs;
    protected OutPixFmt pixfmt;
    protected int width;
    protected int height;
    protected int bpp;
    public static SCCanvas CreateCanvas(int fmt)
    {
        if (fmt == (int)OutPixFmt.PIX_FMT_YUV420P || fmt == (int)OutPixFmt.PIX_FMT_YUVJ420P)
            return new SCRendererYUV420P((OutPixFmt)fmt);
        else if (fmt == (int)OutPixFmt.PIX_FMT_YUV422P || fmt == (int)OutPixFmt.PIX_FMT_YUVJ422P)
            return new SCRendererYUV422P((OutPixFmt)fmt);
        else if (fmt == (int)OutPixFmt.PIX_FMT_YUV444P || fmt == (int)OutPixFmt.PIX_FMT_YUVJ444P)
            return new SCRendererYUV444P((OutPixFmt)fmt);

        else if (fmt == (int)OutPixFmt.PIX_FMT_YUYV422)
            return new SCRendererYUYV422((OutPixFmt)fmt);

        else if (fmt == (int)OutPixFmt.PIX_FMT_GRAY8)
            return new SCRendererGray8((OutPixFmt)fmt);

        else if (fmt == (int)OutPixFmt.PIX_FMT_NV12)
            return new SCRendererNV12((OutPixFmt)fmt);
        else if (fmt == (int)OutPixFmt.PIX_FMT_NV21)
            return new SCRendererNV21((OutPixFmt)fmt);

        else if (fmt == (int)OutPixFmt.PIX_FMT_RGB24)
            return new SCRendererRGB((OutPixFmt)fmt);
        else if (fmt == (int)OutPixFmt.PIX_FMT_BGR24)
            return new SCRendererBGR((OutPixFmt)fmt);

        else if (fmt == (int)OutPixFmt.PIX_FMT_ARGB)
            return new SCRendererARGB((OutPixFmt)fmt);
        else if (fmt == (int)OutPixFmt.PIX_FMT_RGBA)
            return new SCRendererRGBA((OutPixFmt)fmt);
        else if (fmt == (int)OutPixFmt.PIX_FMT_ABGR)
            return new SCRendererABGR((OutPixFmt)fmt);
        else if (fmt == (int)OutPixFmt.PIX_FMT_BGRA)
            return new SCRendererBGRA((OutPixFmt)fmt);

        return null;
    }

    public virtual void InitCanvas(ref Material mat, VideoStream vs)
    {
        width = vs.pixelwidth;
        height = vs.pixelheight;
    }
    public virtual void Renderer(Frame frame) { }
}