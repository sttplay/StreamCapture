﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCRendererYUYV422 : SCCanvas {

    private byte[] cacheYUV;
    public SCRendererYUYV422(OutPixFmt fmt)
    {
        texs = new Texture2D[2];
        pixfmt = fmt;
        bpp = 2;
    }

    public override void InitCanvas(ref Material mat, VideoStream vs)
    {
        base.InitCanvas(ref mat, vs);
        this.mat = mat = new Material(Resources.Load<Material>("StreamCapture/SCMatYUYV422"));
        texs[0] = new Texture2D(width, height, TextureFormat.RG16, false);
        texs[1] = new Texture2D(width / 2, height, TextureFormat.RGBA32, false);
    }

    public override void Renderer(Frame frame)
    {

        if (frame == null) return;

        if (frame.format != (int)OutPixFmt.PIX_FMT_YUYV422)
        {
            Debug.LogError("The pixel format is different from the canvas type");
            return;
        }

        if (frame.linesize[0] != width * bpp)
        {
            if (cacheYUV == null)
            {
                cacheYUV = new byte[width * height * bpp];
            }
            frame.data[0] = IStreamCapture.MemoryAlignment(frame.data[0], width * bpp, height, frame.linesize[0], cacheYUV);

        }

        texs[0].LoadRawTextureData(frame.data[0], width * height * bpp);
        texs[1].LoadRawTextureData(frame.data[0], width * height * bpp);


        texs[0].Apply();
        texs[1].Apply();


        mat.SetTexture("_YTex", texs[0]);
        mat.SetTexture("_UVTex", texs[1]);
    }
}
