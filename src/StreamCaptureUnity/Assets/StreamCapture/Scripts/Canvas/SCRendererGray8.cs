﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCRendererGray8 : SCCanvas {

    private byte[] cache;
    public SCRendererGray8(OutPixFmt fmt)
    {
        texs = new Texture2D[1];
        pixfmt = fmt;
        bpp = 1;
    }

    public override void InitCanvas(ref Material mat, VideoStream vs)
    {
        base.InitCanvas(ref mat, vs);
        this.mat = mat = new Material(Resources.Load<Material>("StreamCapture/SCMatGRAY"));
        texs[0] = new Texture2D(width, height, TextureFormat.R8, false);
    }

    public override void Renderer(Frame frame)
    {

        if (frame == null) return;

        if (frame.format != (int)OutPixFmt.PIX_FMT_GRAY8)
        {
            Debug.LogError("The pixel format is different from the canvas type");
            return;
        }

        if (frame.linesize[0] != width)
        {
            if (cache == null)
            {
                cache = new byte[width * height];
            }
            frame.data[0] = IStreamCapture.MemoryAlignment(frame.data[0], width, height, frame.linesize[0], cache);
        }

        texs[0].LoadRawTextureData(frame.data[0], width * height);


        texs[0].Apply();
        mat.SetTexture("_Tex", texs[0]);
    }
}
