﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SCPlayer))]
public class SCPlayerInspectorEditor : Editor
{
    private SerializedObject scp;
    private SerializedProperty defaultTex;
    private SerializedProperty rendererRawImage;
    private SerializedProperty rendererMesh;
    private SerializedProperty openModel;

    private SerializedProperty enableVideo;
    private SerializedProperty enableAudio;

    private SerializedProperty urlType;
    private SerializedProperty url;
    private SerializedProperty hardwareAccelType;
    private SerializedProperty autoOpen;
    private SerializedProperty openAutoPlay;
    private SerializedProperty isLoop;
    private SerializedProperty volume;
    private SerializedProperty outpixfmt;
    private SerializedProperty nosync;
    private SerializedProperty nobuffer;
    private SerializedProperty codecForceLowDelay;
    private SerializedProperty openTimeout;
    private SerializedProperty readTimeout;
    private SerializedProperty rtspTransportType;

    private SerializedProperty cameraWidth;
    private SerializedProperty cameraHeight;
    private SerializedProperty cameraFrameRate;
    private SerializedProperty cameraInputPriority;

    private void OnEnable()
    {
        scp = new SerializedObject(target);
        defaultTex = scp.FindProperty("defaultTex");
        rendererRawImage = scp.FindProperty("rendererRawImage");
        rendererMesh = scp.FindProperty("rendererMesh");
        openModel = scp.FindProperty("openModel");

        enableVideo = scp.FindProperty("enableVideo");
        enableAudio = scp.FindProperty("enableAudio");

        urlType = scp.FindProperty("urlType");
        url = scp.FindProperty("url");
        hardwareAccelType = scp.FindProperty("hardwareAccelType");
        autoOpen = scp.FindProperty("autoOpen");
        openAutoPlay = scp.FindProperty("openAutoPlay");
        isLoop = scp.FindProperty("isLoop");

        volume = scp.FindProperty("volume");

        outpixfmt = scp.FindProperty("outpixfmt");
        openTimeout = scp.FindProperty("openTimeout");
        readTimeout = scp.FindProperty("readTimeout");
        nosync = scp.FindProperty("nosync");
        nobuffer = scp.FindProperty("nobuffer");
        codecForceLowDelay = scp.FindProperty("codecForceLowDelay");
        rtspTransportType = scp.FindProperty("rtspTransportType");

        cameraWidth = scp.FindProperty("cameraWidth");
        cameraHeight = scp.FindProperty("cameraHeight");
        cameraFrameRate = scp.FindProperty("cameraFrameRate");
        cameraInputPriority = scp.FindProperty("cameraInputPriority");
    }

    public override void OnInspectorGUI()
    {
        scp.Update();
        EditorGUILayout.PropertyField(defaultTex);
        EditorGUILayout.PropertyField(rendererRawImage);
        EditorGUILayout.PropertyField(rendererMesh);
        EditorGUILayout.PropertyField(openModel);
       
        if (openModel.enumValueIndex == (int)OpenModel.URL)
        {
            EditorGUILayout.PropertyField(urlType);
            EditorGUILayout.PropertyField(url);
            EditorGUILayout.PropertyField(hardwareAccelType);
            EditorGUILayout.PropertyField(enableVideo);
            EditorGUILayout.PropertyField(enableAudio);
            EditorGUILayout.PropertyField(autoOpen);
            EditorGUILayout.PropertyField(openAutoPlay);
            EditorGUILayout.PropertyField(isLoop);
            EditorGUILayout.PropertyField(volume);

            EditorGUILayout.PropertyField(outpixfmt);
            EditorGUILayout.PropertyField(openTimeout);
            EditorGUILayout.PropertyField(readTimeout);
            EditorGUILayout.PropertyField(nosync);
            EditorGUILayout.PropertyField(nobuffer);
            EditorGUILayout.PropertyField(codecForceLowDelay);
            EditorGUILayout.PropertyField(rtspTransportType);
        }
        else if (openModel.enumValueIndex == (int)OpenModel.Camera)
        {
            EditorGUILayout.PropertyField(url);
            EditorGUILayout.PropertyField(hardwareAccelType);
            EditorGUILayout.PropertyField(cameraWidth);
            EditorGUILayout.PropertyField(cameraHeight);
            EditorGUILayout.PropertyField(cameraFrameRate);
            EditorGUILayout.PropertyField(autoOpen);
            EditorGUILayout.PropertyField(nosync);
            EditorGUILayout.PropertyField(cameraInputPriority);
            EditorGUILayout.PropertyField(outpixfmt);
        }
        scp.ApplyModifiedProperties();

    }
}
